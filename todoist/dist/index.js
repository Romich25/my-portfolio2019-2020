/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/app/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/app.css":
/*!***************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/app.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"::-webkit-scrollbar { width: 8px; height: 3px;}\\r\\n::-webkit-scrollbar-button {  background-color: #17a2b8; height: 0;}\\r\\n::-webkit-scrollbar-track {  background-color: #17a2b8;}\\r\\n::-webkit-scrollbar-track-piece { background-color: #ffffff;}\\r\\n::-webkit-scrollbar-thumb { height: 50px; background-color: #17a2b8;}\\r\\n\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/app.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/enter-modal.css":
/*!***********************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/enter-modal.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".enter-modal {\\r\\n    position: fixed;\\r\\n    z-index: 4;\\r\\n    padding-top: 93px; \\r\\n    left: 0;\\r\\n    top: 0;\\r\\n    width: 100%;\\r\\n    height: 100%;\\r\\n    overflow: auto;\\r\\n    background-color: rgba(0,0,0,0.9);\\r\\n    display: block;\\r\\n}\\r\\n\\r\\n\\r\\n.modal-content {\\r\\n    position: relative;\\r\\n    margin: auto;\\r\\n    padding: 0;\\r\\n    max-width: 600px;\\r\\n    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);\\r\\n    -webkit-animation-name: animatetop;\\r\\n    -webkit-animation-duration: 0.4s;\\r\\n    animation-name: animatetop;\\r\\n    animation-duration: 0.4s\\r\\n}\\r\\n\\r\\n.add-new:hover .input-group-text {\\r\\n    background: #d6e3f3;\\r\\n    cursor: pointer;\\r\\n}\\r\\n\\r\\n\\r\\n@-webkit-keyframes animatetop {\\r\\n    from {top:-300px; opacity:0} \\r\\n    to {top:0; opacity:1}\\r\\n}\\r\\n\\r\\n@keyframes animatetop {\\r\\n    from {top:-300px; opacity:0}\\r\\n    to {top:0; opacity:1}\\r\\n}\\r\\n\\r\\n\\r\\n.close {\\r\\n    color: white;\\r\\n    float: right;\\r\\n    font-size: 28px;\\r\\n    font-weight: bold;\\r\\n}\\r\\n\\r\\n.close:hover,\\r\\n.close:focus {\\r\\n    color: white;\\r\\n    text-decoration: none;\\r\\n    cursor: pointer;\\r\\n}\\r\\n\\r\\n.modal-header {\\r\\n    padding: 2px 16px;\\r\\n    color: white;\\r\\n}\\r\\n\\r\\n.modal-body {padding: 2px 16px;}\\r\\n\\r\\n.modal-footer {\\r\\n    padding: 2px 16px;\\r\\n    color: white;\\r\\n}\\r\\n\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/enter-modal.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-body/enter-button/enter-button.css":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-body/enter-button/enter-button.css ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/enter-button/enter-button.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-body/error-text/error-text.css":
/*!**********************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-body/error-text/error-text.css ***!
  \**********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/error-text/error-text.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-body/login-label/login-label.css":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-body/login-label/login-label.css ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/login-label/login-label.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-body/modal-body.css":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-body/modal-body.css ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/modal-body.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-body/password-label/password-label.css":
/*!******************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-body/password-label/password-label.css ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/password-label/password-label.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-body/register/register.css":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-body/register/register.css ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".register {\\r\\n    cursor: pointer;\\r\\n}\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/register/register.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-content.css":
/*!***************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-content.css ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-content.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-header/modal-header.css":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-header/modal-header.css ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-header/modal-header.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/footer/footer.css":
/*!*************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/footer/footer.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".footer {\\r\\n    height: 40px;\\r\\n    position: absolute;\\r\\n    width: 100%;\\r\\n    bottom: 0;\\r\\n    line-height: 40px;\\r\\n}\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/footer/footer.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/header/container/container.css":
/*!**************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/header/container/container.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/header/container/container.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/header/container/logo/logo.css":
/*!**************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/header/container/logo/logo.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"[alt=\\\"logo\\\"] {\\r\\n    height: 53px;\\r\\n    display: block;\\r\\n    filter: invert(1);\\r\\n}\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/header/container/logo/logo.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/header/container/search-label/search-input-group/input-group-prepend/input-group-prepend.css":
/*!****************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/header/container/search-label/search-input-group/input-group-prepend/input-group-prepend.css ***!
  \****************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/header/container/search-label/search-input-group/input-group-prepend/input-group-prepend.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/header/container/search-label/search-input-group/search-input-group.css":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/header/container/search-label/search-input-group/search-input-group.css ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/header/container/search-label/search-input-group/search-input-group.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/header/container/search-label/search-input-group/search/search.css":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/header/container/search-label/search-input-group/search/search.css ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/header/container/search-label/search-input-group/search/search.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/header/container/search-label/search-label.css":
/*!******************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/header/container/search-label/search-label.css ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/header/container/search-label/search-label.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/header/container/user/exet-button/exit-button.css":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/header/container/user/exet-button/exit-button.css ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/header/container/user/exet-button/exit-button.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/header/container/user/input-group-prepend/input-group-prepend.css":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/header/container/user/input-group-prepend/input-group-prepend.css ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/header/container/user/input-group-prepend/input-group-prepend.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/header/container/user/input/input.css":
/*!*********************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/header/container/user/input/input.css ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/header/container/user/input/input.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/header/container/user/user.css":
/*!**************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/header/container/user/user.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/header/container/user/user.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/header/header.css":
/*!*************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/header/header.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/header/header.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/add-category-modal/add-category-modal.css":
/*!******************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/add-category-modal/add-category-modal.css ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".add-category-modal{\\r\\n    display: none;\\r\\n    position: fixed;\\r\\n    z-index: 4;\\r\\n    padding-top: 93px; \\r\\n    left: 0;\\r\\n    top: 0;\\r\\n    width: 100%;\\r\\n    height: 100%;\\r\\n    overflow: auto;\\r\\n    background-color: rgb(0,0,0);\\r\\n    background-color: rgba(0,0,0,0.4);\\r\\n}\\r\\n\\r\\n.modal-content {\\r\\n    position: relative;\\r\\n    margin: auto;\\r\\n    padding: 0;\\r\\n    width: 50%;\\r\\n    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);\\r\\n    -webkit-animation-name: animatetop;\\r\\n    -webkit-animation-duration: 0.4s;\\r\\n    animation-name: animatetop;\\r\\n    animation-duration: 0.4s\\r\\n}\\r\\n\\r\\n.add-new:hover .input-group-text {\\r\\n    background: #d6e3f3;\\r\\n    cursor: pointer;\\r\\n}\\r\\n\\r\\n\\r\\n@-webkit-keyframes animatetop {\\r\\n    from {top:-300px; opacity:0} \\r\\n    to {top:0; opacity:1}\\r\\n}\\r\\n\\r\\n@keyframes animatetop {\\r\\n    from {top:-300px; opacity:0}\\r\\n    to {top:0; opacity:1}\\r\\n}\\r\\n\\r\\n\\r\\n.close {\\r\\n    color: white;\\r\\n    float: right;\\r\\n    font-size: 28px;\\r\\n    font-weight: bold;\\r\\n}\\r\\n\\r\\n.close:hover,\\r\\n.close:focus {\\r\\n    color: white;\\r\\n    text-decoration: none;\\r\\n    cursor: pointer;\\r\\n}\\r\\n\\r\\n.modal-header {\\r\\n    padding: 2px 16px;\\r\\n    color: white;\\r\\n}\\r\\n\\r\\n.modal-body {padding: 2px 16px;}\\r\\n\\r\\n.modal-footer {\\r\\n    padding: 2px 16px;\\r\\n    color: white;\\r\\n}\\r\\n\\r\\n\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/add-category-modal.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/add-category-modal/category-form/category-form.css":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/add-category-modal/category-form/category-form.css ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/category-form.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/add-category-modal/category-form/modal-header/modal-header.css":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/add-category-modal/category-form/modal-header/modal-header.css ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/modal-header/modal-header.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/add-category-modal/category-form/modalbody/add-category-btn/add-category-btn.css":
/*!*********************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/add-category-modal/category-form/modalbody/add-category-btn/add-category-btn.css ***!
  \*********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/modalbody/add-category-btn/add-category-btn.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/add-category-modal/category-form/modalbody/category-title-label/category-title-label.css":
/*!*****************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/add-category-modal/category-form/modalbody/category-title-label/category-title-label.css ***!
  \*****************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/modalbody/category-title-label/category-title-label.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/add-category-modal/category-form/modalbody/modal-body.css":
/*!**********************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/add-category-modal/category-form/modalbody/modal-body.css ***!
  \**********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/modalbody/modal-body.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/add-task-btn/add-task-btn.css":
/*!******************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/add-task-btn/add-task-btn.css ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/add-task-btn/add-task-btn.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/main.css":
/*!*********************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/main.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".main {\\r\\n    /* display: block; */\\r\\n    height: 85.7vh;\\r\\n    overflow: auto;\\r\\n    background-color: #e7fcff;\\r\\n}\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/main.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/form.css":
/*!*****************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/form.css ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/form.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/category/category.css":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/category/category.css ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/category/category.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/comment/comment.css":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/comment/comment.css ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/comment/comment.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/deadline-date/deadline-date.css":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/deadline-date/deadline-date.css ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/deadline-date/deadline-date.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/modal-body.css":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/modal-body.css ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/modal-body.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/priority/priority.css":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/priority/priority.css ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/priority/priority.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/task-title/task-title.css":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/task-title/task-title.css ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/task-title/task-title.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/time/time.css":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/time/time.css ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/time/time.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-footer/modal-footer.css":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-footer/modal-footer.css ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-footer/modal-footer.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-header/modal-header.css":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-header/modal-header.css ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-header/modal-header.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/modal-add-task.css":
/*!**********************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/modal-add-task.css ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".modal-add-task {\\r\\n    display: none;\\r\\n    position: fixed;\\r\\n    z-index: 4;\\r\\n    padding-top: 93px; \\r\\n    left: 0;\\r\\n    top: 0;\\r\\n    width: 100%;\\r\\n    height: 100%;\\r\\n    overflow: auto;\\r\\n    background-color: rgb(0,0,0);\\r\\n    background-color: rgba(0,0,0,0.4);\\r\\n}\\r\\n\\r\\n\\r\\n.modal-content {\\r\\n    position: relative;\\r\\n    margin: auto;\\r\\n    padding: 0;\\r\\n    width: 50%;\\r\\n    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);\\r\\n    -webkit-animation-name: animatetop;\\r\\n    -webkit-animation-duration: 0.4s;\\r\\n    animation-name: animatetop;\\r\\n    animation-duration: 0.4s\\r\\n}\\r\\n\\r\\n.add-new:hover .input-group-text {\\r\\n    background: #d6e3f3;\\r\\n    cursor: pointer;\\r\\n}\\r\\n\\r\\n\\r\\n@-webkit-keyframes animatetop {\\r\\n    from {top:-300px; opacity:0} \\r\\n    to {top:0; opacity:1}\\r\\n}\\r\\n\\r\\n@keyframes animatetop {\\r\\n    from {top:-300px; opacity:0}\\r\\n    to {top:0; opacity:1}\\r\\n}\\r\\n\\r\\n\\r\\n.close {\\r\\n    color: white;\\r\\n    float: right;\\r\\n    font-size: 28px;\\r\\n    font-weight: bold;\\r\\n}\\r\\n\\r\\n.close:hover,\\r\\n.close:focus {\\r\\n    color: white;\\r\\n    text-decoration: none;\\r\\n    cursor: pointer;\\r\\n}\\r\\n\\r\\n.modal-header {\\r\\n    padding: 2px 16px;\\r\\n    color: white;\\r\\n}\\r\\n\\r\\n.modal-body {padding: 2px 16px;}\\r\\n\\r\\n.modal-footer {\\r\\n    padding: 2px 16px;\\r\\n    color: white;\\r\\n}\\r\\n\\r\\n\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/modal-add-task.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/change-form.css":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/change-form.css ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/change-form.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/form-group.css":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/form-group.css ***!
  \*********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/form-group.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/category-label/category-label.css":
/*!***********************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/category-label/category-label.css ***!
  \***********************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/category-label/category-label.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/comment-label/comment-label.css":
/*!*********************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/comment-label/comment-label.css ***!
  \*********************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/comment-label/comment-label.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/deadline-label/deadline-label.css":
/*!***********************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/deadline-label/deadline-label.css ***!
  \***********************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".ui-widget-header {\\r\\n    background: rgb(23, 162, 184);\\r\\n}\\r\\n\\r\\n.ui-widget-header {\\r\\n    color: rgb(255, 255, 255);\\r\\n}\\r\\n\\r\\n.ui-datepicker table {\\r\\n    background: #e7fcff;\\r\\n}\\r\\n\\r\\n.ui-datepicker .ui-datepicker-next {\\r\\n    background: rgb(23, 162, 184);\\r\\n}    \\r\\n\\r\\n.ui-datepicker .ui-datepicker-next:hover,\\r\\n.ui-datepicker-prev:hover {\\r\\n    background: rgb(23, 162, 184);\\r\\n    border: none;\\r\\n}    \\r\\n\\r\\n.ui-state-default, .ui-widget-content .ui-state-default {\\r\\n    background: lightblue;\\r\\n    border: 1px solid rgba(0,123,255,.25);\\r\\n}\\r\\n\\r\\n.ui-widget-content .ui-state-default:hover {\\r\\n    background: rgb(23, 162, 184);\\r\\n}\\r\\n\\r\\n\\r\\n.ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {\\r\\n    border-color:  rgba(40, 167, 69);\\r\\n    \\r\\n    background: rgba(40, 167, 69, 0.561);\\r\\n}\\r\\n\\r\\n.ui-widget-header .ui-icon {\\r\\n    filter: invert(1);\\r\\n}\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/deadline-label/deadline-label.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/labels.css":
/*!************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/labels.css ***!
  \************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/labels.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/priority-label/priority-label.css":
/*!***********************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/priority-label/priority-label.css ***!
  \***********************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/priority-label/priority-label.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/progress-label/progress-label.css":
/*!***********************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/progress-label/progress-label.css ***!
  \***********************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/progress-label/progress-label.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/time-label/time-label.css":
/*!***************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/time-label/time-label.css ***!
  \***************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/time-label/time-label.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/title-label/title-label.css":
/*!*****************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/title-label/title-label.css ***!
  \*****************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/title-label/title-label.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/title/title.css":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/title/title.css ***!
  \**********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/title/title.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/modal-body.css":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/modal-body.css ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/modal-body.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-content.css":
/*!**************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-content.css ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-content.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-footer/change-btn/change-btn.css":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-footer/change-btn/change-btn.css ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-footer/change-btn/change-btn.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-footer/delete-btn/delete-btn.css":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-footer/delete-btn/delete-btn.css ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-footer/delete-btn/delete-btn.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-footer/modal-footer.css":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-footer/modal-footer.css ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-footer/modal-footer.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-header/modal-header.css":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-header/modal-header.css ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-header/modal-header.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal.css":
/*!****************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".modal {\\r\\n    position: fixed;\\r\\n    z-index: 4;\\r\\n    padding-top: 93px; \\r\\n    left: 0;\\r\\n    top: 0;\\r\\n    width: 100%;\\r\\n    height: 100%;\\r\\n    overflow: auto;\\r\\n    background-color: rgb(0,0,0);\\r\\n    background-color: rgba(0,0,0,0.4);\\r\\n}\\r\\n\\r\\n\\r\\n.modal-content {\\r\\n    position: relative;\\r\\n    margin: auto;\\r\\n    padding: 0;\\r\\n    width: 50%;\\r\\n    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);\\r\\n    -webkit-animation-name: animatetop;\\r\\n    -webkit-animation-duration: 0.4s;\\r\\n    animation-name: animatetop;\\r\\n    animation-duration: 0.4s\\r\\n}\\r\\n\\r\\n.add-new:hover .input-group-text {\\r\\n    background: #d6e3f3;\\r\\n    cursor: pointer;\\r\\n}\\r\\n\\r\\n\\r\\n@-webkit-keyframes animatetop {\\r\\n    from {top:-300px; opacity:0} \\r\\n    to {top:0; opacity:1}\\r\\n}\\r\\n\\r\\n@keyframes animatetop {\\r\\n    from {top:-300px; opacity:0}\\r\\n    to {top:0; opacity:1}\\r\\n}\\r\\n\\r\\n\\r\\n.close {\\r\\n    color: white;\\r\\n    float: right;\\r\\n    font-size: 28px;\\r\\n    font-weight: bold;\\r\\n}\\r\\n\\r\\n.close:hover,\\r\\n.close:focus {\\r\\n    color: white;\\r\\n    text-decoration: none;\\r\\n    cursor: pointer;\\r\\n}\\r\\n\\r\\n.modal-header {\\r\\n    padding: 2px 16px;\\r\\n    color: white;\\r\\n}\\r\\n\\r\\n.modal-body {padding: 2px 16px;}\\r\\n\\r\\n.modal-footer {\\r\\n    padding: 2px 16px;\\r\\n    color: white;\\r\\n}\\r\\n\\r\\n\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/all-tasks/all-tasks.css":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/all-tasks/all-tasks.css ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".all-tasks {\\r\\n    overflow: auto;\\r\\n    overflow-x: auto;\\r\\n}\\r\\n\\r\\ntr:hover {\\r\\n    background-color: rgb(233, 236, 239);\\r\\n    cursor: pointer;\\r\\n}\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/all-tasks/all-tasks.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/all-tasks/table/table.css":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/all-tasks/table/table.css ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/all-tasks/table/table.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/all-tasks/table/tbody/tbody.css":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/all-tasks/table/tbody/tbody.css ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/all-tasks/table/tbody/tbody.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/all-tasks/table/thead/thead.css":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/all-tasks/table/thead/thead.css ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/all-tasks/table/thead/thead.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/category-filtration/category-filtration.css":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/category-filtration/category-filtration.css ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/category-filtration/category-filtration.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/date-filtration/date-filtration.css":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/date-filtration/date-filtration.css ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/date-filtration/date-filtration.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/priority-filtration/priority-filtration.css":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/priority-filtration/priority-filtration.css ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/priority-filtration/priority-filtration.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/progress-filtration/progress-filtration.css":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/progress-filtration/progress-filtration.css ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/progress-filtration/progress-filtration.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/show-tasks/show-tasks.css":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/show-tasks/show-tasks.css ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".show-tasks {\\r\\n    margin-top: 15px;\\r\\n}\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/show-tasks/show-tasks.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/tasks-filtration.css":
/*!**************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/tasks-filtration.css ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/tasks-filtration.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/time-filtration/time-filtration.css":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/time-filtration/time-filtration.css ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/time-filtration/time-filtration.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/modal-content/modal-body/login-label/login-label.css":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/modal-content/modal-body/login-label/login-label.css ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-body/login-label/login-label.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/modal-content/modal-body/modal-body.css":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/modal-content/modal-body/modal-body.css ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-body/modal-body.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/modal-content/modal-body/password-label/password-label.css":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/modal-content/modal-body/password-label/password-label.css ***!
  \*********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-body/password-label/password-label.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/modal-content/modal-body/register-button/register-button.css":
/*!***********************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/modal-content/modal-body/register-button/register-button.css ***!
  \***********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-body/register-button/register-button.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/modal-content/modal-content.css":
/*!******************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/modal-content/modal-content.css ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-content.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/modal-content/modal-header/modal-header.css":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/modal-content/modal-header/modal-header.css ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \"\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-header/modal-header.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/register-modal.css":
/*!*****************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/register-modal.css ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".register-modal {\\r\\n    position: fixed;\\r\\n    z-index: 4;\\r\\n    padding-top: 93px; \\r\\n    left: 0;\\r\\n    top: 0;\\r\\n    width: 100%;\\r\\n    height: 100%;\\r\\n    overflow: auto;\\r\\n    background-color: rgba(0,0,0,0.9);\\r\\n    display: none;\\r\\n}\\r\\n\\r\\n\\r\\n.modal-content {\\r\\n    position: relative;\\r\\n    margin: auto;\\r\\n    padding: 0;\\r\\n    max-width: 600px;\\r\\n    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);\\r\\n    -webkit-animation-name: animatetop;\\r\\n    -webkit-animation-duration: 0.4s;\\r\\n    animation-name: animatetop;\\r\\n    animation-duration: 0.4s\\r\\n}\\r\\n\\r\\n.add-new:hover .input-group-text {\\r\\n    background: #d6e3f3;\\r\\n    cursor: pointer;\\r\\n}\\r\\n\\r\\n\\r\\n@-webkit-keyframes animatetop {\\r\\n    from {top:-300px; opacity:0} \\r\\n    to {top:0; opacity:1}\\r\\n}\\r\\n\\r\\n@keyframes animatetop {\\r\\n    from {top:-300px; opacity:0}\\r\\n    to {top:0; opacity:1}\\r\\n}\\r\\n\\r\\n\\r\\n.close {\\r\\n    color: white;\\r\\n    float: right;\\r\\n    font-size: 28px;\\r\\n    font-weight: bold;\\r\\n}\\r\\n\\r\\n.close:hover,\\r\\n.close:focus {\\r\\n    color: white;\\r\\n    text-decoration: none;\\r\\n    cursor: pointer;\\r\\n}\\r\\n\\r\\n.modal-header {\\r\\n    padding: 2px 16px;\\r\\n    color: white;\\r\\n}\\r\\n\\r\\n.modal-body {padding: 2px 16px;}\\r\\n\\r\\n.modal-footer {\\r\\n    padding: 2px 16px;\\r\\n    color: white;\\r\\n}\\r\\n\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./src/app/register-modal/register-modal.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\n/*\n  MIT License http://www.opensource.org/licenses/mit-license.php\n  Author Tobias Koppers @sokra\n*/\n// css base code, injected by the css-loader\n// eslint-disable-next-line func-names\nmodule.exports = function (useSourceMap) {\n  var list = []; // return the list of modules as css string\n\n  list.toString = function toString() {\n    return this.map(function (item) {\n      var content = cssWithMappingToString(item, useSourceMap);\n\n      if (item[2]) {\n        return \"@media \".concat(item[2], \" {\").concat(content, \"}\");\n      }\n\n      return content;\n    }).join('');\n  }; // import a list of modules into the list\n  // eslint-disable-next-line func-names\n\n\n  list.i = function (modules, mediaQuery, dedupe) {\n    if (typeof modules === 'string') {\n      // eslint-disable-next-line no-param-reassign\n      modules = [[null, modules, '']];\n    }\n\n    var alreadyImportedModules = {};\n\n    if (dedupe) {\n      for (var i = 0; i < this.length; i++) {\n        // eslint-disable-next-line prefer-destructuring\n        var id = this[i][0];\n\n        if (id != null) {\n          alreadyImportedModules[id] = true;\n        }\n      }\n    }\n\n    for (var _i = 0; _i < modules.length; _i++) {\n      var item = [].concat(modules[_i]);\n\n      if (dedupe && alreadyImportedModules[item[0]]) {\n        // eslint-disable-next-line no-continue\n        continue;\n      }\n\n      if (mediaQuery) {\n        if (!item[2]) {\n          item[2] = mediaQuery;\n        } else {\n          item[2] = \"\".concat(mediaQuery, \" and \").concat(item[2]);\n        }\n      }\n\n      list.push(item);\n    }\n  };\n\n  return list;\n};\n\nfunction cssWithMappingToString(item, useSourceMap) {\n  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring\n\n  var cssMapping = item[3];\n\n  if (!cssMapping) {\n    return content;\n  }\n\n  if (useSourceMap && typeof btoa === 'function') {\n    var sourceMapping = toComment(cssMapping);\n    var sourceURLs = cssMapping.sources.map(function (source) {\n      return \"/*# sourceURL=\".concat(cssMapping.sourceRoot || '').concat(source, \" */\");\n    });\n    return [content].concat(sourceURLs).concat([sourceMapping]).join('\\n');\n  }\n\n  return [content].join('\\n');\n} // Adapted from convert-source-map (MIT)\n\n\nfunction toComment(sourceMap) {\n  // eslint-disable-next-line no-undef\n  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));\n  var data = \"sourceMappingURL=data:application/json;charset=utf-8;base64,\".concat(base64);\n  return \"/*# \".concat(data, \" */\");\n}\n\n//# sourceURL=webpack:///./node_modules/css-loader/dist/runtime/api.js?");

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar isOldIE = function isOldIE() {\n  var memo;\n  return function memorize() {\n    if (typeof memo === 'undefined') {\n      // Test for IE <= 9 as proposed by Browserhacks\n      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805\n      // Tests for existence of standard globals is to allow style-loader\n      // to operate correctly into non-standard environments\n      // @see https://github.com/webpack-contrib/style-loader/issues/177\n      memo = Boolean(window && document && document.all && !window.atob);\n    }\n\n    return memo;\n  };\n}();\n\nvar getTarget = function getTarget() {\n  var memo = {};\n  return function memorize(target) {\n    if (typeof memo[target] === 'undefined') {\n      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself\n\n      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {\n        try {\n          // This will throw an exception if access to iframe is blocked\n          // due to cross-origin restrictions\n          styleTarget = styleTarget.contentDocument.head;\n        } catch (e) {\n          // istanbul ignore next\n          styleTarget = null;\n        }\n      }\n\n      memo[target] = styleTarget;\n    }\n\n    return memo[target];\n  };\n}();\n\nvar stylesInDom = [];\n\nfunction getIndexByIdentifier(identifier) {\n  var result = -1;\n\n  for (var i = 0; i < stylesInDom.length; i++) {\n    if (stylesInDom[i].identifier === identifier) {\n      result = i;\n      break;\n    }\n  }\n\n  return result;\n}\n\nfunction modulesToDom(list, options) {\n  var idCountMap = {};\n  var identifiers = [];\n\n  for (var i = 0; i < list.length; i++) {\n    var item = list[i];\n    var id = options.base ? item[0] + options.base : item[0];\n    var count = idCountMap[id] || 0;\n    var identifier = \"\".concat(id, \" \").concat(count);\n    idCountMap[id] = count + 1;\n    var index = getIndexByIdentifier(identifier);\n    var obj = {\n      css: item[1],\n      media: item[2],\n      sourceMap: item[3]\n    };\n\n    if (index !== -1) {\n      stylesInDom[index].references++;\n      stylesInDom[index].updater(obj);\n    } else {\n      stylesInDom.push({\n        identifier: identifier,\n        updater: addStyle(obj, options),\n        references: 1\n      });\n    }\n\n    identifiers.push(identifier);\n  }\n\n  return identifiers;\n}\n\nfunction insertStyleElement(options) {\n  var style = document.createElement('style');\n  var attributes = options.attributes || {};\n\n  if (typeof attributes.nonce === 'undefined') {\n    var nonce =  true ? __webpack_require__.nc : undefined;\n\n    if (nonce) {\n      attributes.nonce = nonce;\n    }\n  }\n\n  Object.keys(attributes).forEach(function (key) {\n    style.setAttribute(key, attributes[key]);\n  });\n\n  if (typeof options.insert === 'function') {\n    options.insert(style);\n  } else {\n    var target = getTarget(options.insert || 'head');\n\n    if (!target) {\n      throw new Error(\"Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.\");\n    }\n\n    target.appendChild(style);\n  }\n\n  return style;\n}\n\nfunction removeStyleElement(style) {\n  // istanbul ignore if\n  if (style.parentNode === null) {\n    return false;\n  }\n\n  style.parentNode.removeChild(style);\n}\n/* istanbul ignore next  */\n\n\nvar replaceText = function replaceText() {\n  var textStore = [];\n  return function replace(index, replacement) {\n    textStore[index] = replacement;\n    return textStore.filter(Boolean).join('\\n');\n  };\n}();\n\nfunction applyToSingletonTag(style, index, remove, obj) {\n  var css = remove ? '' : obj.media ? \"@media \".concat(obj.media, \" {\").concat(obj.css, \"}\") : obj.css; // For old IE\n\n  /* istanbul ignore if  */\n\n  if (style.styleSheet) {\n    style.styleSheet.cssText = replaceText(index, css);\n  } else {\n    var cssNode = document.createTextNode(css);\n    var childNodes = style.childNodes;\n\n    if (childNodes[index]) {\n      style.removeChild(childNodes[index]);\n    }\n\n    if (childNodes.length) {\n      style.insertBefore(cssNode, childNodes[index]);\n    } else {\n      style.appendChild(cssNode);\n    }\n  }\n}\n\nfunction applyToTag(style, options, obj) {\n  var css = obj.css;\n  var media = obj.media;\n  var sourceMap = obj.sourceMap;\n\n  if (media) {\n    style.setAttribute('media', media);\n  } else {\n    style.removeAttribute('media');\n  }\n\n  if (sourceMap && btoa) {\n    css += \"\\n/*# sourceMappingURL=data:application/json;base64,\".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), \" */\");\n  } // For old IE\n\n  /* istanbul ignore if  */\n\n\n  if (style.styleSheet) {\n    style.styleSheet.cssText = css;\n  } else {\n    while (style.firstChild) {\n      style.removeChild(style.firstChild);\n    }\n\n    style.appendChild(document.createTextNode(css));\n  }\n}\n\nvar singleton = null;\nvar singletonCounter = 0;\n\nfunction addStyle(obj, options) {\n  var style;\n  var update;\n  var remove;\n\n  if (options.singleton) {\n    var styleIndex = singletonCounter++;\n    style = singleton || (singleton = insertStyleElement(options));\n    update = applyToSingletonTag.bind(null, style, styleIndex, false);\n    remove = applyToSingletonTag.bind(null, style, styleIndex, true);\n  } else {\n    style = insertStyleElement(options);\n    update = applyToTag.bind(null, style, options);\n\n    remove = function remove() {\n      removeStyleElement(style);\n    };\n  }\n\n  update(obj);\n  return function updateStyle(newObj) {\n    if (newObj) {\n      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {\n        return;\n      }\n\n      update(obj = newObj);\n    } else {\n      remove();\n    }\n  };\n}\n\nmodule.exports = function (list, options) {\n  options = options || {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>\n  // tags it will allow on a page\n\n  if (!options.singleton && typeof options.singleton !== 'boolean') {\n    options.singleton = isOldIE();\n  }\n\n  list = list || [];\n  var lastIdentifiers = modulesToDom(list, options);\n  return function update(newList) {\n    newList = newList || [];\n\n    if (Object.prototype.toString.call(newList) !== '[object Array]') {\n      return;\n    }\n\n    for (var i = 0; i < lastIdentifiers.length; i++) {\n      var identifier = lastIdentifiers[i];\n      var index = getIndexByIdentifier(identifier);\n      stylesInDom[index].references--;\n    }\n\n    var newLastIdentifiers = modulesToDom(newList, options);\n\n    for (var _i = 0; _i < lastIdentifiers.length; _i++) {\n      var _identifier = lastIdentifiers[_i];\n\n      var _index = getIndexByIdentifier(_identifier);\n\n      if (stylesInDom[_index].references === 0) {\n        stylesInDom[_index].updater();\n\n        stylesInDom.splice(_index, 1);\n      }\n    }\n\n    lastIdentifiers = newLastIdentifiers;\n  };\n};\n\n//# sourceURL=webpack:///./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js?");

/***/ }),

/***/ "./src/app/app.css":
/*!*************************!*\
  !*** ./src/app/app.css ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js!./app.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/app.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/app.css?");

/***/ }),

/***/ "./src/app/app.js":
/*!************************!*\
  !*** ./src/app/app.js ***!
  \************************/
/*! exports provided: App */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"App\", function() { return App; });\n/* harmony import */ var _app_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app.css */ \"./src/app/app.css\");\n/* harmony import */ var _app_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_app_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./header */ \"./src/app/header/index.js\");\n/* harmony import */ var _main__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./main */ \"./src/app/main/index.js\");\n/* harmony import */ var _footer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./footer */ \"./src/app/footer/index.js\");\n/* harmony import */ var _enter_modal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./enter-modal */ \"./src/app/enter-modal/index.js\");\n/* harmony import */ var _register_modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./register-modal */ \"./src/app/register-modal/index.js\");\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nfunction App() {\r\n    const app = document.createElement('div');\r\n\r\n    app.classList.add('app');\r\n\r\n    app.append(Object(_enter_modal__WEBPACK_IMPORTED_MODULE_4__[\"EnterModal\"])(), Object(_register_modal__WEBPACK_IMPORTED_MODULE_5__[\"RegisterModal\"])(), Object(_header__WEBPACK_IMPORTED_MODULE_1__[\"Header\"])(), Object(_main__WEBPACK_IMPORTED_MODULE_2__[\"Main\"])(), Object(_footer__WEBPACK_IMPORTED_MODULE_3__[\"Footer\"])());\r\n\r\n    return app;\r\n}\r\n\r\n\n\n//# sourceURL=webpack:///./src/app/app.js?");

/***/ }),

/***/ "./src/app/enter-modal/enter-modal.css":
/*!*********************************************!*\
  !*** ./src/app/enter-modal/enter-modal.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js!./enter-modal.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/enter-modal.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/enter-modal/enter-modal.css?");

/***/ }),

/***/ "./src/app/enter-modal/enter-modal.js":
/*!********************************************!*\
  !*** ./src/app/enter-modal/enter-modal.js ***!
  \********************************************/
/*! exports provided: EnterModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"EnterModal\", function() { return EnterModal; });\n/* harmony import */ var _enter_modal_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./enter-modal.css */ \"./src/app/enter-modal/enter-modal.css\");\n/* harmony import */ var _enter_modal_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_enter_modal_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _modal_content__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modal-content */ \"./src/app/enter-modal/modal-content/index.js\");\n/* harmony import */ var _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../server-requests/server-requests */ \"./src/app/server-requests/server-requests.js\");\n/* harmony import */ var _app_header_container_search_label_search_input_group_search_getAutocompleteTitles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../app/header/container/search-label/search-input-group/search/getAutocompleteTitles */ \"./src/app/header/container/search-label/search-input-group/search/getAutocompleteTitles.js\");\n/* harmony import */ var _main_tasks_filtration_all_tasks_table_tbody_get_tr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../main/tasks-filtration/all-tasks/table/tbody/get-tr */ \"./src/app/main/tasks-filtration/all-tasks/table/tbody/get-tr.js\");\n/* harmony import */ var _task_helpers_changeDoneTitle__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../task helpers/changeDoneTitle */ \"./src/app/task helpers/changeDoneTitle.js\");\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nfunction EnterModal() {\r\n    const enterModal = document.createElement('div');\r\n  \r\n    enterModal.classList.add('enter-modal');\r\n\r\n    enterModal.append(Object(_modal_content__WEBPACK_IMPORTED_MODULE_1__[\"ModalContent\"])());\r\n\r\n    if(localStorage.getItem('id') !== null) {\r\n        enterModal.style.display = 'none';\r\n        const id = localStorage.getItem('id');\r\n            \r\n        new _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_2__[\"ServerRequests\"]().getUserTasks(id)\r\n        .then(tasks => {\r\n            const tbody = document.querySelector('.tbody');\r\n            tbody.style.display = 'table-rows';\r\n            Object(_app_header_container_search_label_search_input_group_search_getAutocompleteTitles__WEBPACK_IMPORTED_MODULE_3__[\"getAutocompleteTitles\"])(new _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_2__[\"ServerRequests\"]().getUserTasks(id));\r\n            tasks.forEach(task => {\r\n                const userNameInput = document.querySelector('.user-name-input');\r\n                const login = localStorage.getItem('login');\r\n\r\n                tbody.append(Object(_main_tasks_filtration_all_tasks_table_tbody_get_tr__WEBPACK_IMPORTED_MODULE_4__[\"getTr\"])(task))\r\n                \r\n                Object(_task_helpers_changeDoneTitle__WEBPACK_IMPORTED_MODULE_5__[\"changeDoneTitle\"])(task);\r\n                \r\n                userNameInput.setAttribute('placeholder', `${login}`);\r\n            })\r\n        })\r\n    } else {\r\n        enterModal.style.display = 'block';\r\n    }\r\n\r\n    return enterModal;\r\n}\n\n//# sourceURL=webpack:///./src/app/enter-modal/enter-modal.js?");

/***/ }),

/***/ "./src/app/enter-modal/index.js":
/*!**************************************!*\
  !*** ./src/app/enter-modal/index.js ***!
  \**************************************/
/*! exports provided: EnterModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _enter_modal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./enter-modal */ \"./src/app/enter-modal/enter-modal.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"EnterModal\", function() { return _enter_modal__WEBPACK_IMPORTED_MODULE_0__[\"EnterModal\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/index.js?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/index.js":
/*!****************************************************!*\
  !*** ./src/app/enter-modal/modal-content/index.js ***!
  \****************************************************/
/*! exports provided: ModalContent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modal_content__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-content */ \"./src/app/enter-modal/modal-content/modal-content.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"ModalContent\", function() { return _modal_content__WEBPACK_IMPORTED_MODULE_0__[\"ModalContent\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/index.js?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-body/enter-button/enter-button.css":
/*!************************************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-body/enter-button/enter-button.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./enter-button.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-body/enter-button/enter-button.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/enter-button/enter-button.css?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-body/enter-button/enter-button.js":
/*!***********************************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-body/enter-button/enter-button.js ***!
  \***********************************************************************************/
/*! exports provided: EnterButton */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"EnterButton\", function() { return EnterButton; });\n/* harmony import */ var _enter_button_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./enter-button.css */ \"./src/app/enter-modal/modal-content/modal-body/enter-button/enter-button.css\");\n/* harmony import */ var _enter_button_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_enter_button_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction EnterButton() {\r\n    const enterButton = document.createElement('button');\r\n    const content = `\r\n        <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i>\r\n        enter\r\n    `;\r\n\r\n    enterButton.classList.add('btn', 'btn-primary', 'enter-button', 'w-100');\r\n\r\n    enterButton.setAttribute('type', 'submit');\r\n\r\n    enterButton.innerHTML = content;\r\n       \r\n\r\n    return enterButton;\r\n}\r\n\r\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/enter-button/enter-button.js?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-body/enter-button/index.js":
/*!****************************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-body/enter-button/index.js ***!
  \****************************************************************************/
/*! exports provided: EnterButton */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _enter_button__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./enter-button */ \"./src/app/enter-modal/modal-content/modal-body/enter-button/enter-button.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"EnterButton\", function() { return _enter_button__WEBPACK_IMPORTED_MODULE_0__[\"EnterButton\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/enter-button/index.js?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-body/error-text/error-text.css":
/*!********************************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-body/error-text/error-text.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./error-text.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-body/error-text/error-text.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/error-text/error-text.css?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-body/error-text/error-text.js":
/*!*******************************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-body/error-text/error-text.js ***!
  \*******************************************************************************/
/*! exports provided: ErrorText */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ErrorText\", function() { return ErrorText; });\n/* harmony import */ var _error_text_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./error-text.css */ \"./src/app/enter-modal/modal-content/modal-body/error-text/error-text.css\");\n/* harmony import */ var _error_text_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_error_text_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction ErrorText() {\r\n    const errorText = document.createElement('p');\r\n\r\n    errorText.classList.add('error-text', 'text-danger');\r\n\r\n    return errorText;\r\n}\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/error-text/error-text.js?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-body/error-text/index.js":
/*!**************************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-body/error-text/index.js ***!
  \**************************************************************************/
/*! exports provided: ErrorText */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _error_text__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./error-text */ \"./src/app/enter-modal/modal-content/modal-body/error-text/error-text.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"ErrorText\", function() { return _error_text__WEBPACK_IMPORTED_MODULE_0__[\"ErrorText\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/error-text/index.js?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-body/index.js":
/*!***************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-body/index.js ***!
  \***************************************************************/
/*! exports provided: ModalBody */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modal_body__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-body */ \"./src/app/enter-modal/modal-content/modal-body/modal-body.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"ModalBody\", function() { return _modal_body__WEBPACK_IMPORTED_MODULE_0__[\"ModalBody\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/index.js?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-body/login-label/index.js":
/*!***************************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-body/login-label/index.js ***!
  \***************************************************************************/
/*! exports provided: LoginLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _login_label__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login-label */ \"./src/app/enter-modal/modal-content/modal-body/login-label/login-label.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"LoginLabel\", function() { return _login_label__WEBPACK_IMPORTED_MODULE_0__[\"LoginLabel\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/login-label/index.js?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-body/login-label/login-label.css":
/*!**********************************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-body/login-label/login-label.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./login-label.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-body/login-label/login-label.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/login-label/login-label.css?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-body/login-label/login-label.js":
/*!*********************************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-body/login-label/login-label.js ***!
  \*********************************************************************************/
/*! exports provided: LoginLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"LoginLabel\", function() { return LoginLabel; });\n/* harmony import */ var _login_label_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login-label.css */ \"./src/app/enter-modal/modal-content/modal-body/login-label/login-label.css\");\n/* harmony import */ var _login_label_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_login_label_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction LoginLabel() {\r\n    const loginLabel = document.createElement('label');\r\n    const title = 'login:';\r\n    const loginInput = document.createElement('input');\r\n\r\n    loginLabel.classList.add('login-label', 'w-100');\r\n    loginInput.classList.add('form-control', 'login');\r\n\r\n    loginInput.setAttribute('type', 'text');\r\n\r\n    loginLabel.append(title, loginInput);\r\n\r\n    return loginLabel;\r\n}\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/login-label/login-label.js?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-body/modal-body.css":
/*!*********************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-body/modal-body.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./modal-body.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-body/modal-body.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/modal-body.css?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-body/modal-body.js":
/*!********************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-body/modal-body.js ***!
  \********************************************************************/
/*! exports provided: ModalBody */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ModalBody\", function() { return ModalBody; });\n/* harmony import */ var _modal_body_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-body.css */ \"./src/app/enter-modal/modal-content/modal-body/modal-body.css\");\n/* harmony import */ var _modal_body_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_body_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _login_label__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login-label */ \"./src/app/enter-modal/modal-content/modal-body/login-label/index.js\");\n/* harmony import */ var _password_label__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./password-label */ \"./src/app/enter-modal/modal-content/modal-body/password-label/index.js\");\n/* harmony import */ var _enter_button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./enter-button */ \"./src/app/enter-modal/modal-content/modal-body/enter-button/index.js\");\n/* harmony import */ var _error_text__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./error-text */ \"./src/app/enter-modal/modal-content/modal-body/error-text/index.js\");\n/* harmony import */ var _register__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./register */ \"./src/app/enter-modal/modal-content/modal-body/register/index.js\");\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nfunction ModalBody() {\r\n    const modalBody = document.createElement('div');\r\n\r\n    modalBody.classList.add('form-group', 'modal-body', 'd-flex', 'flex-wrap');\r\n\r\n    modalBody.append(Object(_login_label__WEBPACK_IMPORTED_MODULE_1__[\"LoginLabel\"])(), Object(_password_label__WEBPACK_IMPORTED_MODULE_2__[\"PasswordLabel\"])(), Object(_enter_button__WEBPACK_IMPORTED_MODULE_3__[\"EnterButton\"])(), Object(_error_text__WEBPACK_IMPORTED_MODULE_4__[\"ErrorText\"])(), Object(_register__WEBPACK_IMPORTED_MODULE_5__[\"Register\"])());\r\n\r\n    return modalBody;\r\n}\r\n\r\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/modal-body.js?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-body/password-label/index.js":
/*!******************************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-body/password-label/index.js ***!
  \******************************************************************************/
/*! exports provided: PasswordLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _password_label__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./password-label */ \"./src/app/enter-modal/modal-content/modal-body/password-label/password-label.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"PasswordLabel\", function() { return _password_label__WEBPACK_IMPORTED_MODULE_0__[\"PasswordLabel\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/password-label/index.js?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-body/password-label/password-label.css":
/*!****************************************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-body/password-label/password-label.css ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./password-label.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-body/password-label/password-label.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/password-label/password-label.css?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-body/password-label/password-label.js":
/*!***************************************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-body/password-label/password-label.js ***!
  \***************************************************************************************/
/*! exports provided: PasswordLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"PasswordLabel\", function() { return PasswordLabel; });\n/* harmony import */ var _password_label_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./password-label.css */ \"./src/app/enter-modal/modal-content/modal-body/password-label/password-label.css\");\n/* harmony import */ var _password_label_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_password_label_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction PasswordLabel() {\r\n    const passwordLabel = document.createElement('label');\r\n    const title = 'password:';\r\n    const passwordInput = document.createElement('input');\r\n\r\n    passwordLabel.classList.add('password-label', 'w-100');\r\n    passwordInput.classList.add('form-control', 'password');\r\n\r\n    passwordInput.setAttribute('type', 'password');\r\n\r\n    passwordLabel.append(title, passwordInput);\r\n\r\n    return passwordLabel;\r\n}\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/password-label/password-label.js?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-body/register/index.js":
/*!************************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-body/register/index.js ***!
  \************************************************************************/
/*! exports provided: Register */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _register__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./register */ \"./src/app/enter-modal/modal-content/modal-body/register/register.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Register\", function() { return _register__WEBPACK_IMPORTED_MODULE_0__[\"Register\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/register/index.js?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-body/register/register.css":
/*!****************************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-body/register/register.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./register.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-body/register/register.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/register/register.css?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-body/register/register.js":
/*!***************************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-body/register/register.js ***!
  \***************************************************************************/
/*! exports provided: Register */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Register\", function() { return Register; });\n/* harmony import */ var _register_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./register.css */ \"./src/app/enter-modal/modal-content/modal-body/register/register.css\");\n/* harmony import */ var _register_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_register_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction Register() {\r\n    const register = document.createElement('p');\r\n\r\n    register.classList.add('register', 'text-primary');\r\n\r\n    register.innerHTML = 'register now';\r\n\r\n    register.addEventListener('click', openRgisterModal);\r\n\r\n    function openRgisterModal() {\r\n        const registerModal = document.querySelector('.register-modal');\r\n\r\n        registerModal.style.display = 'block';\r\n    }\r\n\r\n    return register;\r\n}\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-body/register/register.js?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-content.css":
/*!*************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-content.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js!./modal-content.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-content.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-content.css?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-content.js":
/*!************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-content.js ***!
  \************************************************************/
/*! exports provided: ModalContent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ModalContent\", function() { return ModalContent; });\n/* harmony import */ var _modal_content_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-content.css */ \"./src/app/enter-modal/modal-content/modal-content.css\");\n/* harmony import */ var _modal_content_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_content_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _modal_header_index___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modal-header/index. */ \"./src/app/enter-modal/modal-content/modal-header/index..js\");\n/* harmony import */ var _modal_body__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modal-body */ \"./src/app/enter-modal/modal-content/modal-body/index.js\");\n/* harmony import */ var _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../server-requests/server-requests */ \"./src/app/server-requests/server-requests.js\");\n/* harmony import */ var _main_tasks_filtration_all_tasks_table_tbody_get_tr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../main/tasks-filtration/all-tasks/table/tbody/get-tr */ \"./src/app/main/tasks-filtration/all-tasks/table/tbody/get-tr.js\");\n/* harmony import */ var _task_helpers_changeDoneTitle__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../task helpers/changeDoneTitle */ \"./src/app/task helpers/changeDoneTitle.js\");\n/* harmony import */ var _header_container_search_label_search_input_group_search_getAutocompleteTitles__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../header/container/search-label/search-input-group/search/getAutocompleteTitles */ \"./src/app/header/container/search-label/search-input-group/search/getAutocompleteTitles.js\");\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nfunction ModalContent() {\r\n    const modalContent= document.createElement('form');\r\n\r\n    modalContent.classList.add('enter-form', 'modal-content');\r\n\r\n    modalContent.append(Object(_modal_header_index___WEBPACK_IMPORTED_MODULE_1__[\"ModalHeader\"])(), Object(_modal_body__WEBPACK_IMPORTED_MODULE_2__[\"ModalBody\"])());\r\n\r\n    modalContent.addEventListener('submit', enterInToDo);\r\n\r\n\r\n    function enterInToDo(ev) {\r\n        ev.preventDefault();\r\n        const login = document.querySelector('.login').value;\r\n        const password = document.querySelector('.password').value;\r\n        const userData = {\r\n            login,\r\n            password\r\n        }\r\n\r\n        fetch('http://localhost:2000/users/is-logged-in', {\r\n        method: 'POST',\r\n        headers: {\r\n            'Content-Type': 'application/json'\r\n        },\r\n        body: JSON.stringify(userData)\r\n    })\r\n    .then(res => res.json())\r\n    .then(userData => {\r\n        if(userData !== null) {\r\n            localStorage.setItem('login', `${userData.login}`);\r\n            localStorage.setItem('id', `${userData.id}`);\r\n        } \r\n        return userData.id;\r\n    })\r\n    \r\n    .then(id => {\r\n        const enterModal = document.querySelector('.enter-modal');\r\n        \r\n        enterModal.style.display = 'none';\r\n\r\n        new _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_3__[\"ServerRequests\"]().getUserTasks(id)\r\n        .then(tasks => {\r\n            const tbody = document.querySelector('.tbody');\r\n\r\n            Object(_header_container_search_label_search_input_group_search_getAutocompleteTitles__WEBPACK_IMPORTED_MODULE_6__[\"getAutocompleteTitles\"])(new _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_3__[\"ServerRequests\"]().getUserTasks(id));\r\n\r\n            tasks.forEach(task => {\r\n                tbody.append(Object(_main_tasks_filtration_all_tasks_table_tbody_get_tr__WEBPACK_IMPORTED_MODULE_4__[\"getTr\"])(task))\r\n                Object(_task_helpers_changeDoneTitle__WEBPACK_IMPORTED_MODULE_5__[\"changeDoneTitle\"])(task);\r\n            })   \r\n            const userNameInput = document.querySelector('.user-name-input');\r\n                \r\n            userNameInput.setAttribute('placeholder', `${login}`);\r\n        })\r\n    })\r\n    .catch(() => {\r\n        const errorText = document.querySelector('.error-text');\r\n        const inputLogin = document.querySelector('.login');\r\n        const inputPassword = document.querySelector('.password');\r\n\r\n        \r\n\r\n        errorText.innerHTML = `The username or password you entered is incorrect. \r\n        Enter the correct data or register`;\r\n\r\n        inputLogin.addEventListener('keydown', delErrorText);\r\n        inputPassword.addEventListener('keydown', delErrorText);\r\n\r\n        function delErrorText() {\r\n            errorText.innerHTML = null;\r\n        }\r\n    })    \r\n\r\n        // new ServerRequests().getUsers()\r\n        // .then(users => {\r\n           \r\n        // const user = users.find((user) => {\r\n        //     const userLogin = user.login;\r\n        //     const userPassword = user.password;\r\n        //     const userid = user.userId;\r\n        //     const userNameInput = document.querySelector('.user-name-input');\r\n\r\n        //     userNameInput.setAttribute('placeholder', `${userLogin}`);\r\n        //     userNameInput.setAttribute('data-id', `${userid}`);\r\n\r\n        //     localStorage.setItem(`login`,`${userLogin}`);\r\n        //     localStorage.setItem(`id`, `${userid}`);\r\n        //     return userLogin === login && userPassword === password;\r\n        // })\r\n            \r\n        //    return user.userId\r\n        // })\r\n        // .then(id => {\r\n        //     const enterModal = document.querySelector('.enter-modal');\r\n        \r\n        //     enterModal.style.display = 'none';\r\n           \r\n        //     new ServerRequests().getUserTasks(id)\r\n        //     .then(tasks => {\r\n        //         const tbody = document.querySelector('.tbody');\r\n                             \r\n        //         getAutocompleteTitles(new ServerRequests().getUserTasks(id));\r\n        //         tasks.forEach(task => {\r\n        //             tbody.append(getTr(task))\r\n                    \r\n        //             changeDoneTitle(task);\r\n        //         })\r\n        //     })\r\n        // })\r\n        // .catch(() => {\r\n        //     const errorText = document.querySelector('.error-text');\r\n        //     const inputLogin = document.querySelector('.login');\r\n        //     const inputPassword = document.querySelector('.password');\r\n\r\n        //     errorText.innerHTML = `The username or password you entered is incorrect. \r\n        //     Enter the correct data or register`;\r\n\r\n        //     inputLogin.addEventListener('keydown', delErrorText);\r\n        //     inputPassword.addEventListener('keydown', delErrorText);\r\n\r\n        //     function delErrorText() {\r\n        //         errorText.innerHTML = null;\r\n        //     }\r\n        // })\r\n    }\r\n    return modalContent;\r\n}\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-content.js?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-header/index..js":
/*!******************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-header/index..js ***!
  \******************************************************************/
/*! exports provided: ModalHeader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modal_header__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-header */ \"./src/app/enter-modal/modal-content/modal-header/modal-header.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"ModalHeader\", function() { return _modal_header__WEBPACK_IMPORTED_MODULE_0__[\"ModalHeader\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-header/index..js?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-header/modal-header.css":
/*!*************************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-header/modal-header.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./modal-header.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/enter-modal/modal-content/modal-header/modal-header.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-header/modal-header.css?");

/***/ }),

/***/ "./src/app/enter-modal/modal-content/modal-header/modal-header.js":
/*!************************************************************************!*\
  !*** ./src/app/enter-modal/modal-content/modal-header/modal-header.js ***!
  \************************************************************************/
/*! exports provided: ModalHeader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ModalHeader\", function() { return ModalHeader; });\n/* harmony import */ var _modal_header_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-header.css */ \"./src/app/enter-modal/modal-content/modal-header/modal-header.css\");\n/* harmony import */ var _modal_header_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_header_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction ModalHeader() {\r\n    const modalHeader = document.createElement('div');\r\n    const content = `\r\n        <h2>Sing in/Register</h2>\r\n    `;\r\n\r\n    modalHeader.classList.add('modal-header', 'bg-info');\r\n\r\n    modalHeader.innerHTML = content;\r\n\r\n    return modalHeader;\r\n}\r\n\r\n\n\n//# sourceURL=webpack:///./src/app/enter-modal/modal-content/modal-header/modal-header.js?");

/***/ }),

/***/ "./src/app/footer/footer.css":
/*!***********************************!*\
  !*** ./src/app/footer/footer.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js!./footer.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/footer/footer.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/footer/footer.css?");

/***/ }),

/***/ "./src/app/footer/footer.js":
/*!**********************************!*\
  !*** ./src/app/footer/footer.js ***!
  \**********************************/
/*! exports provided: Footer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Footer\", function() { return Footer; });\n/* harmony import */ var _footer_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./footer.css */ \"./src/app/footer/footer.css\");\n/* harmony import */ var _footer_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_footer_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction Footer() {\r\n    const footer = document.createElement('footer');\r\n\r\n    footer.classList.add('footer', 'bg-info', 'text-center', 'text-white');\r\n\r\n    footer.innerHTML = `&copy; Roman Panfilov, 2020`;\r\n\r\n    return footer;\r\n}\n\n//# sourceURL=webpack:///./src/app/footer/footer.js?");

/***/ }),

/***/ "./src/app/footer/index.js":
/*!*********************************!*\
  !*** ./src/app/footer/index.js ***!
  \*********************************/
/*! exports provided: Footer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _footer_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./footer.js */ \"./src/app/footer/footer.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Footer\", function() { return _footer_js__WEBPACK_IMPORTED_MODULE_0__[\"Footer\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/footer/index.js?");

/***/ }),

/***/ "./src/app/header/container/container.css":
/*!************************************************!*\
  !*** ./src/app/header/container/container.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js!./container.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/header/container/container.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/header/container/container.css?");

/***/ }),

/***/ "./src/app/header/container/container.js":
/*!***********************************************!*\
  !*** ./src/app/header/container/container.js ***!
  \***********************************************/
/*! exports provided: Container */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Container\", function() { return Container; });\n/* harmony import */ var _container_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./container.css */ \"./src/app/header/container/container.css\");\n/* harmony import */ var _container_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_container_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _logo__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./logo */ \"./src/app/header/container/logo/index.js\");\n/* harmony import */ var _user__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user */ \"./src/app/header/container/user/index.js\");\n/* harmony import */ var _search_label__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./search-label */ \"./src/app/header/container/search-label/index.js\");\n\r\n\r\n\r\n\r\n\r\n\r\nfunction Container() {\r\n    const container = document.createElement('div');\r\n\r\n    container.classList.add('container', 'd-flex', 'justify-content-between', 'flex-wrap');\r\n\r\n    container.append(Object(_logo__WEBPACK_IMPORTED_MODULE_1__[\"Logo\"])(), Object(_search_label__WEBPACK_IMPORTED_MODULE_3__[\"SearchLabel\"])(), Object(_user__WEBPACK_IMPORTED_MODULE_2__[\"User\"])());\r\n\r\n    return container;\r\n}\n\n//# sourceURL=webpack:///./src/app/header/container/container.js?");

/***/ }),

/***/ "./src/app/header/container/index.js":
/*!*******************************************!*\
  !*** ./src/app/header/container/index.js ***!
  \*******************************************/
/*! exports provided: Container */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _container_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./container.js */ \"./src/app/header/container/container.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Container\", function() { return _container_js__WEBPACK_IMPORTED_MODULE_0__[\"Container\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/header/container/index.js?");

/***/ }),

/***/ "./src/app/header/container/logo/index.js":
/*!************************************************!*\
  !*** ./src/app/header/container/logo/index.js ***!
  \************************************************/
/*! exports provided: Logo */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _logo_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./logo.js */ \"./src/app/header/container/logo/logo.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Logo\", function() { return _logo_js__WEBPACK_IMPORTED_MODULE_0__[\"Logo\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/header/container/logo/index.js?");

/***/ }),

/***/ "./src/app/header/container/logo/logo.css":
/*!************************************************!*\
  !*** ./src/app/header/container/logo/logo.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./logo.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/header/container/logo/logo.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/header/container/logo/logo.css?");

/***/ }),

/***/ "./src/app/header/container/logo/logo.js":
/*!***********************************************!*\
  !*** ./src/app/header/container/logo/logo.js ***!
  \***********************************************/
/*! exports provided: Logo */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Logo\", function() { return Logo; });\n/* harmony import */ var _logo_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./logo.css */ \"./src/app/header/container/logo/logo.css\");\n/* harmony import */ var _logo_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_logo_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _images_logo_png__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../../images/logo.png */ \"./src/images/logo.png\");\n\r\n\r\n\r\n\r\n\r\nfunction Logo() {\r\n    \r\n    // const logo = document.createElement('img');\r\n\r\n    \r\n\r\n\r\n    const logo = new Image();\r\n    logo.src = _images_logo_png__WEBPACK_IMPORTED_MODULE_1__[\"default\"];\r\n    logo.setAttribute('alt', 'logo');\r\n\r\n\r\n    \r\n\r\n    return logo;\r\n}\n\n//# sourceURL=webpack:///./src/app/header/container/logo/logo.js?");

/***/ }),

/***/ "./src/app/header/container/search-label/index.js":
/*!********************************************************!*\
  !*** ./src/app/header/container/search-label/index.js ***!
  \********************************************************/
/*! exports provided: SearchLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _search_label__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./search-label */ \"./src/app/header/container/search-label/search-label.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"SearchLabel\", function() { return _search_label__WEBPACK_IMPORTED_MODULE_0__[\"SearchLabel\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/header/container/search-label/index.js?");

/***/ }),

/***/ "./src/app/header/container/search-label/search-input-group/index.js":
/*!***************************************************************************!*\
  !*** ./src/app/header/container/search-label/search-input-group/index.js ***!
  \***************************************************************************/
/*! exports provided: SearchInputGroup */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _search_input_group__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./search-input-group */ \"./src/app/header/container/search-label/search-input-group/search-input-group.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"SearchInputGroup\", function() { return _search_input_group__WEBPACK_IMPORTED_MODULE_0__[\"SearchInputGroup\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/header/container/search-label/search-input-group/index.js?");

/***/ }),

/***/ "./src/app/header/container/search-label/search-input-group/input-group-prepend/index.js":
/*!***********************************************************************************************!*\
  !*** ./src/app/header/container/search-label/search-input-group/input-group-prepend/index.js ***!
  \***********************************************************************************************/
/*! exports provided: InputGroupPrepend */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _input_group_prepend__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./input-group-prepend */ \"./src/app/header/container/search-label/search-input-group/input-group-prepend/input-group-prepend.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"InputGroupPrepend\", function() { return _input_group_prepend__WEBPACK_IMPORTED_MODULE_0__[\"InputGroupPrepend\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/header/container/search-label/search-input-group/input-group-prepend/index.js?");

/***/ }),

/***/ "./src/app/header/container/search-label/search-input-group/input-group-prepend/input-group-prepend.css":
/*!**************************************************************************************************************!*\
  !*** ./src/app/header/container/search-label/search-input-group/input-group-prepend/input-group-prepend.css ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader/dist/cjs.js!./input-group-prepend.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/header/container/search-label/search-input-group/input-group-prepend/input-group-prepend.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/header/container/search-label/search-input-group/input-group-prepend/input-group-prepend.css?");

/***/ }),

/***/ "./src/app/header/container/search-label/search-input-group/input-group-prepend/input-group-prepend.js":
/*!*************************************************************************************************************!*\
  !*** ./src/app/header/container/search-label/search-input-group/input-group-prepend/input-group-prepend.js ***!
  \*************************************************************************************************************/
/*! exports provided: InputGroupPrepend */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"InputGroupPrepend\", function() { return InputGroupPrepend; });\n/* harmony import */ var _input_group_prepend_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./input-group-prepend.css */ \"./src/app/header/container/search-label/search-input-group/input-group-prepend/input-group-prepend.css\");\n/* harmony import */ var _input_group_prepend_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_input_group_prepend_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction InputGroupPrepend() {\r\n    const inputGroupPrepend = document.createElement('div');\r\n    const content = `\r\n        <div class=\"input-group-text\">\r\n            <i class=\"fa fa-search\" aria-hidden=\"true\"></i>\r\n        </div>\r\n    `;\r\n\r\n    inputGroupPrepend.classList.add('input-group-prepend', 'button-search');\r\n\r\n    inputGroupPrepend.innerHTML = content;\r\n\r\n    inputGroupPrepend.addEventListener('click', openSearchTaskInTable);\r\n\r\n    function openSearchTaskInTable() {\r\n        const trTableRows = document.querySelectorAll('.tbody tr');\r\n        const search = document.querySelector('.search');\r\n        trTableRows.forEach(tr => {\r\n            if(tr.childNodes[0].textContent !== search.value) {\r\n            tr.style.display = 'none';\r\n            } else {\r\n                tr.style.display = 'table-row'\r\n            }\r\n\r\n            if(search.value === '') {\r\n                tr.style.display = 'table-row'\r\n            }\r\n        })\r\n    }\r\n\r\n    return inputGroupPrepend;\r\n}\n\n//# sourceURL=webpack:///./src/app/header/container/search-label/search-input-group/input-group-prepend/input-group-prepend.js?");

/***/ }),

/***/ "./src/app/header/container/search-label/search-input-group/search-input-group.css":
/*!*****************************************************************************************!*\
  !*** ./src/app/header/container/search-label/search-input-group/search-input-group.css ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./search-input-group.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/header/container/search-label/search-input-group/search-input-group.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/header/container/search-label/search-input-group/search-input-group.css?");

/***/ }),

/***/ "./src/app/header/container/search-label/search-input-group/search-input-group.js":
/*!****************************************************************************************!*\
  !*** ./src/app/header/container/search-label/search-input-group/search-input-group.js ***!
  \****************************************************************************************/
/*! exports provided: SearchInputGroup */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"SearchInputGroup\", function() { return SearchInputGroup; });\n/* harmony import */ var _search_input_group_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./search-input-group.css */ \"./src/app/header/container/search-label/search-input-group/search-input-group.css\");\n/* harmony import */ var _search_input_group_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_search_input_group_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _search_search__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./search/search */ \"./src/app/header/container/search-label/search-input-group/search/search.js\");\n/* harmony import */ var _input_group_prepend__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./input-group-prepend */ \"./src/app/header/container/search-label/search-input-group/input-group-prepend/index.js\");\n\r\n\r\n\r\n\r\nfunction SearchInputGroup() {\r\n    const inputGroup = document.createElement('div');\r\n\r\n    inputGroup.classList.add('input-group', 'mb-2');\r\n\r\n    inputGroup.append(Object(_search_search__WEBPACK_IMPORTED_MODULE_1__[\"Search\"])(), Object(_input_group_prepend__WEBPACK_IMPORTED_MODULE_2__[\"InputGroupPrepend\"])());\r\n\r\n    return inputGroup;\r\n}\r\n\n\n//# sourceURL=webpack:///./src/app/header/container/search-label/search-input-group/search-input-group.js?");

/***/ }),

/***/ "./src/app/header/container/search-label/search-input-group/search/getAutocompleteTitles.js":
/*!**************************************************************************************************!*\
  !*** ./src/app/header/container/search-label/search-input-group/search/getAutocompleteTitles.js ***!
  \**************************************************************************************************/
/*! exports provided: getAutocompleteTitles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getAutocompleteTitles\", function() { return getAutocompleteTitles; });\nfunction getAutocompleteTitles(getTasks) {\r\n    \r\n\r\n    getTasks\r\n    .then(tasks => tasks.map(task => task.title))      \r\n    .then(source =>  $('.search').autocomplete({\r\n        source,\r\n        minLength: 3,\r\n        select: function( event, ui ) {\r\n            const rows = document.querySelectorAll('.all-tasks tbody tr');\r\n            const search = document.querySelector('.search');\r\n            rows.forEach(tr => {\r\n                if(tr.childNodes[0].textContent !== ui.item.value) {\r\n                    tr.style.display = 'none';\r\n            } else {\r\n                tr.style.display = 'table-row'\r\n            }\r\n    \r\n            if(ui.item.value === '') {\r\n                tr.style.display = 'table-row'\r\n            }\r\n              \r\n            })\r\n\r\n        }\r\n    }))\r\n}\n\n//# sourceURL=webpack:///./src/app/header/container/search-label/search-input-group/search/getAutocompleteTitles.js?");

/***/ }),

/***/ "./src/app/header/container/search-label/search-input-group/search/search.css":
/*!************************************************************************************!*\
  !*** ./src/app/header/container/search-label/search-input-group/search/search.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader/dist/cjs.js!./search.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/header/container/search-label/search-input-group/search/search.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/header/container/search-label/search-input-group/search/search.css?");

/***/ }),

/***/ "./src/app/header/container/search-label/search-input-group/search/search.js":
/*!***********************************************************************************!*\
  !*** ./src/app/header/container/search-label/search-input-group/search/search.js ***!
  \***********************************************************************************/
/*! exports provided: Search */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Search\", function() { return Search; });\n/* harmony import */ var _search_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./search.css */ \"./src/app/header/container/search-label/search-input-group/search/search.css\");\n/* harmony import */ var _search_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_search_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../server-requests/server-requests */ \"./src/app/server-requests/server-requests.js\");\n/* harmony import */ var _getAutocompleteTitles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./getAutocompleteTitles */ \"./src/app/header/container/search-label/search-input-group/search/getAutocompleteTitles.js\");\n\r\n\r\n\r\n\r\n\r\nfunction Search() {\r\n    const search = document.createElement('input');\r\n\r\n    search.classList.add('search', 'form-control');\r\n\r\n    search.setAttribute('type', 'search');\r\n    const userId = localStorage.getItem('id');\r\n\r\n    const getTasks = new _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_1__[\"ServerRequests\"]().getUserTasks(userId);\r\n\r\n    \r\n    Object(_getAutocompleteTitles__WEBPACK_IMPORTED_MODULE_2__[\"getAutocompleteTitles\"])(getTasks);\r\n\r\n    search.addEventListener('keypress', openSearchTaskInTableEnter);\r\n    \r\n    function openSearchTaskInTableEnter(ev) {\r\n        if(ev.keyCode === 13) {\r\n           ev.preventDefault();\r\n           openSearchTaskInTable();\r\n       }\r\n   }\r\n\r\n   function openSearchTaskInTable() {\r\n        const trTableRows = document.querySelectorAll('.tbody tr');\r\n        trTableRows.forEach(tr => {\r\n            if(search.value === '') {\r\n                tr.style.display = 'table-row'\r\n            }\r\n\r\n            if(tr.childNodes[0].textContent.includes(`${search.value}`) == false) {\r\n                tr.style.display = 'none';\r\n            } else {\r\n                tr.style.display = 'table-row'\r\n            }\r\n        })\r\n    }\r\n    return search;\r\n}\n\n//# sourceURL=webpack:///./src/app/header/container/search-label/search-input-group/search/search.js?");

/***/ }),

/***/ "./src/app/header/container/search-label/search-label.css":
/*!****************************************************************!*\
  !*** ./src/app/header/container/search-label/search-label.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./search-label.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/header/container/search-label/search-label.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/header/container/search-label/search-label.css?");

/***/ }),

/***/ "./src/app/header/container/search-label/search-label.js":
/*!***************************************************************!*\
  !*** ./src/app/header/container/search-label/search-label.js ***!
  \***************************************************************/
/*! exports provided: SearchLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"SearchLabel\", function() { return SearchLabel; });\n/* harmony import */ var _search_label_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./search-label.css */ \"./src/app/header/container/search-label/search-label.css\");\n/* harmony import */ var _search_label_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_search_label_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _search_input_group__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./search-input-group */ \"./src/app/header/container/search-label/search-input-group/index.js\");\n/* harmony import */ var _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../server-requests/server-requests */ \"./src/app/server-requests/server-requests.js\");\n\r\n\r\n\r\n\r\nfunction SearchLabel() {\r\n    const search = document.createElement('label');\r\n\r\n    search.classList.add('search-label', 'col');\r\n\r\n    \r\n\r\n    search.append(Object(_search_input_group__WEBPACK_IMPORTED_MODULE_1__[\"SearchInputGroup\"])());\r\n\r\n\r\n    \r\n    // const getTasks = new ServerRequests().getTasks();\r\n\r\n    // function getAutocompleteTitles() {\r\n    //     getTasks\r\n    //     .then(tasks => tasks.map(task => task.title))      \r\n    //     .then(source =>  $('.search').autocomplete({\r\n    //         source,\r\n    //         minLength: 3,\r\n    //         select: function( event, ui ) {\r\n    //             const rows = document.querySelectorAll('.all-tasks tbody tr');\r\n    //             rows.forEach(tr => {\r\n    //                 if(tr.childNodes[0].textContent !== ui.item.value) {\r\n    //                         tr.style.display = 'none';\r\n    //                 } else {\r\n    //                     tr.style.display = 'table-row'\r\n    //                 }\r\n            \r\n    //                 if(ui.item.value === '') {\r\n    //                     tr.style.display = 'table-row'\r\n    //                 }\r\n    //             })\r\n    //         }\r\n    //     }))\r\n    // }\r\n    \r\n    // getAutocompleteTitles();\r\n\r\n    \r\n\r\n    return search;\r\n}\r\n\r\n\r\n\n\n//# sourceURL=webpack:///./src/app/header/container/search-label/search-label.js?");

/***/ }),

/***/ "./src/app/header/container/user/exet-button/exit-button.css":
/*!*******************************************************************!*\
  !*** ./src/app/header/container/user/exet-button/exit-button.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./exit-button.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/header/container/user/exet-button/exit-button.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/header/container/user/exet-button/exit-button.css?");

/***/ }),

/***/ "./src/app/header/container/user/exet-button/exit-button.js":
/*!******************************************************************!*\
  !*** ./src/app/header/container/user/exet-button/exit-button.js ***!
  \******************************************************************/
/*! exports provided: ExitButton */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ExitButton\", function() { return ExitButton; });\n/* harmony import */ var _exit_button_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./exit-button.css */ \"./src/app/header/container/user/exet-button/exit-button.css\");\n/* harmony import */ var _exit_button_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_exit_button_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction ExitButton() {\r\n    const exitButton = document.createElement('button');\r\n    \r\n\r\n    exitButton.classList.add('btn', 'btn-dark', 'ml-2');\r\n\r\n    exitButton.setAttribute('type', 'button');\r\n\r\n    exitButton.innerHTML = 'exit';\r\n\r\n    exitButton.addEventListener('click', openEnterModal);\r\n\r\n    function openEnterModal() {\r\n        const enterModal = document.querySelector('.enter-modal');\r\n        const tableRows = document.querySelectorAll('.tbody tr');\r\n        const inputPassword = document.querySelector('.enter-modal .password')\r\n\r\n        enterModal.style.display = 'block';\r\n\r\n        tableRows.forEach(tr => {\r\n            tr.remove();\r\n        })\r\n        localStorage.clear();\r\n        inputPassword.value = null;\r\n    }\r\n\r\n    return exitButton;\r\n}\n\n//# sourceURL=webpack:///./src/app/header/container/user/exet-button/exit-button.js?");

/***/ }),

/***/ "./src/app/header/container/user/index.js":
/*!************************************************!*\
  !*** ./src/app/header/container/user/index.js ***!
  \************************************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _user_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./user.js */ \"./src/app/header/container/user/user.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"User\", function() { return _user_js__WEBPACK_IMPORTED_MODULE_0__[\"User\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/header/container/user/index.js?");

/***/ }),

/***/ "./src/app/header/container/user/input-group-prepend/index.js":
/*!********************************************************************!*\
  !*** ./src/app/header/container/user/input-group-prepend/index.js ***!
  \********************************************************************/
/*! exports provided: InputGroupPrepend */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _input_group_prepend__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./input-group-prepend */ \"./src/app/header/container/user/input-group-prepend/input-group-prepend.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"InputGroupPrepend\", function() { return _input_group_prepend__WEBPACK_IMPORTED_MODULE_0__[\"InputGroupPrepend\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/header/container/user/input-group-prepend/index.js?");

/***/ }),

/***/ "./src/app/header/container/user/input-group-prepend/input-group-prepend.css":
/*!***********************************************************************************!*\
  !*** ./src/app/header/container/user/input-group-prepend/input-group-prepend.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./input-group-prepend.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/header/container/user/input-group-prepend/input-group-prepend.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/header/container/user/input-group-prepend/input-group-prepend.css?");

/***/ }),

/***/ "./src/app/header/container/user/input-group-prepend/input-group-prepend.js":
/*!**********************************************************************************!*\
  !*** ./src/app/header/container/user/input-group-prepend/input-group-prepend.js ***!
  \**********************************************************************************/
/*! exports provided: InputGroupPrepend */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"InputGroupPrepend\", function() { return InputGroupPrepend; });\n/* harmony import */ var _input_group_prepend_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./input-group-prepend.css */ \"./src/app/header/container/user/input-group-prepend/input-group-prepend.css\");\n/* harmony import */ var _input_group_prepend_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_input_group_prepend_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction InputGroupPrepend() {\r\n    const inputGroupPrepend = document.createElement('div');\r\n    const content = `\r\n        <div class=\"input-group-text\">\r\n            <i class=\"fa fa-user-circle-o\" aria-hidden=\"true\"></i>\r\n        </div>\r\n    `;\r\n\r\n    inputGroupPrepend.classList.add('input-group-prepend');\r\n    inputGroupPrepend.innerHTML = content;\r\n\r\n    return inputGroupPrepend;\r\n}\n\n//# sourceURL=webpack:///./src/app/header/container/user/input-group-prepend/input-group-prepend.js?");

/***/ }),

/***/ "./src/app/header/container/user/input/index.js":
/*!******************************************************!*\
  !*** ./src/app/header/container/user/input/index.js ***!
  \******************************************************/
/*! exports provided: Input */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _input__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./input */ \"./src/app/header/container/user/input/input.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Input\", function() { return _input__WEBPACK_IMPORTED_MODULE_0__[\"Input\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/header/container/user/input/index.js?");

/***/ }),

/***/ "./src/app/header/container/user/input/input.css":
/*!*******************************************************!*\
  !*** ./src/app/header/container/user/input/input.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./input.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/header/container/user/input/input.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/header/container/user/input/input.css?");

/***/ }),

/***/ "./src/app/header/container/user/input/input.js":
/*!******************************************************!*\
  !*** ./src/app/header/container/user/input/input.js ***!
  \******************************************************/
/*! exports provided: Input */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Input\", function() { return Input; });\n/* harmony import */ var _input_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./input.css */ \"./src/app/header/container/user/input/input.css\");\n/* harmony import */ var _input_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_input_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction Input() {\r\n    const input = document.createElement('input');\r\n\r\n    input.classList.add('form-control', 'user-name-input');\r\n\r\n    input.setAttribute('readonly', 'readonly');\r\n    input.setAttribute('name', 'user');\r\n\r\n    return input;\r\n}\n\n//# sourceURL=webpack:///./src/app/header/container/user/input/input.js?");

/***/ }),

/***/ "./src/app/header/container/user/user.css":
/*!************************************************!*\
  !*** ./src/app/header/container/user/user.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./user.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/header/container/user/user.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/header/container/user/user.css?");

/***/ }),

/***/ "./src/app/header/container/user/user.js":
/*!***********************************************!*\
  !*** ./src/app/header/container/user/user.js ***!
  \***********************************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"User\", function() { return User; });\n/* harmony import */ var _user_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./user.css */ \"./src/app/header/container/user/user.css\");\n/* harmony import */ var _user_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_user_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _input_group_prepend__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./input-group-prepend */ \"./src/app/header/container/user/input-group-prepend/index.js\");\n/* harmony import */ var _input__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./input */ \"./src/app/header/container/user/input/index.js\");\n/* harmony import */ var _exet_button_exit_button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./exet-button/exit-button */ \"./src/app/header/container/user/exet-button/exit-button.js\");\n\r\n\r\n\r\n\r\n\r\n\r\nfunction User() {\r\n    const user = document.createElement('label');\r\n    const inputGroup = document.createElement('div');\r\n\r\n    user.classList.add('user', 'd-flex');\r\n    inputGroup.classList.add('input-group', 'mb-2', 'mr-sm-2');\r\n    \r\n\r\n    user.append(inputGroup);\r\n\r\n    inputGroup.append(Object(_input_group_prepend__WEBPACK_IMPORTED_MODULE_1__[\"InputGroupPrepend\"])(), Object(_input__WEBPACK_IMPORTED_MODULE_2__[\"Input\"])(), Object(_exet_button_exit_button__WEBPACK_IMPORTED_MODULE_3__[\"ExitButton\"])());\r\n\r\n    return user;\r\n}\n\n//# sourceURL=webpack:///./src/app/header/container/user/user.js?");

/***/ }),

/***/ "./src/app/header/header.css":
/*!***********************************!*\
  !*** ./src/app/header/header.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js!./header.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/header/header.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/header/header.css?");

/***/ }),

/***/ "./src/app/header/header.js":
/*!**********************************!*\
  !*** ./src/app/header/header.js ***!
  \**********************************/
/*! exports provided: Header */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Header\", function() { return Header; });\n/* harmony import */ var _header_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header.css */ \"./src/app/header/header.css\");\n/* harmony import */ var _header_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_header_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _container__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./container */ \"./src/app/header/container/index.js\");\n\r\n\r\n\r\n\r\n\r\n\r\nfunction Header() {\r\n    const header = document.createElement('header');\r\n\r\n    header.classList.add('header', 'bg-info', 'pb-3', 'pt-4', 'h4', 'mb-3');\r\n\r\n    header.append(Object(_container__WEBPACK_IMPORTED_MODULE_1__[\"Container\"])());\r\n\r\n    return header;\r\n}\r\n\r\n\n\n//# sourceURL=webpack:///./src/app/header/header.js?");

/***/ }),

/***/ "./src/app/header/index.js":
/*!*********************************!*\
  !*** ./src/app/header/index.js ***!
  \*********************************/
/*! exports provided: Header */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _header_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header.js */ \"./src/app/header/header.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Header\", function() { return _header_js__WEBPACK_IMPORTED_MODULE_0__[\"Header\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/header/index.js?");

/***/ }),

/***/ "./src/app/index.js":
/*!**************************!*\
  !*** ./src/app/index.js ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app */ \"./src/app/app.js\");\n\r\n\r\n\r\ndocument.getElementById('root').append(Object(_app__WEBPACK_IMPORTED_MODULE_0__[\"App\"])());\r\n\r\n\r\n\r\n\r\n                \r\n\r\n\r\n\r\n\r\n\r\n\n\n//# sourceURL=webpack:///./src/app/index.js?");

/***/ }),

/***/ "./src/app/main/add-category-modal/add-category-modal.css":
/*!****************************************************************!*\
  !*** ./src/app/main/add-category-modal/add-category-modal.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js!./add-category-modal.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/add-category-modal/add-category-modal.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/add-category-modal.css?");

/***/ }),

/***/ "./src/app/main/add-category-modal/add-category-modal.js":
/*!***************************************************************!*\
  !*** ./src/app/main/add-category-modal/add-category-modal.js ***!
  \***************************************************************/
/*! exports provided: AddCategoryModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"AddCategoryModal\", function() { return AddCategoryModal; });\n/* harmony import */ var _add_category_modal_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./add-category-modal.css */ \"./src/app/main/add-category-modal/add-category-modal.css\");\n/* harmony import */ var _add_category_modal_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_add_category_modal_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _category_form__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./category-form */ \"./src/app/main/add-category-modal/category-form/index.js\");\n\r\n\r\n\r\nfunction AddCategoryModal() {\r\n    const addCategoryModal = document.createElement('div');\r\n\r\n    addCategoryModal.classList.add('add-category-modal');\r\n    \r\n    addCategoryModal.append(Object(_category_form__WEBPACK_IMPORTED_MODULE_1__[\"CategoryForm\"])());\r\n\r\n    return addCategoryModal;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/add-category-modal.js?");

/***/ }),

/***/ "./src/app/main/add-category-modal/category-form/category-form.css":
/*!*************************************************************************!*\
  !*** ./src/app/main/add-category-modal/category-form/category-form.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./category-form.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/add-category-modal/category-form/category-form.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/category-form.css?");

/***/ }),

/***/ "./src/app/main/add-category-modal/category-form/category-form.js":
/*!************************************************************************!*\
  !*** ./src/app/main/add-category-modal/category-form/category-form.js ***!
  \************************************************************************/
/*! exports provided: CategoryForm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"CategoryForm\", function() { return CategoryForm; });\n/* harmony import */ var _category_form_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./category-form.css */ \"./src/app/main/add-category-modal/category-form/category-form.css\");\n/* harmony import */ var _category_form_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_category_form_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _modal_header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modal-header */ \"./src/app/main/add-category-modal/category-form/modal-header/index.js\");\n/* harmony import */ var _modalbody__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modalbody */ \"./src/app/main/add-category-modal/category-form/modalbody/index.js\");\n/* harmony import */ var _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../server-requests/server-requests */ \"./src/app/server-requests/server-requests.js\");\n\r\n\r\n\r\n\r\n\r\n\r\nfunction CategoryForm() {\r\n    const categoryForm = document.createElement('form');\r\n\r\n    categoryForm.classList.add('category-form', 'modal-content', 'col-4');\r\n\r\n    categoryForm.append(Object(_modal_header__WEBPACK_IMPORTED_MODULE_1__[\"ModalHeader\"])(), Object(_modalbody__WEBPACK_IMPORTED_MODULE_2__[\"ModalBody\"])());\r\n\r\n    categoryForm.addEventListener('submit', addNewCategories);\r\n\r\n    function addNewCategories(ev) {\r\n        ev.preventDefault();\r\n\r\n        const category = document.querySelector('.new-category').value;\r\n        const newCategory = {\r\n            category\r\n        }\r\n        \r\n        fetch('http://localhost:2000/categories/', {\r\n            method: 'POST',\r\n            headers: {\r\n                'Content-Type': 'application/json'\r\n            },\r\n            body: JSON.stringify(newCategory)\r\n        })\r\n        .then(res => {\r\n            return res.json();\r\n        })\r\n        .then(category => {\r\n            const categoryEl = document.querySelector('.new-category');\r\n            const newOption = document.createElement('option');\r\n            const addNewCategoryModal = document.querySelector('.add-category-modal');\r\n\r\n            newOption.textContent = `${category.category}`;\r\n            newOption.setAttribute('data-id', category.id);\r\n\r\n            categoryEl.appendChild(newOption);\r\n           \r\n            addNewCategoryModal.style.display = 'none';\r\n\r\n            return category.id;\r\n        }).then(id => {\r\n            new _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_3__[\"ServerRequests\"]().getCategory(id).then(category => {\r\n                addOptionsInCategories(category);\r\n\r\n                function addOptionsInCategories(category) {\r\n                    const categorySelects = document.querySelectorAll('.category');\r\n\r\n                    categorySelects.forEach(select => {\r\n                        const option = document.createElement('option');\r\n                        option.textContent = category.category;\r\n                        option.setAttribute('data-id', category.id);\r\n                        select.append(option);\r\n                    })\r\n                }\r\n            })\r\n        })\r\n    }\r\n\r\n    return categoryForm;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/category-form.js?");

/***/ }),

/***/ "./src/app/main/add-category-modal/category-form/index.js":
/*!****************************************************************!*\
  !*** ./src/app/main/add-category-modal/category-form/index.js ***!
  \****************************************************************/
/*! exports provided: CategoryForm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _category_form__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./category-form */ \"./src/app/main/add-category-modal/category-form/category-form.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"CategoryForm\", function() { return _category_form__WEBPACK_IMPORTED_MODULE_0__[\"CategoryForm\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/index.js?");

/***/ }),

/***/ "./src/app/main/add-category-modal/category-form/modal-header/index.js":
/*!*****************************************************************************!*\
  !*** ./src/app/main/add-category-modal/category-form/modal-header/index.js ***!
  \*****************************************************************************/
/*! exports provided: ModalHeader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modal_header__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-header */ \"./src/app/main/add-category-modal/category-form/modal-header/modal-header.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"ModalHeader\", function() { return _modal_header__WEBPACK_IMPORTED_MODULE_0__[\"ModalHeader\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/modal-header/index.js?");

/***/ }),

/***/ "./src/app/main/add-category-modal/category-form/modal-header/modal-header.css":
/*!*************************************************************************************!*\
  !*** ./src/app/main/add-category-modal/category-form/modal-header/modal-header.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./modal-header.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/add-category-modal/category-form/modal-header/modal-header.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/modal-header/modal-header.css?");

/***/ }),

/***/ "./src/app/main/add-category-modal/category-form/modal-header/modal-header.js":
/*!************************************************************************************!*\
  !*** ./src/app/main/add-category-modal/category-form/modal-header/modal-header.js ***!
  \************************************************************************************/
/*! exports provided: ModalHeader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ModalHeader\", function() { return ModalHeader; });\n/* harmony import */ var _modal_header_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-header.css */ \"./src/app/main/add-category-modal/category-form/modal-header/modal-header.css\");\n/* harmony import */ var _modal_header_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_header_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction ModalHeader() {\r\n    const modalHeader = document.createElement('div');\r\n    const title = document.createElement('h2');\r\n    const close = document.createElement('span');\r\n\r\n    modalHeader.classList.add('modal-header', 'bg-info');\r\n    close.classList.add('close');\r\n\r\n    title.innerHTML = 'add category';\r\n    close.innerHTML = '&times;';\r\n\r\n    modalHeader.append(title, close);\r\n\r\n    close.addEventListener('click', closeModal);\r\n\r\n    function closeModal(ev) {\r\n        const taskModal = document.querySelector('.add-category-modal');\r\n        taskModal.style.display = 'none'\r\n    }\r\n   \r\n    window.addEventListener('click', (ev) => {\r\n        const taskModal = document.querySelector('.add-category-modal');\r\n        if(ev.target == taskModal) {\r\n            taskModal.style.display = 'none';\r\n        }\r\n    })\r\n\r\n    return modalHeader;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/modal-header/modal-header.js?");

/***/ }),

/***/ "./src/app/main/add-category-modal/category-form/modalbody/add-category-btn/add-category-btn.css":
/*!*******************************************************************************************************!*\
  !*** ./src/app/main/add-category-modal/category-form/modalbody/add-category-btn/add-category-btn.css ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader/dist/cjs.js!./add-category-btn.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/add-category-modal/category-form/modalbody/add-category-btn/add-category-btn.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/modalbody/add-category-btn/add-category-btn.css?");

/***/ }),

/***/ "./src/app/main/add-category-modal/category-form/modalbody/add-category-btn/add-category-btn.js":
/*!******************************************************************************************************!*\
  !*** ./src/app/main/add-category-modal/category-form/modalbody/add-category-btn/add-category-btn.js ***!
  \******************************************************************************************************/
/*! exports provided: AddCategoryBtn */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"AddCategoryBtn\", function() { return AddCategoryBtn; });\n/* harmony import */ var _add_category_btn_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./add-category-btn.css */ \"./src/app/main/add-category-modal/category-form/modalbody/add-category-btn/add-category-btn.css\");\n/* harmony import */ var _add_category_btn_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_add_category_btn_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction AddCategoryBtn() {\r\n    const addCategoryBtn = document.createElement('button');\r\n    \r\n    addCategoryBtn.classList.add('btn', 'btn-primary', 'add-category-btn');\r\n\r\n    addCategoryBtn.setAttribute('type', 'submit');\r\n\r\n    addCategoryBtn.innerHTML = `\r\n        add\r\n        <i class=\"fa fa-floppy-o\" aria-hidden=\"true\"></i>\r\n    `;\r\n\r\n    return addCategoryBtn;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/modalbody/add-category-btn/add-category-btn.js?");

/***/ }),

/***/ "./src/app/main/add-category-modal/category-form/modalbody/add-category-btn/index.js":
/*!*******************************************************************************************!*\
  !*** ./src/app/main/add-category-modal/category-form/modalbody/add-category-btn/index.js ***!
  \*******************************************************************************************/
/*! exports provided: AddCategoryBtn */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _add_category_btn__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./add-category-btn */ \"./src/app/main/add-category-modal/category-form/modalbody/add-category-btn/add-category-btn.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"AddCategoryBtn\", function() { return _add_category_btn__WEBPACK_IMPORTED_MODULE_0__[\"AddCategoryBtn\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/modalbody/add-category-btn/index.js?");

/***/ }),

/***/ "./src/app/main/add-category-modal/category-form/modalbody/category-title-label/category-title-label.css":
/*!***************************************************************************************************************!*\
  !*** ./src/app/main/add-category-modal/category-form/modalbody/category-title-label/category-title-label.css ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader/dist/cjs.js!./category-title-label.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/add-category-modal/category-form/modalbody/category-title-label/category-title-label.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/modalbody/category-title-label/category-title-label.css?");

/***/ }),

/***/ "./src/app/main/add-category-modal/category-form/modalbody/category-title-label/category-title-label.js":
/*!**************************************************************************************************************!*\
  !*** ./src/app/main/add-category-modal/category-form/modalbody/category-title-label/category-title-label.js ***!
  \**************************************************************************************************************/
/*! exports provided: CategoryTitleLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"CategoryTitleLabel\", function() { return CategoryTitleLabel; });\n/* harmony import */ var _category_title_label_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./category-title-label.css */ \"./src/app/main/add-category-modal/category-form/modalbody/category-title-label/category-title-label.css\");\n/* harmony import */ var _category_title_label_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_category_title_label_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction CategoryTitleLabel() {\r\n    const categoryTitleLabel = document.createElement('label');\r\n    const categoryTitle = document.createElement('div');\r\n    const categoryInput = document.createElement('input');\r\n\r\n    categoryTitleLabel.classList.add('category-title-label','col');\r\n    categoryInput.classList.add('form-control', 'new-category');\r\n\r\n    categoryInput.setAttribute('type', 'text');\r\n    categoryInput.setAttribute('required', 'required');\r\n\r\n    categoryTitle.innerHTML = 'category title:';\r\n\r\n    categoryTitleLabel.append(categoryTitle, categoryInput);\r\n\r\n    return categoryTitleLabel;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/modalbody/category-title-label/category-title-label.js?");

/***/ }),

/***/ "./src/app/main/add-category-modal/category-form/modalbody/category-title-label/index.js":
/*!***********************************************************************************************!*\
  !*** ./src/app/main/add-category-modal/category-form/modalbody/category-title-label/index.js ***!
  \***********************************************************************************************/
/*! exports provided: CategoryTitleLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _category_title_label__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./category-title-label */ \"./src/app/main/add-category-modal/category-form/modalbody/category-title-label/category-title-label.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"CategoryTitleLabel\", function() { return _category_title_label__WEBPACK_IMPORTED_MODULE_0__[\"CategoryTitleLabel\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/modalbody/category-title-label/index.js?");

/***/ }),

/***/ "./src/app/main/add-category-modal/category-form/modalbody/index.js":
/*!**************************************************************************!*\
  !*** ./src/app/main/add-category-modal/category-form/modalbody/index.js ***!
  \**************************************************************************/
/*! exports provided: ModalBody */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modal_body__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-body */ \"./src/app/main/add-category-modal/category-form/modalbody/modal-body.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"ModalBody\", function() { return _modal_body__WEBPACK_IMPORTED_MODULE_0__[\"ModalBody\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/modalbody/index.js?");

/***/ }),

/***/ "./src/app/main/add-category-modal/category-form/modalbody/modal-body.css":
/*!********************************************************************************!*\
  !*** ./src/app/main/add-category-modal/category-form/modalbody/modal-body.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./modal-body.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/add-category-modal/category-form/modalbody/modal-body.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/modalbody/modal-body.css?");

/***/ }),

/***/ "./src/app/main/add-category-modal/category-form/modalbody/modal-body.js":
/*!*******************************************************************************!*\
  !*** ./src/app/main/add-category-modal/category-form/modalbody/modal-body.js ***!
  \*******************************************************************************/
/*! exports provided: ModalBody */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ModalBody\", function() { return ModalBody; });\n/* harmony import */ var _modal_body_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-body.css */ \"./src/app/main/add-category-modal/category-form/modalbody/modal-body.css\");\n/* harmony import */ var _modal_body_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_body_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _category_title_label__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./category-title-label */ \"./src/app/main/add-category-modal/category-form/modalbody/category-title-label/index.js\");\n/* harmony import */ var _add_category_btn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./add-category-btn */ \"./src/app/main/add-category-modal/category-form/modalbody/add-category-btn/index.js\");\n\r\n\r\n\r\n\r\nfunction ModalBody() {\r\n    const modalBody = document.createElement('div');\r\n\r\n    modalBody.classList.add('form-group', 'modal-body');\r\n\r\n    modalBody.append(Object(_category_title_label__WEBPACK_IMPORTED_MODULE_1__[\"CategoryTitleLabel\"])(), Object(_add_category_btn__WEBPACK_IMPORTED_MODULE_2__[\"AddCategoryBtn\"])());\r\n\r\n    return modalBody;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/category-form/modalbody/modal-body.js?");

/***/ }),

/***/ "./src/app/main/add-category-modal/index.js":
/*!**************************************************!*\
  !*** ./src/app/main/add-category-modal/index.js ***!
  \**************************************************/
/*! exports provided: AddCategoryModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _add_category_modal_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./add-category-modal.js */ \"./src/app/main/add-category-modal/add-category-modal.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"AddCategoryModal\", function() { return _add_category_modal_js__WEBPACK_IMPORTED_MODULE_0__[\"AddCategoryModal\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/add-category-modal/index.js?");

/***/ }),

/***/ "./src/app/main/add-task-btn/add-task-btn.css":
/*!****************************************************!*\
  !*** ./src/app/main/add-task-btn/add-task-btn.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js!./add-task-btn.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/add-task-btn/add-task-btn.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/add-task-btn/add-task-btn.css?");

/***/ }),

/***/ "./src/app/main/add-task-btn/add-task-btn.js":
/*!***************************************************!*\
  !*** ./src/app/main/add-task-btn/add-task-btn.js ***!
  \***************************************************/
/*! exports provided: AddTaskBtn */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"AddTaskBtn\", function() { return AddTaskBtn; });\n/* harmony import */ var _add_task_btn_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./add-task-btn.css */ \"./src/app/main/add-task-btn/add-task-btn.css\");\n/* harmony import */ var _add_task_btn_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_add_task_btn_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\n\r\nfunction AddTaskBtn() {\r\n    const addTaskBtn = document.createElement('button');\r\n    \r\n    const content = `\r\n    <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>\r\n    Add new task`;\r\n    \r\n\r\n    addTaskBtn.classList.add('add-task-btn', 'btn', 'btn-lg', 'btn-block', 'btn-info', 'mb-5', 'mt-4');\r\n    addTaskBtn.innerHTML = content;\r\n\r\n    addTaskBtn.addEventListener('click', openAddNewTaskModal);\r\n\r\n    function openAddNewTaskModal() {\r\n        const addModal = document.querySelector('.modal-add-task');\r\n        addModal.style.display = 'block';\r\n    }\r\n\r\n    return addTaskBtn;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/add-task-btn/add-task-btn.js?");

/***/ }),

/***/ "./src/app/main/add-task-btn/index.js":
/*!********************************************!*\
  !*** ./src/app/main/add-task-btn/index.js ***!
  \********************************************/
/*! exports provided: AddTaskBtn */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _add_task_btn_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./add-task-btn.js */ \"./src/app/main/add-task-btn/add-task-btn.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"AddTaskBtn\", function() { return _add_task_btn_js__WEBPACK_IMPORTED_MODULE_0__[\"AddTaskBtn\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/add-task-btn/index.js?");

/***/ }),

/***/ "./src/app/main/index.js":
/*!*******************************!*\
  !*** ./src/app/main/index.js ***!
  \*******************************/
/*! exports provided: Main */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _main_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main.js */ \"./src/app/main/main.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Main\", function() { return _main_js__WEBPACK_IMPORTED_MODULE_0__[\"Main\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/index.js?");

/***/ }),

/***/ "./src/app/main/main.css":
/*!*******************************!*\
  !*** ./src/app/main/main.css ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js!./main.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/main.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/main.css?");

/***/ }),

/***/ "./src/app/main/main.js":
/*!******************************!*\
  !*** ./src/app/main/main.js ***!
  \******************************/
/*! exports provided: Main */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Main\", function() { return Main; });\n/* harmony import */ var _main_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main.css */ \"./src/app/main/main.css\");\n/* harmony import */ var _main_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_main_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _add_task_btn__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./add-task-btn */ \"./src/app/main/add-task-btn/index.js\");\n/* harmony import */ var _tasks_filtration__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tasks-filtration */ \"./src/app/main/tasks-filtration/index.js\");\n/* harmony import */ var _tasks_filtration_all_tasks__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tasks-filtration/all-tasks */ \"./src/app/main/tasks-filtration/all-tasks/index.js\");\n/* harmony import */ var _modal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modal */ \"./src/app/main/modal/index.js\");\n/* harmony import */ var _add_category_modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-category-modal */ \"./src/app/main/add-category-modal/index.js\");\n/* harmony import */ var _modal_add_task__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modal-add-task */ \"./src/app/main/modal-add-task/index.js\");\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nfunction Main() {\r\n    const main = document.createElement('main');\r\n    const title = document.createElement('h2');\r\n\r\n    title.classList.add('text-center', 'bg-secondary', 'text-white', 'mb-0');\r\n    title.innerHTML = 'Tasks filter';\r\n\r\n    main.classList.add('main', 'container');\r\n\r\n    main.append(Object(_add_task_btn__WEBPACK_IMPORTED_MODULE_1__[\"AddTaskBtn\"])(), title, Object(_tasks_filtration__WEBPACK_IMPORTED_MODULE_2__[\"TasksFiltration\"])(), Object(_tasks_filtration_all_tasks__WEBPACK_IMPORTED_MODULE_3__[\"AllTasks\"])(), Object(_modal__WEBPACK_IMPORTED_MODULE_4__[\"Modal\"])(), Object(_modal_add_task__WEBPACK_IMPORTED_MODULE_6__[\"ModalAddTask\"])(), Object(_add_category_modal__WEBPACK_IMPORTED_MODULE_5__[\"AddCategoryModal\"])());\r\n\r\n    return main;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/main.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/form.css":
/*!***************************************************!*\
  !*** ./src/app/main/modal-add-task/form/form.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./form.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/form.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/form.css?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/form.js":
/*!**************************************************!*\
  !*** ./src/app/main/modal-add-task/form/form.js ***!
  \**************************************************/
/*! exports provided: FormAddTask */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"FormAddTask\", function() { return FormAddTask; });\n/* harmony import */ var _form_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./form.css */ \"./src/app/main/modal-add-task/form/form.css\");\n/* harmony import */ var _form_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_form_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _modal_header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modal-header */ \"./src/app/main/modal-add-task/form/modal-header/index.js\");\n/* harmony import */ var _modal_body__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modal-body */ \"./src/app/main/modal-add-task/form/modal-body/index.js\");\n/* harmony import */ var _modal_footer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modal-footer */ \"./src/app/main/modal-add-task/form/modal-footer/index.js\");\n\r\n\r\n\r\n\r\n\r\nfunction FormAddTask() {\r\n    const formAddTask = document.createElement('form');\r\n\r\n    formAddTask.classList.add('form-add-task', 'modal-content');\r\n\r\n    formAddTask.setAttribute('id', 'form-task');\r\n\r\n    formAddTask.append(Object(_modal_header__WEBPACK_IMPORTED_MODULE_1__[\"AddTaskModalHeader\"])(), Object(_modal_body__WEBPACK_IMPORTED_MODULE_2__[\"AddTaskModalBody\"])(), Object(_modal_footer__WEBPACK_IMPORTED_MODULE_3__[\"AddTaskModalFooter\"])());\r\n    \r\n    return formAddTask\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/form.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/index.js":
/*!***************************************************!*\
  !*** ./src/app/main/modal-add-task/form/index.js ***!
  \***************************************************/
/*! exports provided: FormAddTask */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _form_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./form.js */ \"./src/app/main/modal-add-task/form/form.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"FormAddTask\", function() { return _form_js__WEBPACK_IMPORTED_MODULE_0__[\"FormAddTask\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/index.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/category/category.css":
/*!***************************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/category/category.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader/dist/cjs.js!./category.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/category/category.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/category/category.css?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/category/category.js":
/*!**************************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/category/category.js ***!
  \**************************************************************************/
/*! exports provided: Category */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Category\", function() { return Category; });\n/* harmony import */ var _category_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./category.css */ \"./src/app/main/modal-add-task/form/modal-body/category/category.css\");\n/* harmony import */ var _category_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_category_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../server-requests/server-requests */ \"./src/app/server-requests/server-requests.js\");\n\r\n\r\n\r\n\r\n\r\nfunction Category() {\r\n    const addCategory = document.createElement('label');\r\n    const title = document.createElement('div');\r\n    const inputGroup = document.createElement('div');\r\n    const inputGroupPrepend = document.createElement('div');\r\n    const categorySelect = document.createElement('select');\r\n\r\n    addCategory.classList.add('add-category');\r\n    inputGroup.classList.add('input-group', 'mb-3');\r\n    inputGroupPrepend.classList.add('input-group-prepend', 'add-new');\r\n    categorySelect.classList.add('form-control', 'category');\r\n\r\n    categorySelect.setAttribute('name', 'category');\r\n    categorySelect.setAttribute('id', 'category');\r\n   \r\n    title.innerHTML = 'category:';\r\n    inputGroupPrepend.innerHTML = `\r\n        <span class=\"input-group-text\" id=\"basic-addon1\">\r\n            <i class=\"fa fa-plus\" aria-hidden=\"true\"></i>\r\n        </span>\r\n    `;\r\n\r\n    inputGroup.append(inputGroupPrepend, categorySelect);\r\n    addCategory.append(title, inputGroup);\r\n\r\n    function addCategories(categories) {\r\n        categories.forEach(category => {    \r\n            addOptionsInCategories(category);\r\n        })\r\n    }\r\n\r\n    function addOptionsInCategories(category) {\r\n        const option = document.createElement('option');\r\n        option.textContent = category.category;\r\n        option.setAttribute('data-id', category.id);\r\n        categorySelect.append(option);\r\n    }\r\n\r\n    const getCategories = new _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_1__[\"ServerRequests\"]().getCategories();\r\n    \r\n    getCategories.then(categories => {\r\n        addCategories(categories);\r\n        return categories;\r\n    })\r\n\r\n\r\n    inputGroupPrepend.addEventListener('click', openCategoryAddModal);\r\n\r\n    function openCategoryAddModal() {\r\n        const addCategoryModal = document.querySelector('.add-category-modal');\r\n        addCategoryModal.style.display = \"block\";\r\n    }\r\n\r\n    return addCategory;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/category/category.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/category/categoryRender.js":
/*!********************************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/category/categoryRender.js ***!
  \********************************************************************************/
/*! exports provided: categoryRender */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"categoryRender\", function() { return categoryRender; });\n\r\nfunction categoryRender(getCategories, categorySelect) {\r\n    function addCategories(categories) {\r\n        categories.forEach(category => {    \r\n            addOptionsInCategories(category);\r\n        })\r\n    }\r\n\r\n    function addOptionsInCategories(category) {\r\n        const option = document.createElement('option');\r\n        option.textContent = category.category;\r\n        option.setAttribute('data-id', category.id);\r\n        categorySelect.append(option);\r\n    }\r\n\r\n    getCategories.then(categories => {\r\n        addCategories(categories);\r\n        return categories;\r\n    })\r\n}\r\n\r\n\r\n    \r\n\r\n    \r\n    \r\n    \n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/category/categoryRender.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/category/index.js":
/*!***********************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/category/index.js ***!
  \***********************************************************************/
/*! exports provided: Category */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _category__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./category */ \"./src/app/main/modal-add-task/form/modal-body/category/category.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Category\", function() { return _category__WEBPACK_IMPORTED_MODULE_0__[\"Category\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/category/index.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/comment/comment.css":
/*!*************************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/comment/comment.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader/dist/cjs.js!./comment.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/comment/comment.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/comment/comment.css?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/comment/comment.js":
/*!************************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/comment/comment.js ***!
  \************************************************************************/
/*! exports provided: Comment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Comment\", function() { return Comment; });\n/* harmony import */ var _comment_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./comment.css */ \"./src/app/main/modal-add-task/form/modal-body/comment/comment.css\");\n/* harmony import */ var _comment_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_comment_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction Comment() {\r\n    const commentLabel = document.createElement('label');\r\n    const commentText = document.createElement('textarea');\r\n\r\n    commentLabel.classList.add('comment-label', 'w-100');\r\n    commentText.classList.add('form-control', 'comment');\r\n\r\n    commentText.setAttribute('rows', '3');\r\n    commentText.setAttribute('placeholder', 'add comment');\r\n\r\n    commentLabel.innerHTML = 'comment:';\r\n\r\n    commentLabel.append(commentText);\r\n\r\n    return commentLabel;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/comment/comment.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/comment/index.js":
/*!**********************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/comment/index.js ***!
  \**********************************************************************/
/*! exports provided: Comment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _comment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./comment */ \"./src/app/main/modal-add-task/form/modal-body/comment/comment.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Comment\", function() { return _comment__WEBPACK_IMPORTED_MODULE_0__[\"Comment\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/comment/index.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/deadline-date/deadline-date.css":
/*!*************************************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/deadline-date/deadline-date.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader/dist/cjs.js!./deadline-date.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/deadline-date/deadline-date.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/deadline-date/deadline-date.css?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/deadline-date/deadline-date.js":
/*!************************************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/deadline-date/deadline-date.js ***!
  \************************************************************************************/
/*! exports provided: DeadlineDate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"DeadlineDate\", function() { return DeadlineDate; });\n/* harmony import */ var _deadline_date_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./deadline-date.css */ \"./src/app/main/modal-add-task/form/modal-body/deadline-date/deadline-date.css\");\n/* harmony import */ var _deadline_date_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_deadline_date_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction DeadlineDate() {\r\n    const deadlineDate = document.createElement('label');\r\n    const title = document.createElement('div');\r\n    const daedlineDateInput = document.createElement('input');\r\n\r\n    title.innerHTML = 'deadline date:';\r\n\r\n    deadlineDate.classList.add('deadline-date');\r\n    daedlineDateInput.classList.add('form-control', 'input-date');\r\n\r\n    daedlineDateInput.setAttribute('id', 'input-date');\r\n    daedlineDateInput.setAttribute('type', 'text');\r\n    daedlineDateInput.setAttribute('autocomplete', 'off');\r\n    daedlineDateInput.setAttribute('required', 'required');\r\n\r\n    deadlineDate.append(title, daedlineDateInput);\r\n\r\n    $(function() {\r\n        $(daedlineDateInput).datepicker({\r\n        dateFormat: 'yy-m-d',\r\n        dayNamesMin : [ \"S\", \"M\", \"T\", \"W\", \"T\", \"F\", \"S\" ],\r\n        })\r\n    })\r\n\r\n    return deadlineDate;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/deadline-date/deadline-date.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/deadline-date/index.js":
/*!****************************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/deadline-date/index.js ***!
  \****************************************************************************/
/*! exports provided: DeadlineDate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _deadline_date__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./deadline-date */ \"./src/app/main/modal-add-task/form/modal-body/deadline-date/deadline-date.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"DeadlineDate\", function() { return _deadline_date__WEBPACK_IMPORTED_MODULE_0__[\"DeadlineDate\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/deadline-date/index.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/index.js":
/*!**************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/index.js ***!
  \**************************************************************/
/*! exports provided: AddTaskModalBody */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modal_body__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-body */ \"./src/app/main/modal-add-task/form/modal-body/modal-body.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"AddTaskModalBody\", function() { return _modal_body__WEBPACK_IMPORTED_MODULE_0__[\"AddTaskModalBody\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/index.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/modal-body.css":
/*!********************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/modal-body.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./modal-body.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/modal-body.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/modal-body.css?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/modal-body.js":
/*!*******************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/modal-body.js ***!
  \*******************************************************************/
/*! exports provided: AddTaskModalBody */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"AddTaskModalBody\", function() { return AddTaskModalBody; });\n/* harmony import */ var _modal_body_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-body.css */ \"./src/app/main/modal-add-task/form/modal-body/modal-body.css\");\n/* harmony import */ var _modal_body_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_body_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _task_title__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./task-title */ \"./src/app/main/modal-add-task/form/modal-body/task-title/index.js\");\n/* harmony import */ var _deadline_date__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./deadline-date */ \"./src/app/main/modal-add-task/form/modal-body/deadline-date/index.js\");\n/* harmony import */ var _time__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./time */ \"./src/app/main/modal-add-task/form/modal-body/time/index.js\");\n/* harmony import */ var _category__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./category */ \"./src/app/main/modal-add-task/form/modal-body/category/index.js\");\n/* harmony import */ var _priority__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./priority */ \"./src/app/main/modal-add-task/form/modal-body/priority/index.js\");\n/* harmony import */ var _comment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./comment */ \"./src/app/main/modal-add-task/form/modal-body/comment/index.js\");\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nfunction AddTaskModalBody() {\r\n    const addTaskModalBody = document.createElement('div');\r\n\r\n    addTaskModalBody.classList.add('form-group', 'mb-0', 'labels', 'd-flex', 'justify-content-between', 'flex-wrap', 'modal-body');\r\n\r\n    addTaskModalBody.append(Object(_task_title__WEBPACK_IMPORTED_MODULE_1__[\"TaskTitle\"])(), Object(_deadline_date__WEBPACK_IMPORTED_MODULE_2__[\"DeadlineDate\"])(), Object(_time__WEBPACK_IMPORTED_MODULE_3__[\"Time\"])(), Object(_category__WEBPACK_IMPORTED_MODULE_4__[\"Category\"])(), Object(_priority__WEBPACK_IMPORTED_MODULE_5__[\"Priority\"])(), Object(_comment__WEBPACK_IMPORTED_MODULE_6__[\"Comment\"])());\r\n\r\n    return addTaskModalBody;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/modal-body.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/priority/index.js":
/*!***********************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/priority/index.js ***!
  \***********************************************************************/
/*! exports provided: Priority */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _priority__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./priority */ \"./src/app/main/modal-add-task/form/modal-body/priority/priority.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Priority\", function() { return _priority__WEBPACK_IMPORTED_MODULE_0__[\"Priority\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/priority/index.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/priority/priority.css":
/*!***************************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/priority/priority.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader/dist/cjs.js!./priority.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/priority/priority.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/priority/priority.css?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/priority/priority.js":
/*!**************************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/priority/priority.js ***!
  \**************************************************************************/
/*! exports provided: Priority */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Priority\", function() { return Priority; });\n/* harmony import */ var _priority_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./priority.css */ \"./src/app/main/modal-add-task/form/modal-body/priority/priority.css\");\n/* harmony import */ var _priority_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_priority_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction Priority() {\r\n    const priority = document.createElement('label');\r\n    const priorityTitle = document.createElement('div');\r\n    const priorityInput = document.createElement('input');\r\n\r\n    priority.classList.add('priority');\r\n    priorityTitle.classList.add('priority-title');\r\n    priorityInput.classList.add('custom-range', 'priority-input');\r\n\r\n    priorityInput.setAttribute('min', '0');\r\n    priorityInput.setAttribute('max', '2');\r\n    priorityInput.setAttribute('value', '0');\r\n    priorityInput.setAttribute('id', 'priority');\r\n    priorityInput.setAttribute('type', 'range');\r\n\r\n    priorityTitle.innerHTML = 'priority: low';\r\n\r\n    priority.append(priorityTitle, priorityInput);\r\n\r\n    function renderPriority() {\r\n        priorityInput.addEventListener('input', () => {\r\n            let priority;\r\n            if(priorityInput.value == 0) {\r\n                priority = 'low'\r\n            } else if(priorityInput.value == 1) {\r\n                priority = 'middle'\r\n            } else if(priorityInput.value == 2) {\r\n                priority = 'important'\r\n            }\r\n            priorityTitle.textContent = `priority: ${priority}`;\r\n        })\r\n    };\r\n\r\n    renderPriority();\r\n\r\n    return priority;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/priority/priority.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/task-title/index.js":
/*!*************************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/task-title/index.js ***!
  \*************************************************************************/
/*! exports provided: TaskTitle */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _task_tile__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./task-tile */ \"./src/app/main/modal-add-task/form/modal-body/task-title/task-tile.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"TaskTitle\", function() { return _task_tile__WEBPACK_IMPORTED_MODULE_0__[\"TaskTitle\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/task-title/index.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/task-title/task-tile.js":
/*!*****************************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/task-title/task-tile.js ***!
  \*****************************************************************************/
/*! exports provided: TaskTitle */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"TaskTitle\", function() { return TaskTitle; });\n/* harmony import */ var _task_title_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./task-title.css */ \"./src/app/main/modal-add-task/form/modal-body/task-title/task-title.css\");\n/* harmony import */ var _task_title_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_task_title_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction TaskTitle() {\r\n    const taskTitle = document.createElement('label');\r\n    const title = document.createElement('div');\r\n    const titleInput = document.createElement('input');\r\n\r\n    title.innerHTML = 'task title:';\r\n\r\n    taskTitle.classList.add('task-title');\r\n    titleInput.classList.add('form-control', 'input', 'input-add');\r\n\r\n    titleInput.setAttribute('type', 'text');\r\n    titleInput.setAttribute('placeholder', 'create task');\r\n    titleInput.setAttribute('id', 'input-add');\r\n    titleInput.setAttribute('required', 'required');\r\n\r\n    taskTitle.append(title, titleInput);\r\n    \r\n\r\n    return taskTitle;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/task-title/task-tile.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/task-title/task-title.css":
/*!*******************************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/task-title/task-title.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader/dist/cjs.js!./task-title.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/task-title/task-title.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/task-title/task-title.css?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/time/index.js":
/*!*******************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/time/index.js ***!
  \*******************************************************************/
/*! exports provided: Time */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _time__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./time */ \"./src/app/main/modal-add-task/form/modal-body/time/time.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Time\", function() { return _time__WEBPACK_IMPORTED_MODULE_0__[\"Time\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/time/index.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/time/time.css":
/*!*******************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/time/time.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader/dist/cjs.js!./time.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-body/time/time.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/time/time.css?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-body/time/time.js":
/*!******************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-body/time/time.js ***!
  \******************************************************************/
/*! exports provided: Time */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Time\", function() { return Time; });\n/* harmony import */ var _time_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./time.css */ \"./src/app/main/modal-add-task/form/modal-body/time/time.css\");\n/* harmony import */ var _time_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_time_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction Time() {\r\n    const time = document.createElement('label');\r\n    const title = document.createElement('div');\r\n    const timeInput = document.createElement('input');\r\n\r\n    title.innerHTML = 'time:';\r\n\r\n    time.classList.add('time');\r\n    timeInput.classList.add('form-control', 'input-time');\r\n\r\n    timeInput.setAttribute('type', 'time');\r\n    timeInput.setAttribute('id', 'input-time');\r\n    timeInput.setAttribute('autocomplete', 'off');\r\n\r\n    time.append(title, timeInput);\r\n\r\n    return time;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-body/time/time.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-footer/index.js":
/*!****************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-footer/index.js ***!
  \****************************************************************/
/*! exports provided: AddTaskModalFooter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modal_footer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-footer */ \"./src/app/main/modal-add-task/form/modal-footer/modal-footer.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"AddTaskModalFooter\", function() { return _modal_footer__WEBPACK_IMPORTED_MODULE_0__[\"AddTaskModalFooter\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-footer/index.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-footer/modal-footer.css":
/*!************************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-footer/modal-footer.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./modal-footer.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-footer/modal-footer.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-footer/modal-footer.css?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-footer/modal-footer.js":
/*!***********************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-footer/modal-footer.js ***!
  \***********************************************************************/
/*! exports provided: AddTaskModalFooter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"AddTaskModalFooter\", function() { return AddTaskModalFooter; });\n/* harmony import */ var _modal_footer_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-footer.css */ \"./src/app/main/modal-add-task/form/modal-footer/modal-footer.css\");\n/* harmony import */ var _modal_footer_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_footer_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction AddTaskModalFooter() {\r\n    const addTaskModalFooter = document.createElement('div');\r\n    const saveButton = document.createElement('button');\r\n\r\n    addTaskModalFooter.classList.add('modal-footer', 'bg-info');\r\n    saveButton.classList.add('save-button', 'btn', 'btn-primary');\r\n\r\n    saveButton.setAttribute('type', 'submit');\r\n    saveButton.setAttribute('id', 'save-button');\r\n    saveButton.innerHTML = `\r\n        save\r\n        <i class=\"fa fa-floppy-o\" aria-hidden=\"true\"></i>\r\n    `;\r\n\r\n    addTaskModalFooter.append(saveButton);\r\n\r\n    return addTaskModalFooter;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-footer/modal-footer.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-header/index.js":
/*!****************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-header/index.js ***!
  \****************************************************************/
/*! exports provided: AddTaskModalHeader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modal_header__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-header */ \"./src/app/main/modal-add-task/form/modal-header/modal-header.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"AddTaskModalHeader\", function() { return _modal_header__WEBPACK_IMPORTED_MODULE_0__[\"AddTaskModalHeader\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-header/index.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-header/modal-header.css":
/*!************************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-header/modal-header.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./modal-header.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/form/modal-header/modal-header.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-header/modal-header.css?");

/***/ }),

/***/ "./src/app/main/modal-add-task/form/modal-header/modal-header.js":
/*!***********************************************************************!*\
  !*** ./src/app/main/modal-add-task/form/modal-header/modal-header.js ***!
  \***********************************************************************/
/*! exports provided: AddTaskModalHeader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"AddTaskModalHeader\", function() { return AddTaskModalHeader; });\n/* harmony import */ var _modal_header_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-header.css */ \"./src/app/main/modal-add-task/form/modal-header/modal-header.css\");\n/* harmony import */ var _modal_header_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_header_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction AddTaskModalHeader() {\r\n    const addTaskModalheader = document.createElement('div');\r\n    const title = document.createElement('h2');\r\n    const close = document.createElement('span');\r\n\r\n    title.innerHTML = 'Create your task:';\r\n    close.innerHTML = '&times;';\r\n\r\n    addTaskModalheader.classList.add('modal-header', 'bg-info');\r\n    close.classList.add('close');\r\n\r\n    addTaskModalheader.append(title, close);\r\n \r\n    close.onclick = function() {\r\n        const modal = document.querySelector('.modal-add-task');\r\n        modal.style.display = 'none';\r\n    }\r\n\r\n\r\n    window.addEventListener('click', (ev) => {\r\n        const modal = document.querySelector('.modal-add-task');\r\n        if(ev.target === modal) {\r\n            modal.style.display = 'none';\r\n        }\r\n    })\r\n\r\n    return addTaskModalheader;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/form/modal-header/modal-header.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/index.js":
/*!**********************************************!*\
  !*** ./src/app/main/modal-add-task/index.js ***!
  \**********************************************/
/*! exports provided: ModalAddTask */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modal_add_task_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-add-task.js */ \"./src/app/main/modal-add-task/modal-add-task.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"ModalAddTask\", function() { return _modal_add_task_js__WEBPACK_IMPORTED_MODULE_0__[\"ModalAddTask\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/index.js?");

/***/ }),

/***/ "./src/app/main/modal-add-task/modal-add-task.css":
/*!********************************************************!*\
  !*** ./src/app/main/modal-add-task/modal-add-task.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js!./modal-add-task.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal-add-task/modal-add-task.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/modal-add-task.css?");

/***/ }),

/***/ "./src/app/main/modal-add-task/modal-add-task.js":
/*!*******************************************************!*\
  !*** ./src/app/main/modal-add-task/modal-add-task.js ***!
  \*******************************************************/
/*! exports provided: ModalAddTask */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ModalAddTask\", function() { return ModalAddTask; });\n/* harmony import */ var _modal_add_task_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-add-task.css */ \"./src/app/main/modal-add-task/modal-add-task.css\");\n/* harmony import */ var _modal_add_task_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_add_task_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _form__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./form */ \"./src/app/main/modal-add-task/form/index.js\");\n/* harmony import */ var _main_tasks_filtration_all_tasks_table_tbody_get_tr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../main/tasks-filtration/all-tasks/table/tbody/get-tr */ \"./src/app/main/tasks-filtration/all-tasks/table/tbody/get-tr.js\");\n/* harmony import */ var _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../server-requests/server-requests */ \"./src/app/server-requests/server-requests.js\");\n/* harmony import */ var _header_container_search_label_search_input_group_search_getAutocompleteTitles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../header/container/search-label/search-input-group/search/getAutocompleteTitles */ \"./src/app/header/container/search-label/search-input-group/search/getAutocompleteTitles.js\");\n/* harmony import */ var _modal_render_tasks__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../modal/render-tasks */ \"./src/app/main/modal/render-tasks.js\");\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nfunction ModalAddTask() {\r\n    const modalAddTask = document.createElement('div');\r\n\r\n    modalAddTask.classList.add('modal-add-task');\r\n    modalAddTask.append(Object(_form__WEBPACK_IMPORTED_MODULE_1__[\"FormAddTask\"])());\r\n    modalAddTask.addEventListener('submit', addNewTask);\r\n\r\n    function addNewTask(ev) {\r\n        ev.preventDefault();\r\n        const title = ev.target.querySelector('#input-add').value;\r\n        const deadlineDate = ev.target.querySelector('#input-date').value;\r\n        const category = ev.target.querySelector('#category').value;\r\n        const priorityValue = +ev.target.querySelector('#priority').value;\r\n        const time = ev.target.querySelector('#input-time').value;\r\n        const comment = ev.target.querySelector('.comment').value;\r\n        const user = document.querySelector('.user-name-input').getAttribute('placeholder');\r\n        // const userId = document.querySelector('.user-name-input').getAttribute('data-id');\r\n        const userId = localStorage.getItem('id');\r\n\r\n        let priority;\r\n        if(priorityValue === 0) {\r\n            priority = 'low'\r\n        } else if(priorityValue === 1) {\r\n            priority = 'middle'\r\n        } else if(priorityValue === 2) {\r\n            priority = 'important'\r\n        }\r\n        \r\n        const task = {\r\n            title,\r\n            deadlineDate,\r\n            category,\r\n            priority,\r\n            time,\r\n            comment,\r\n            user,\r\n            userId\r\n        }\r\n\r\n        fetch('http://localhost:2000/tasks/', {\r\n            method: 'POST',\r\n            headers: {\r\n                'Content-Type': 'application/json'\r\n            },\r\n            body: JSON.stringify(task)\r\n        })\r\n        .then(res => res.json())\r\n        .then(id => {\r\n            new _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_3__[\"ServerRequests\"]().getTask(id).then(task => {\r\n                const tr = Object(_main_tasks_filtration_all_tasks_table_tbody_get_tr__WEBPACK_IMPORTED_MODULE_2__[\"getTr\"])(task);\r\n                const tBody = document.querySelector('tbody');\r\n                const td = tr.lastChild;\r\n\r\n                if(td.textContent === 'low') {\r\n                    td.style.color = '#28a745';\r\n                } else if(td.textContent === 'middle') {\r\n                    td.style.color = '#ffc107';\r\n                } else if(td.textContent === 'important'){\r\n                    td.style.color = '#dc3545';\r\n                }\r\n\r\n                tr.addEventListener('click', getInfoTaskFromTable);\r\n                tBody.append(tr);\r\n        \r\n                function getInfoTaskFromTable(ev) {\r\n                    const id = ev.target.parentElement.getAttribute('data-id');\r\n                    const modal = document.querySelector('.modal');\r\n                    const titleOfModal = document.querySelector('.modal-header h2');\r\n                    const title = ev.target.parentElement.firstChild.textContent;\r\n        \r\n                    modal.style.display = \"block\";\r\n                    titleOfModal.textContent = `${title}`;\r\n                      \r\n                    new _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_3__[\"ServerRequests\"]().getTask(id)\r\n                    .then(task => {\r\n                        Object(_modal_render_tasks__WEBPACK_IMPORTED_MODULE_5__[\"renderTaskInfoToModalChange\"])(task);\r\n                    })\r\n                }\r\n            })\r\n\r\n            const getTasks = new _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_3__[\"ServerRequests\"]().getUserTasks(userId);\r\n\r\n            Object(_header_container_search_label_search_input_group_search_getAutocompleteTitles__WEBPACK_IMPORTED_MODULE_4__[\"getAutocompleteTitles\"])(getTasks);\r\n        })\r\n        .then(() => {\r\n            const title = document.querySelector('#input-add');\r\n            const deadlineDate = document.querySelector('#input-date');\r\n            const time = document.querySelector('#input-time');\r\n            const category = document.querySelector('#category');\r\n            const comment = document.querySelector('.comment');\r\n\r\n            title.value = null;\r\n            deadlineDate.value = null;\r\n            time.value = null;\r\n            category.value = null;\r\n            comment.value =null;\r\n        })\r\n        modalAddTask.style.display = 'none';\r\n    }\r\n    return modalAddTask;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal-add-task/modal-add-task.js?");

/***/ }),

/***/ "./src/app/main/modal/index.js":
/*!*************************************!*\
  !*** ./src/app/main/modal/index.js ***!
  \*************************************/
/*! exports provided: Modal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modal_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal.js */ \"./src/app/main/modal/modal.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Modal\", function() { return _modal_js__WEBPACK_IMPORTED_MODULE_0__[\"Modal\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal/index.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/index.js":
/*!***************************************************!*\
  !*** ./src/app/main/modal/modal-content/index.js ***!
  \***************************************************/
/*! exports provided: ModalContent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modal_content_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-content.js */ \"./src/app/main/modal/modal-content/modal-content.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"ModalContent\", function() { return _modal_content_js__WEBPACK_IMPORTED_MODULE_0__[\"ModalContent\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/index.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/change-form.css":
/*!*********************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/change-form.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader/dist/cjs.js!./change-form.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/change-form.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/change-form.css?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/change-form.js":
/*!********************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/change-form.js ***!
  \********************************************************************************/
/*! exports provided: ChangeForm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ChangeForm\", function() { return ChangeForm; });\n/* harmony import */ var _change_form_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./change-form.css */ \"./src/app/main/modal/modal-content/modal-body/change-form/change-form.css\");\n/* harmony import */ var _change_form_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_change_form_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _form_group__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./form-group */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/index.js\");\n\r\n\r\n\r\nfunction ChangeForm() {\r\n    const changeForm = document.createElement('form');\r\n\r\n    changeForm.classList.add('change-form');\r\n\r\n    changeForm.append(Object(_form_group__WEBPACK_IMPORTED_MODULE_1__[\"FormGroup\"])());\r\n\r\n    return changeForm;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/change-form.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/form-group.css":
/*!*******************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/form-group.css ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../../node_modules/css-loader/dist/cjs.js!./form-group.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/form-group.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/form-group.css?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/form-group.js":
/*!******************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/form-group.js ***!
  \******************************************************************************************/
/*! exports provided: FormGroup */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"FormGroup\", function() { return FormGroup; });\n/* harmony import */ var _form_group_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./form-group.css */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/form-group.css\");\n/* harmony import */ var _form_group_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_form_group_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _title__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./title */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/title/index.js\");\n/* harmony import */ var _labels__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./labels */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/index.js\");\n\r\n\r\n\r\n\r\nfunction FormGroup() {\r\n    const formGroup = document.createElement('div');\r\n\r\n    formGroup.classList.add('form-group');\r\n\r\n    formGroup.append(Object(_title__WEBPACK_IMPORTED_MODULE_1__[\"Title\"])(), Object(_labels__WEBPACK_IMPORTED_MODULE_2__[\"Labels\"])());\r\n\r\n    return formGroup;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/form-group.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/index.js":
/*!*************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/index.js ***!
  \*************************************************************************************/
/*! exports provided: FormGroup */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _form_group__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./form-group */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/form-group.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"FormGroup\", function() { return _form_group__WEBPACK_IMPORTED_MODULE_0__[\"FormGroup\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/index.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/category-label/category-label.css":
/*!*********************************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/category-label/category-label.css ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../../../../node_modules/css-loader/dist/cjs.js!./category-label.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/category-label/category-label.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/category-label/category-label.css?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/category-label/category-label.js":
/*!********************************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/category-label/category-label.js ***!
  \********************************************************************************************************************/
/*! exports provided: CategoryLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"CategoryLabel\", function() { return CategoryLabel; });\n/* harmony import */ var _category_label_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./category-label.css */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/category-label/category-label.css\");\n/* harmony import */ var _category_label_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_category_label_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! .././../../../../../../../server-requests/server-requests */ \"./src/app/server-requests/server-requests.js\");\n\r\n\r\n\r\nfunction CategoryLabel() {\r\n    const categoryLabel = document.createElement('label');\r\n    const title = document.createElement('div');\r\n    const inputGroup = document.createElement('div');\r\n    const inputGroupPrepend = document.createElement('div');\r\n    const categorySelect = document.createElement('select');\r\n\r\n    categoryLabel.classList.add('category-label');\r\n    inputGroup.classList.add('input-group', 'mb-3');\r\n    inputGroupPrepend.classList.add('input-group-prepend', 'add-new');\r\n    categorySelect.classList.add('form-control', 'category');\r\n\r\n    categorySelect.setAttribute('name', 'category');\r\n    categorySelect.setAttribute('id', 'info-category');\r\n\r\n    title.innerHTML = 'category:';\r\n    inputGroupPrepend.innerHTML = `\r\n        <span class=\"input-group-text\" id=\"basic-addon1\">\r\n            <i class=\"fa fa-plus\" aria-hidden=\"true\"></i>\r\n        </span>\r\n    `;\r\n\r\n    inputGroup.append(inputGroupPrepend, categorySelect);\r\n    categoryLabel.append(title, inputGroup);\r\n\r\n    function addCategories(categories) {\r\n        categories.forEach(category => {    \r\n            addOptionsInCategories(category);\r\n        })\r\n    }\r\n\r\n    function addOptionsInCategories(category) {\r\n        const option = document.createElement('option');\r\n        option.textContent = category.category;\r\n        option.setAttribute('data-id', category.id);\r\n        categorySelect.append(option);\r\n    }\r\n\r\n    const getCategories = new _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_1__[\"ServerRequests\"]().getCategories();\r\n    \r\n    getCategories.then(categories => {\r\n        addCategories(categories);\r\n        return categories;\r\n    })\r\n\r\n    inputGroupPrepend.addEventListener('click', openCategoryAddModal);\r\n\r\n    function openCategoryAddModal() {\r\n        const addCategoryModal = document.querySelector('.add-category-modal');\r\n        addCategoryModal.style.display = \"block\";\r\n    }\r\n    \r\n    return categoryLabel;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/category-label/category-label.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/category-label/index.js":
/*!***********************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/category-label/index.js ***!
  \***********************************************************************************************************/
/*! exports provided: CategoryLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _category_label__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./category-label */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/category-label/category-label.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"CategoryLabel\", function() { return _category_label__WEBPACK_IMPORTED_MODULE_0__[\"CategoryLabel\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/category-label/index.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/comment-label/comment-label.css":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/comment-label/comment-label.css ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../../../../node_modules/css-loader/dist/cjs.js!./comment-label.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/comment-label/comment-label.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/comment-label/comment-label.css?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/comment-label/comment-label.js":
/*!******************************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/comment-label/comment-label.js ***!
  \******************************************************************************************************************/
/*! exports provided: CommentLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"CommentLabel\", function() { return CommentLabel; });\n/* harmony import */ var _comment_label_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./comment-label.css */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/comment-label/comment-label.css\");\n/* harmony import */ var _comment_label_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_comment_label_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction CommentLabel() {\r\n    const commentLabel = document.createElement('label');\r\n    const commentText = document.createElement('textarea');\r\n\r\n    commentLabel.classList.add('comment-label', 'w-100');\r\n    commentText.classList.add('form-control', 'comment-info');\r\n\r\n    commentText.setAttribute('rows', '3');\r\n\r\n    commentLabel.innerHTML = 'comment:';\r\n\r\n    commentLabel.append(commentText);\r\n\r\n    return commentLabel;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/comment-label/comment-label.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/comment-label/index.js":
/*!**********************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/comment-label/index.js ***!
  \**********************************************************************************************************/
/*! exports provided: CommentLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _comment_label__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./comment-label */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/comment-label/comment-label.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"CommentLabel\", function() { return _comment_label__WEBPACK_IMPORTED_MODULE_0__[\"CommentLabel\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/comment-label/index.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/deadline-label/deadline-label.css":
/*!*********************************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/deadline-label/deadline-label.css ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../../../../node_modules/css-loader/dist/cjs.js!./deadline-label.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/deadline-label/deadline-label.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/deadline-label/deadline-label.css?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/deadline-label/deadline-label.js":
/*!********************************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/deadline-label/deadline-label.js ***!
  \********************************************************************************************************************/
/*! exports provided: DeadlineLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"DeadlineLabel\", function() { return DeadlineLabel; });\n/* harmony import */ var _deadline_label_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./deadline-label.css */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/deadline-label/deadline-label.css\");\n/* harmony import */ var _deadline_label_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_deadline_label_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction DeadlineLabel() {\r\n    const deadlineLabel = document.createElement('label');\r\n    const title = document.createElement('div');\r\n    const deadlineInput = document.createElement('input');\r\n\r\n    deadlineLabel.classList.add('deadline-label');\r\n    deadlineInput.classList.add('form-control', 'input-date');\r\n\r\n    title.innerHTML = 'deadline date:';\r\n\r\n    deadlineInput.setAttribute('type', 'text');\r\n    deadlineInput.setAttribute('id', 'info-date');\r\n    deadlineInput.setAttribute('autocomplete', 'off');\r\n    deadlineInput.setAttribute('required', 'required');\r\n\r\n    deadlineLabel.append(title, deadlineInput);\r\n\r\n    $(function() {\r\n        $(deadlineInput).datepicker({\r\n        dateFormat: 'yy-m-d',\r\n        dayNamesMin : [ \"S\", \"M\", \"T\", \"W\", \"T\", \"F\", \"S\" ],\r\n        })\r\n\r\n    })\r\n\r\n    return deadlineLabel;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/deadline-label/deadline-label.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/deadline-label/index.js":
/*!***********************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/deadline-label/index.js ***!
  \***********************************************************************************************************/
/*! exports provided: DeadlineLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _deadline_label__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./deadline-label */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/deadline-label/deadline-label.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"DeadlineLabel\", function() { return _deadline_label__WEBPACK_IMPORTED_MODULE_0__[\"DeadlineLabel\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/deadline-label/index.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/index.js":
/*!********************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/index.js ***!
  \********************************************************************************************/
/*! exports provided: Labels */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _labels__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./labels */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/labels.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Labels\", function() { return _labels__WEBPACK_IMPORTED_MODULE_0__[\"Labels\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/index.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/labels.css":
/*!**********************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/labels.css ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../../../node_modules/css-loader/dist/cjs.js!./labels.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/labels.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/labels.css?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/labels.js":
/*!*********************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/labels.js ***!
  \*********************************************************************************************/
/*! exports provided: Labels */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Labels\", function() { return Labels; });\n/* harmony import */ var _labels_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./labels.css */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/labels.css\");\n/* harmony import */ var _labels_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_labels_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _title_label__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./title-label */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/title-label/index.js\");\n/* harmony import */ var _deadline_label__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./deadline-label */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/deadline-label/index.js\");\n/* harmony import */ var _time_label__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./time-label */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/time-label/index.js\");\n/* harmony import */ var _category_label__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./category-label */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/category-label/index.js\");\n/* harmony import */ var _progress_label__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./progress-label */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/progress-label/index.js\");\n/* harmony import */ var _priority_label__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./priority-label */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/priority-label/index.js\");\n/* harmony import */ var _comment_label__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./comment-label */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/comment-label/index.js\");\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nfunction Labels() {\r\n    const labels = document.createElement('div');\r\n\r\n    labels.classList.add('labels', 'd-flex', 'justify-content-between', 'flex-wrap');\r\n\r\n    labels.append(Object(_title_label__WEBPACK_IMPORTED_MODULE_1__[\"TitleLabel\"])(), Object(_deadline_label__WEBPACK_IMPORTED_MODULE_2__[\"DeadlineLabel\"])(), Object(_time_label__WEBPACK_IMPORTED_MODULE_3__[\"TimeLabel\"])(), Object(_category_label__WEBPACK_IMPORTED_MODULE_4__[\"CategoryLabel\"])(), Object(_progress_label__WEBPACK_IMPORTED_MODULE_5__[\"ProgressLabel\"])(), Object(_priority_label__WEBPACK_IMPORTED_MODULE_6__[\"PriorityLabel\"])(), Object(_comment_label__WEBPACK_IMPORTED_MODULE_7__[\"CommentLabel\"])());\r\n\r\n    return labels;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/labels.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/priority-label/index.js":
/*!***********************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/priority-label/index.js ***!
  \***********************************************************************************************************/
/*! exports provided: PriorityLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _priority_label__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./priority-label */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/priority-label/priority-label.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"PriorityLabel\", function() { return _priority_label__WEBPACK_IMPORTED_MODULE_0__[\"PriorityLabel\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/priority-label/index.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/priority-label/priority-label.css":
/*!*********************************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/priority-label/priority-label.css ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../../../../node_modules/css-loader/dist/cjs.js!./priority-label.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/priority-label/priority-label.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/priority-label/priority-label.css?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/priority-label/priority-label.js":
/*!********************************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/priority-label/priority-label.js ***!
  \********************************************************************************************************************/
/*! exports provided: PriorityLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"PriorityLabel\", function() { return PriorityLabel; });\n/* harmony import */ var _priority_label_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./priority-label.css */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/priority-label/priority-label.css\");\n/* harmony import */ var _priority_label_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_priority_label_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction PriorityLabel() {\r\n    const priorityLabel = document.createElement('label');\r\n    const priorityTitle = document.createElement('div');\r\n    const priorityInput = document.createElement('input');\r\n\r\n    priorityLabel.classList.add('priority-label');\r\n    priorityTitle.classList.add('priority-title');\r\n    priorityInput.classList.add('custom-range', 'priority-input');\r\n\r\n    priorityInput.setAttribute('type', 'range');\r\n    priorityInput.setAttribute('min', '0');\r\n    priorityInput.setAttribute('max', '2');\r\n    priorityInput.setAttribute('value', '0');\r\n    priorityInput.setAttribute('id', 'info-priority');\r\n\r\n    priorityTitle.innerHTML = 'priority:';\r\n\r\n    priorityLabel.append(priorityTitle, priorityInput);\r\n    \r\n\r\n    function renderPriority() {\r\n        priorityInput.addEventListener('input', () => {\r\n            let priority;\r\n            if(priorityInput.value == 0) {\r\n                priority = 'low'\r\n            } else if(priorityInput.value == 1) {\r\n                priority = 'middle'\r\n            } else if(priorityInput.value == 2) {\r\n                priority = 'important'\r\n            }\r\n\r\n            priorityTitle.textContent = `priority: ${priority}`;\r\n        })\r\n    };\r\n\r\n    renderPriority();\r\n\r\n    return priorityLabel;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/priority-label/priority-label.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/progress-label/index.js":
/*!***********************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/progress-label/index.js ***!
  \***********************************************************************************************************/
/*! exports provided: ProgressLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _progress_label__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./progress-label */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/progress-label/progress-label.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"ProgressLabel\", function() { return _progress_label__WEBPACK_IMPORTED_MODULE_0__[\"ProgressLabel\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/progress-label/index.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/progress-label/progress-label.css":
/*!*********************************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/progress-label/progress-label.css ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../../../../node_modules/css-loader/dist/cjs.js!./progress-label.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/progress-label/progress-label.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/progress-label/progress-label.css?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/progress-label/progress-label.js":
/*!********************************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/progress-label/progress-label.js ***!
  \********************************************************************************************************************/
/*! exports provided: ProgressLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ProgressLabel\", function() { return ProgressLabel; });\n/* harmony import */ var _progress_label_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./progress-label.css */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/progress-label/progress-label.css\");\n/* harmony import */ var _progress_label_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_progress_label_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction ProgressLabel() {\r\n    const progressLabel = document.createElement('label');\r\n    const progressTitle = document.createElement('div');\r\n    const progressInput = document.createElement('input');\r\n\r\n    progressLabel.classList.add('progress-label');\r\n    progressTitle.classList.add('progress-title');\r\n    progressInput.classList.add('custom-range', 'progress-input');\r\n\r\n    progressInput.setAttribute('type', 'range');\r\n    progressInput.setAttribute('min', '0');\r\n    progressInput.setAttribute('max', '4');\r\n    progressInput.setAttribute('value', '0');\r\n\r\n    progressTitle.innerHTML = 'progress: 0%';\r\n\r\n    progressLabel.append(progressTitle, progressInput);\r\n\r\n\r\n    progressInput.addEventListener('input', () => {\r\n        let progress;\r\n        if(+progressInput.value === 0) {\r\n            progress = 0;\r\n        } else if(+progressInput.value === 1) {\r\n            progress = 25;\r\n        } else if(+progressInput.value === 2) {\r\n            progress = 50;\r\n        } else if(+progressInput.value === 3) {\r\n            progress = 75;\r\n        } else {\r\n            progress = 100;\r\n        }\r\n\r\n        progressTitle.textContent = `progress: ${progress}%`;\r\n    })\r\n\r\n    return progressLabel;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/progress-label/progress-label.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/time-label/index.js":
/*!*******************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/time-label/index.js ***!
  \*******************************************************************************************************/
/*! exports provided: TimeLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _time_label__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./time-label */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/time-label/time-label.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"TimeLabel\", function() { return _time_label__WEBPACK_IMPORTED_MODULE_0__[\"TimeLabel\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/time-label/index.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/time-label/time-label.css":
/*!*************************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/time-label/time-label.css ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../../../../node_modules/css-loader/dist/cjs.js!./time-label.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/time-label/time-label.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/time-label/time-label.css?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/time-label/time-label.js":
/*!************************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/time-label/time-label.js ***!
  \************************************************************************************************************/
/*! exports provided: TimeLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"TimeLabel\", function() { return TimeLabel; });\n/* harmony import */ var _time_label_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./time-label.css */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/time-label/time-label.css\");\n/* harmony import */ var _time_label_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_time_label_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction TimeLabel() {\r\n    const timeLabel = document.createElement('label');\r\n    const title = document.createElement('div');\r\n    const timeInput = document.createElement('input');\r\n\r\n    timeLabel.classList.add('time-label');\r\n    timeInput.classList.add('form-control');\r\n\r\n    timeInput.setAttribute('type', 'time');\r\n    timeInput.setAttribute('id', 'info-time');\r\n    timeInput.setAttribute('autocomplete', 'off');\r\n\r\n    title.innerHTML = 'time:';\r\n\r\n    timeLabel.append(title, timeInput);\r\n\r\n    return timeLabel\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/time-label/time-label.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/title-label/index.js":
/*!********************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/title-label/index.js ***!
  \********************************************************************************************************/
/*! exports provided: TitleLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _title_label__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./title-label */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/title-label/title-label.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"TitleLabel\", function() { return _title_label__WEBPACK_IMPORTED_MODULE_0__[\"TitleLabel\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/title-label/index.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/title-label/title-label.css":
/*!***************************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/title-label/title-label.css ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../../../../node_modules/css-loader/dist/cjs.js!./title-label.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/title-label/title-label.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/title-label/title-label.css?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/title-label/title-label.js":
/*!**************************************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/title-label/title-label.js ***!
  \**************************************************************************************************************/
/*! exports provided: TitleLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"TitleLabel\", function() { return TitleLabel; });\n/* harmony import */ var _title_label_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./title-label.css */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/title-label/title-label.css\");\n/* harmony import */ var _title_label_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_title_label_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction TitleLabel() {\r\n    const titleLabel = document.createElement('label');\r\n    const title = document.createElement('div');\r\n    const titleInput = document.createElement('input');\r\n\r\n    titleLabel.classList.add('title-label');\r\n    titleInput.classList.add('form-control', 'info-title');\r\n\r\n    titleInput.setAttribute('id', 'info-title');\r\n    titleInput.setAttribute('type', 'text');\r\n    titleInput.setAttribute('required', 'required');\r\n    \r\n    title.innerHTML = 'task title:';\r\n\r\n    titleLabel.append(title, titleInput);\r\n\r\n    return titleLabel\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/labels/title-label/title-label.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/title/index.js":
/*!*******************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/title/index.js ***!
  \*******************************************************************************************/
/*! exports provided: Title */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _title__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./title */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/title/title.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Title\", function() { return _title__WEBPACK_IMPORTED_MODULE_0__[\"Title\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/title/index.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/title/title.css":
/*!********************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/title/title.css ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../../../node_modules/css-loader/dist/cjs.js!./title.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/change-form/form-group/title/title.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/title/title.css?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/form-group/title/title.js":
/*!*******************************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/form-group/title/title.js ***!
  \*******************************************************************************************/
/*! exports provided: Title */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Title\", function() { return Title; });\n/* harmony import */ var _title_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./title.css */ \"./src/app/main/modal/modal-content/modal-body/change-form/form-group/title/title.css\");\n/* harmony import */ var _title_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_title_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction Title() {\r\n    const title = document.createElement('h2');\r\n\r\n    title.classList.add('h2', 'display-4', 'row', 'justify-content-center');\r\n\r\n    title.innerHTML = 'Edit task:';\r\n\r\n    return title;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/form-group/title/title.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/change-form/index.js":
/*!**************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/change-form/index.js ***!
  \**************************************************************************/
/*! exports provided: ChangeForm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _change_form_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./change-form.js */ \"./src/app/main/modal/modal-content/modal-body/change-form/change-form.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"ChangeForm\", function() { return _change_form_js__WEBPACK_IMPORTED_MODULE_0__[\"ChangeForm\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/change-form/index.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/modal-body.css":
/*!********************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/modal-body.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./modal-body.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-body/modal-body.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/modal-body.css?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-body/modal-body.js":
/*!*******************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-body/modal-body.js ***!
  \*******************************************************************/
/*! exports provided: ModalBody */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ModalBody\", function() { return ModalBody; });\n/* harmony import */ var _modal_body_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-body.css */ \"./src/app/main/modal/modal-content/modal-body/modal-body.css\");\n/* harmony import */ var _modal_body_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_body_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _change_form__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./change-form */ \"./src/app/main/modal/modal-content/modal-body/change-form/index.js\");\n\r\n\r\n\r\nfunction ModalBody() {\r\n    const modalBody = document.createElement('div');\r\n    const p = document.createElement('p');\r\n\r\n    modalBody.classList.add('modal-body');\r\n    modalBody.append(p, Object(_change_form__WEBPACK_IMPORTED_MODULE_1__[\"ChangeForm\"])());\r\n\r\n    return modalBody;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-body/modal-body.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-content.css":
/*!************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-content.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./modal-content.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-content.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-content.css?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-content.js":
/*!***********************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-content.js ***!
  \***********************************************************/
/*! exports provided: ModalContent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ModalContent\", function() { return ModalContent; });\n/* harmony import */ var _modal_content_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-content.css */ \"./src/app/main/modal/modal-content/modal-content.css\");\n/* harmony import */ var _modal_content_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_content_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _modal_header_modal_header_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modal-header/modal-header.js */ \"./src/app/main/modal/modal-content/modal-header/modal-header.js\");\n/* harmony import */ var _modal_body_modal_body__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modal-body/modal-body */ \"./src/app/main/modal/modal-content/modal-body/modal-body.js\");\n/* harmony import */ var _modal_footer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modal-footer */ \"./src/app/main/modal/modal-content/modal-footer/index.js\");\n\r\n\r\n\r\n\r\n\r\nfunction ModalContent() {\r\n    const modalContent = document.createElement('div');\r\n\r\n    modalContent.classList.add('modal-content');\r\n\r\n    modalContent.append(Object(_modal_header_modal_header_js__WEBPACK_IMPORTED_MODULE_1__[\"ModalHeader\"])(), Object(_modal_body_modal_body__WEBPACK_IMPORTED_MODULE_2__[\"ModalBody\"])(), Object(_modal_footer__WEBPACK_IMPORTED_MODULE_3__[\"ModalFooter\"])());\r\n\r\n    return modalContent;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-content.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-footer/change-btn/change-btn.css":
/*!*********************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-footer/change-btn/change-btn.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader/dist/cjs.js!./change-btn.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-footer/change-btn/change-btn.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-footer/change-btn/change-btn.css?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-footer/change-btn/change-btn.js":
/*!********************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-footer/change-btn/change-btn.js ***!
  \********************************************************************************/
/*! exports provided: ChangeBtn */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ChangeBtn\", function() { return ChangeBtn; });\n/* harmony import */ var _change_btn_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./change-btn.css */ \"./src/app/main/modal/modal-content/modal-footer/change-btn/change-btn.css\");\n/* harmony import */ var _change_btn_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_change_btn_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../server-requests/server-requests */ \"./src/app/server-requests/server-requests.js\");\n/* harmony import */ var _header_container_search_label_search_input_group_search_getAutocompleteTitles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../header/container/search-label/search-input-group/search/getAutocompleteTitles */ \"./src/app/header/container/search-label/search-input-group/search/getAutocompleteTitles.js\");\n/* harmony import */ var _task_helpers_changeDoneTitle__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../task helpers/changeDoneTitle */ \"./src/app/task helpers/changeDoneTitle.js\");\n/* harmony import */ var _task_helpers_changeStatusTitle__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../task helpers/changeStatusTitle */ \"./src/app/task helpers/changeStatusTitle.js\");\n\r\n\r\n\r\n\r\n\r\n\r\nfunction ChangeBtn() {\r\n    const changeBtn = document.createElement('button');\r\n    const content = `\r\n        <i class=\"fa fa-floppy-o\" aria-hidden=\"true\"></i>\r\n        save\r\n    `;\r\n\r\n    changeBtn.classList.add('change-button', 'btn', 'btn-primary');\r\n    changeBtn.innerHTML = content;\r\n\r\n\r\n    changeBtn.addEventListener('click', changeTask);\r\n\r\n    function changeTask() {\r\n        const title = document.querySelector('.info-title').value;\r\n        const deadlineDate = document.querySelector('#info-date').value;\r\n        const category = document.querySelector('#info-category').value;\r\n        const priorityValue = +document.querySelector('.priority-input').value;\r\n        const progressValue = +document.querySelector('.progress-input').value;\r\n        const time = document.querySelector('#info-time').value;\r\n        const comment = document.querySelector('.comment-info').value;\r\n        // const userId = document.querySelector('.user-name-input').getAttribute('data-id');\r\n        const userId = localStorage.getItem('id');\r\n\r\n        let priority;\r\n        if(priorityValue === 0) {\r\n            priority = 'low'\r\n        } else if(priorityValue === 1) {\r\n            priority = 'middle'\r\n        } else if(priorityValue === 2) {\r\n            priority = 'important'\r\n        }\r\n\r\n        let progress;\r\n        if(progressValue === 0) {\r\n            progress = 0;\r\n        } else if(progressValue === 1) {\r\n            progress = 25;\r\n        } else if(progressValue === 2) {\r\n            progress = 50;\r\n        } else if(progressValue === 3) {\r\n            progress = 75;\r\n        } else {\r\n            progress = 100;\r\n        }\r\n        \r\n        const task = {\r\n            title,\r\n            deadlineDate,\r\n            time,\r\n            category,\r\n            progress,\r\n            priority,\r\n            comment\r\n        }    \r\n\r\n        const id = document.querySelector('.info-title').getAttribute('data-id');\r\n    \r\n        fetch('http://localhost:2000/tasks/'+id, {\r\n            method: 'PUT',\r\n            headers: {\r\n                'Content-Type': 'application/json'\r\n            },\r\n            body: JSON.stringify(task)\r\n        })\r\n        .then(res => res.json())\r\n        .then(id => {\r\n            const changeTrEl = document.querySelector(`tr[data-id=\"${id}\"]`);\r\n            const childOfTr = changeTrEl.childNodes;\r\n\r\n            childOfTr[0].textContent = title;\r\n            childOfTr[1].textContent = deadlineDate;\r\n            childOfTr[2].textContent = time;\r\n            childOfTr[3].textContent = category;\r\n            childOfTr[4].textContent = `${progress}%`;\r\n            childOfTr[5].textContent = priority;\r\n\r\n            const modal = document.querySelector('.modal');\r\n            modal.style.display = 'none';\r\n\r\n            const getTasks = new _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_1__[\"ServerRequests\"]().getUserTasks(userId);\r\n\r\n            Object(_header_container_search_label_search_input_group_search_getAutocompleteTitles__WEBPACK_IMPORTED_MODULE_2__[\"getAutocompleteTitles\"])(getTasks);\r\n\r\n            new _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_1__[\"ServerRequests\"]().getTask(id).then(task => {\r\n                Object(_task_helpers_changeDoneTitle__WEBPACK_IMPORTED_MODULE_3__[\"changeDoneTitle\"])(task);\r\n                Object(_task_helpers_changeStatusTitle__WEBPACK_IMPORTED_MODULE_4__[\"changeStatusTitle\"])(task)\r\n            })        \r\n        })\r\n    }\r\n    return changeBtn;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-footer/change-btn/change-btn.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-footer/change-btn/index.js":
/*!***************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-footer/change-btn/index.js ***!
  \***************************************************************************/
/*! exports provided: ChangeBtn */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _change_btn_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./change-btn.js */ \"./src/app/main/modal/modal-content/modal-footer/change-btn/change-btn.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"ChangeBtn\", function() { return _change_btn_js__WEBPACK_IMPORTED_MODULE_0__[\"ChangeBtn\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-footer/change-btn/index.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-footer/delete-btn/delete-btn.css":
/*!*********************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-footer/delete-btn/delete-btn.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader/dist/cjs.js!./delete-btn.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-footer/delete-btn/delete-btn.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-footer/delete-btn/delete-btn.css?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-footer/delete-btn/delete-btn.js":
/*!********************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-footer/delete-btn/delete-btn.js ***!
  \********************************************************************************/
/*! exports provided: DeleteBtn */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"DeleteBtn\", function() { return DeleteBtn; });\n/* harmony import */ var _delete_btn_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./delete-btn.css */ \"./src/app/main/modal/modal-content/modal-footer/delete-btn/delete-btn.css\");\n/* harmony import */ var _delete_btn_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_delete_btn_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../server-requests/server-requests */ \"./src/app/server-requests/server-requests.js\");\n/* harmony import */ var _header_container_search_label_search_input_group_search_getAutocompleteTitles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../header/container/search-label/search-input-group/search/getAutocompleteTitles */ \"./src/app/header/container/search-label/search-input-group/search/getAutocompleteTitles.js\");\n\r\n\r\n\r\n\r\nfunction DeleteBtn() {\r\n    const deleteBtn = document.createElement('button');\r\n    const content = `\r\n        <i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i>\r\n        del\r\n    `;\r\n\r\n    deleteBtn.classList.add('delete-button', 'btn', 'btn-danger');\r\n    deleteBtn.innerHTML = content;\r\n\r\n    deleteBtn.addEventListener('click', deleteTask);\r\n\r\n    function deleteTask() {\r\n        const id = document.querySelector('.info-title').getAttribute('data-id');\r\n\r\n        fetch('http://localhost:2000/tasks/'+id, {\r\n            method: 'Delete',\r\n            headers: {\r\n                'Content-Type': 'application/json'\r\n            },\r\n        }).then(res => {\r\n            return res.json();\r\n        }).then(id => {\r\n            const delTrEl = document.querySelector(`tr[data-id=\"${id}\"]`);\r\n            const modal = document.querySelector('.modal');\r\n            const userId = localStorage.getAttribute('id');\r\n            const getTasks = new _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_1__[\"ServerRequests\"]().getUserTasks(userId);\r\n\r\n            delTrEl.remove();\r\n            \r\n            modal.style.display = 'none';\r\n\r\n            Object(_header_container_search_label_search_input_group_search_getAutocompleteTitles__WEBPACK_IMPORTED_MODULE_2__[\"getAutocompleteTitles\"])(getTasks);\r\n        })\r\n    }\r\n\r\n    return deleteBtn;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-footer/delete-btn/delete-btn.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-footer/delete-btn/index.js":
/*!***************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-footer/delete-btn/index.js ***!
  \***************************************************************************/
/*! exports provided: DeleteBtn */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _delete_btn_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./delete-btn.js */ \"./src/app/main/modal/modal-content/modal-footer/delete-btn/delete-btn.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"DeleteBtn\", function() { return _delete_btn_js__WEBPACK_IMPORTED_MODULE_0__[\"DeleteBtn\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-footer/delete-btn/index.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-footer/index.js":
/*!****************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-footer/index.js ***!
  \****************************************************************/
/*! exports provided: ModalFooter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modal_footer_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-footer.js */ \"./src/app/main/modal/modal-content/modal-footer/modal-footer.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"ModalFooter\", function() { return _modal_footer_js__WEBPACK_IMPORTED_MODULE_0__[\"ModalFooter\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-footer/index.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-footer/modal-footer.css":
/*!************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-footer/modal-footer.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./modal-footer.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-footer/modal-footer.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-footer/modal-footer.css?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-footer/modal-footer.js":
/*!***********************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-footer/modal-footer.js ***!
  \***********************************************************************/
/*! exports provided: ModalFooter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ModalFooter\", function() { return ModalFooter; });\n/* harmony import */ var _modal_footer_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-footer.css */ \"./src/app/main/modal/modal-content/modal-footer/modal-footer.css\");\n/* harmony import */ var _modal_footer_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_footer_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _change_btn__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./change-btn */ \"./src/app/main/modal/modal-content/modal-footer/change-btn/index.js\");\n/* harmony import */ var _delete_btn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./delete-btn */ \"./src/app/main/modal/modal-content/modal-footer/delete-btn/index.js\");\n\r\n\r\n\r\n\r\nfunction ModalFooter() {\r\nconst modalFooter = document.createElement('div');\r\n\r\nmodalFooter.classList.add('modal-footer', 'bg-info');\r\n\r\nmodalFooter.append(Object(_change_btn__WEBPACK_IMPORTED_MODULE_1__[\"ChangeBtn\"])(), Object(_delete_btn__WEBPACK_IMPORTED_MODULE_2__[\"DeleteBtn\"])());\r\n\r\nreturn modalFooter\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-footer/modal-footer.js?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-header/modal-header.css":
/*!************************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-header/modal-header.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./modal-header.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal-content/modal-header/modal-header.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-header/modal-header.css?");

/***/ }),

/***/ "./src/app/main/modal/modal-content/modal-header/modal-header.js":
/*!***********************************************************************!*\
  !*** ./src/app/main/modal/modal-content/modal-header/modal-header.js ***!
  \***********************************************************************/
/*! exports provided: ModalHeader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ModalHeader\", function() { return ModalHeader; });\n/* harmony import */ var _modal_header_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-header.css */ \"./src/app/main/modal/modal-content/modal-header/modal-header.css\");\n/* harmony import */ var _modal_header_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_header_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction ModalHeader() {\r\n    const modalHeader = document.createElement('div');\r\n    const title = document.createElement('h2');\r\n    const close = document.createElement('span');\r\n\r\n    modalHeader.classList.add('modal-header', 'bg-info');\r\n    close.classList.add('close');\r\n\r\n    close.innerHTML = '&times;';\r\n\r\n    modalHeader.append(title, close);\r\n\r\n    \r\n    close.addEventListener('click', closeModal);\r\n\r\n    function closeModal(ev) {\r\n        const taskModal = document.querySelector('.modal');\r\n        taskModal.style.display = 'none'\r\n    }\r\n   \r\n    window.addEventListener('click', (ev) => {\r\n        const taskModal = document.querySelector('.modal');\r\n        if(ev.target == taskModal) {\r\n            taskModal.style.display = 'none';\r\n        }\r\n    })\r\n\r\n\r\n\r\n\r\n    return modalHeader;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/modal-content/modal-header/modal-header.js?");

/***/ }),

/***/ "./src/app/main/modal/modal.css":
/*!**************************************!*\
  !*** ./src/app/main/modal/modal.css ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js!./modal.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/modal/modal.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/modal/modal.css?");

/***/ }),

/***/ "./src/app/main/modal/modal.js":
/*!*************************************!*\
  !*** ./src/app/main/modal/modal.js ***!
  \*************************************/
/*! exports provided: Modal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Modal\", function() { return Modal; });\n/* harmony import */ var _modal_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal.css */ \"./src/app/main/modal/modal.css\");\n/* harmony import */ var _modal_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _modal_content__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modal-content */ \"./src/app/main/modal/modal-content/index.js\");\n\r\n\r\n\r\nfunction Modal() {\r\n    const modal = document.createElement('div');\r\n\r\n    modal.classList.add('modal');\r\n    modal.setAttribute('id', 'infoModal');\r\n    modal.append(Object(_modal_content__WEBPACK_IMPORTED_MODULE_1__[\"ModalContent\"])());\r\n\r\n    return modal;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/modal.js?");

/***/ }),

/***/ "./src/app/main/modal/render-tasks.js":
/*!********************************************!*\
  !*** ./src/app/main/modal/render-tasks.js ***!
  \********************************************/
/*! exports provided: renderTaskInfoToModalChange */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"renderTaskInfoToModalChange\", function() { return renderTaskInfoToModalChange; });\nfunction renderTaskInfoToModalChange(task) {\r\n    const titleInput = document.querySelector('.info-title');\r\n    const deadlineInput = document.querySelector('#info-date');\r\n    const categoryInput = document.querySelector('#info-category');\r\n    const timeInput = document.querySelector('#info-time');\r\n    const priority = document.querySelector('#info-priority');\r\n    const modalTitle = document.querySelector('.modal .modal-header h2');\r\n    const createDate = document.querySelector('.modal .modal-body p');\r\n    const priorityTitle = document.querySelector('.priority-title');\r\n    const progressInput = document.querySelector('.progress-input');\r\n    const progressTitle = document.querySelector('.progress-title');\r\n    const commentInfo = document.querySelector('.comment-info');\r\n\r\n    titleInput.value = task.title;\r\n    titleInput.setAttribute('data-id', task.id);\r\n    deadlineInput.value = task.deadlineDate;\r\n    categoryInput.value = task.category;\r\n    timeInput.value = task.time;\r\n    commentInfo.value = task.comment;\r\n    \r\n    let progressValue;\r\n    if(task.progress === 0) {\r\n        progressValue = 0;\r\n    } else if(task.progress === 25) {\r\n        progressValue = 1;\r\n    } else if(task.progress === 50) {\r\n        progressValue = 2;\r\n    } else if(task.progress === 75) {\r\n        progressValue = 3;\r\n    } else if(task.progress === 100) {\r\n        progressValue = 4;\r\n    }\r\n\r\n    progressTitle.textContent = `progress: ${task.progress}%`;\r\n    progressInput.value = progressValue;\r\n\r\n    let priorityValue;\r\n    if(task.priority == 'low') {\r\n        priorityValue = 0;\r\n    } else if(task.priority == 'middle') {\r\n        priorityValue = 1;\r\n    } else {\r\n        priorityValue = 2;\r\n    }\r\n    priority.value = priorityValue;\r\n    modalTitle.textContent = task.title;\r\n    createDate.textContent = `create date: ${task.date}`;\r\n    priorityTitle.textContent = `priority: ${task.priority}`;\r\n\r\n    return task;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/modal/render-tasks.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/all-tasks/all-tasks.css":
/*!***************************************************************!*\
  !*** ./src/app/main/tasks-filtration/all-tasks/all-tasks.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./all-tasks.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/all-tasks/all-tasks.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/all-tasks/all-tasks.css?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/all-tasks/all-tasks.js":
/*!**************************************************************!*\
  !*** ./src/app/main/tasks-filtration/all-tasks/all-tasks.js ***!
  \**************************************************************/
/*! exports provided: AllTasks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"AllTasks\", function() { return AllTasks; });\n/* harmony import */ var _all_tasks_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./all-tasks.css */ \"./src/app/main/tasks-filtration/all-tasks/all-tasks.css\");\n/* harmony import */ var _all_tasks_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_all_tasks_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./table */ \"./src/app/main/tasks-filtration/all-tasks/table/index.js\");\n\r\n\r\n\r\n\r\nfunction AllTasks() {\r\n    const allTasks = document.createElement('div');\r\n\r\n    allTasks.classList.add('all-tasks');\r\n    allTasks.append(Object(_table__WEBPACK_IMPORTED_MODULE_1__[\"Table\"])());\r\n\r\n    return allTasks;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/all-tasks/all-tasks.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/all-tasks/index.js":
/*!**********************************************************!*\
  !*** ./src/app/main/tasks-filtration/all-tasks/index.js ***!
  \**********************************************************/
/*! exports provided: AllTasks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _all_tasks_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./all-tasks.js */ \"./src/app/main/tasks-filtration/all-tasks/all-tasks.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"AllTasks\", function() { return _all_tasks_js__WEBPACK_IMPORTED_MODULE_0__[\"AllTasks\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/all-tasks/index.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/all-tasks/table/index.js":
/*!****************************************************************!*\
  !*** ./src/app/main/tasks-filtration/all-tasks/table/index.js ***!
  \****************************************************************/
/*! exports provided: Table */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _table_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./table.js */ \"./src/app/main/tasks-filtration/all-tasks/table/table.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Table\", function() { return _table_js__WEBPACK_IMPORTED_MODULE_0__[\"Table\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/all-tasks/table/index.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/all-tasks/table/table.css":
/*!*****************************************************************!*\
  !*** ./src/app/main/tasks-filtration/all-tasks/table/table.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./table.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/all-tasks/table/table.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/all-tasks/table/table.css?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/all-tasks/table/table.js":
/*!****************************************************************!*\
  !*** ./src/app/main/tasks-filtration/all-tasks/table/table.js ***!
  \****************************************************************/
/*! exports provided: Table */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Table\", function() { return Table; });\n/* harmony import */ var _table_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./table.css */ \"./src/app/main/tasks-filtration/all-tasks/table/table.css\");\n/* harmony import */ var _table_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_table_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _thead__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./thead */ \"./src/app/main/tasks-filtration/all-tasks/table/thead/index.js\");\n/* harmony import */ var _tbody__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tbody */ \"./src/app/main/tasks-filtration/all-tasks/table/tbody/index.js\");\n\r\n\r\n\r\n\r\nfunction Table() {\r\n    const table = document.createElement('table');\r\n\r\n    table.classList.add('table');\r\n\r\n    table.append(Object(_thead__WEBPACK_IMPORTED_MODULE_1__[\"Thead\"])(), Object(_tbody__WEBPACK_IMPORTED_MODULE_2__[\"Tbody\"])());\r\n    \r\n\r\n    return table;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/all-tasks/table/table.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/all-tasks/table/tbody/get-tr.js":
/*!***********************************************************************!*\
  !*** ./src/app/main/tasks-filtration/all-tasks/table/tbody/get-tr.js ***!
  \***********************************************************************/
/*! exports provided: getTr */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getTr\", function() { return getTr; });\n/* harmony import */ var _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../server-requests/server-requests */ \"./src/app/server-requests/server-requests.js\");\n/* harmony import */ var _modal_render_tasks__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../modal/render-tasks */ \"./src/app/main/modal/render-tasks.js\");\n\r\n\r\n\r\nfunction getTr(task) {\r\n    const tr = document.createElement('tr');\r\n    const taskTitleTd = getTd(task.title);\r\n    const taskDeadlineDateTd = getTd(task.deadlineDate);\r\n    const taskTimeTd = getTd(task.time);\r\n    const taskCategoryTd = getTd(task.category);\r\n    const taskProgressTd = getTd(`${task.progress}%`);\r\n    const taskPriorityTd = getTd(task.priority);\r\n\r\n    taskPriorityTd.style.fontWeight = 'bold';\r\n\r\n    taskTitleTd.setAttribute('class', 'title');\r\n\r\n    tr.append(taskTitleTd, taskDeadlineDateTd, taskTimeTd, taskCategoryTd, taskProgressTd, taskPriorityTd);\r\n    tr.setAttribute('data-id', task.id);\r\n    tr.setAttribute('style', 'display: table-row');\r\n\r\n    tr.addEventListener('click', getInfoTaskFromTable);\r\n\r\n\r\n    function getInfoTaskFromTable(ev) {\r\n        const id = ev.target.parentElement.getAttribute('data-id');\r\n        \r\n        new _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_0__[\"ServerRequests\"]().getTask(id)\r\n        .then(task => {\r\n            Object(_modal_render_tasks__WEBPACK_IMPORTED_MODULE_1__[\"renderTaskInfoToModalChange\"])(task);\r\n\r\n            const modal = document.querySelector('.modal');\r\n            const titleOfModal = document.querySelector('.modal .modal-header h2');\r\n            \r\n            const title = ev.target.parentElement.firstChild.textContent;\r\n\r\n            modal.style.display = \"block\";\r\n            titleOfModal.innerHTML = `${title}`;\r\n        })\r\n    }\r\n\r\n    \r\n    function getTd(content) {\r\n        const td = document.createElement('td');\r\n    \r\n        td.textContent = content;\r\n\r\n        if(td.textContent === 'low') {\r\n            td.style.color = '#28a745';\r\n        } else if(td.textContent === 'middle') {\r\n            td.style.color = '#ffc107';\r\n        } else if(td.textContent === 'important'){\r\n            td.style.color = '#dc3545';\r\n        }\r\n        return td;\r\n    }\r\n\r\n    return tr;\r\n} \n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/all-tasks/table/tbody/get-tr.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/all-tasks/table/tbody/index.js":
/*!**********************************************************************!*\
  !*** ./src/app/main/tasks-filtration/all-tasks/table/tbody/index.js ***!
  \**********************************************************************/
/*! exports provided: Tbody */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _tbody_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tbody.js */ \"./src/app/main/tasks-filtration/all-tasks/table/tbody/tbody.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Tbody\", function() { return _tbody_js__WEBPACK_IMPORTED_MODULE_0__[\"Tbody\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/all-tasks/table/tbody/index.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/all-tasks/table/tbody/tbody.css":
/*!***********************************************************************!*\
  !*** ./src/app/main/tasks-filtration/all-tasks/table/tbody/tbody.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader/dist/cjs.js!./tbody.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/all-tasks/table/tbody/tbody.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/all-tasks/table/tbody/tbody.css?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/all-tasks/table/tbody/tbody.js":
/*!**********************************************************************!*\
  !*** ./src/app/main/tasks-filtration/all-tasks/table/tbody/tbody.js ***!
  \**********************************************************************/
/*! exports provided: Tbody */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Tbody\", function() { return Tbody; });\n/* harmony import */ var _tbody_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tbody.css */ \"./src/app/main/tasks-filtration/all-tasks/table/tbody/tbody.css\");\n/* harmony import */ var _tbody_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_tbody_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\n\r\nfunction Tbody() {\r\n    const tbody = document.createElement('tbody');\r\n\r\n    tbody.classList.add('tbody', 'tasks');\r\n\r\n    return tbody;\r\n}\r\n\r\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/all-tasks/table/tbody/tbody.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/all-tasks/table/thead/index.js":
/*!**********************************************************************!*\
  !*** ./src/app/main/tasks-filtration/all-tasks/table/thead/index.js ***!
  \**********************************************************************/
/*! exports provided: Thead */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _thead_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./thead.js */ \"./src/app/main/tasks-filtration/all-tasks/table/thead/thead.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"Thead\", function() { return _thead_js__WEBPACK_IMPORTED_MODULE_0__[\"Thead\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/all-tasks/table/thead/index.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/all-tasks/table/thead/thead.css":
/*!***********************************************************************!*\
  !*** ./src/app/main/tasks-filtration/all-tasks/table/thead/thead.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader/dist/cjs.js!./thead.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/all-tasks/table/thead/thead.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/all-tasks/table/thead/thead.css?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/all-tasks/table/thead/thead.js":
/*!**********************************************************************!*\
  !*** ./src/app/main/tasks-filtration/all-tasks/table/thead/thead.js ***!
  \**********************************************************************/
/*! exports provided: Thead */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Thead\", function() { return Thead; });\n/* harmony import */ var _thead_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./thead.css */ \"./src/app/main/tasks-filtration/all-tasks/table/thead/thead.css\");\n/* harmony import */ var _thead_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_thead_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction Thead() {\r\n    const tHead = document.createElement('thead');\r\n    const content = `\r\n    <tr>\r\n        <th scope=\"col\">task title</th>\r\n        <th scope=\"col\">deadline date</th>\r\n        <th scope=\"col\">time</th>\r\n        <th scope=\"col\">category</th>\r\n        <th scope=\"col\">progress</th>\r\n        <th scope=\"col\">priority</th>\r\n    </tr>\r\n    `;\r\n\r\n    tHead.classList.add('thead', 'thead-dark');\r\n    tHead.innerHTML = content;\r\n\r\n    return tHead;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/all-tasks/table/thead/thead.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/category-filtration/category-filtration.css":
/*!***********************************************************************************!*\
  !*** ./src/app/main/tasks-filtration/category-filtration/category-filtration.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./category-filtration.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/category-filtration/category-filtration.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/category-filtration/category-filtration.css?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/category-filtration/category-filtration.js":
/*!**********************************************************************************!*\
  !*** ./src/app/main/tasks-filtration/category-filtration/category-filtration.js ***!
  \**********************************************************************************/
/*! exports provided: CategoryFiltration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"CategoryFiltration\", function() { return CategoryFiltration; });\n/* harmony import */ var _category_filtration_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./category-filtration.css */ \"./src/app/main/tasks-filtration/category-filtration/category-filtration.css\");\n/* harmony import */ var _category_filtration_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_category_filtration_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _modal_add_task_form_modal_body_category_categoryRender__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../modal-add-task/form/modal-body/category/categoryRender */ \"./src/app/main/modal-add-task/form/modal-body/category/categoryRender.js\");\n/* harmony import */ var _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../server-requests/server-requests */ \"./src/app/server-requests/server-requests.js\");\n\r\n\r\n\r\n\r\nfunction CategoryFiltration() {\r\n    const categoryFiltration = document.createElement('label');\r\n    const categorySelect = document.createElement('select');\r\n\r\n    const content = `\r\n        <option value=\"all\">all</option>\r\n    `;\r\n\r\n    categoryFiltration.classList.add('category-iltration');\r\n    categorySelect.classList.add('form-control', 'category', 'category-filter');\r\n\r\n    categoryFiltration.innerHTML = 'category:';\r\n    categorySelect.innerHTML = content;\r\n\r\n    categoryFiltration.append(categorySelect);\r\n\r\n    const categories = new _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_2__[\"ServerRequests\"]().getCategories();\r\n\r\n    Object(_modal_add_task_form_modal_body_category_categoryRender__WEBPACK_IMPORTED_MODULE_1__[\"categoryRender\"])(categories, categorySelect);\r\n\r\n  \r\n    categorySelect.addEventListener('change', filterByCategory);\r\n\r\n    function filterByCategory() {\r\n        const trTableRows = document.querySelectorAll('.tbody tr');\r\n\r\n        trTableRows.forEach(tr => {\r\n            if(categorySelect.value == 'all')  {\r\n                tr.style.display = 'table-row';\r\n            } else if (tr.childNodes[3].textContent !== categorySelect.value) {\r\n                tr.style.display = 'none';\r\n            } else {\r\n                tr.style.display = 'table-row';\r\n            }      \r\n        })\r\n    }\r\n\r\n    return categoryFiltration;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/category-filtration/category-filtration.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/category-filtration/index.js":
/*!********************************************************************!*\
  !*** ./src/app/main/tasks-filtration/category-filtration/index.js ***!
  \********************************************************************/
/*! exports provided: CategoryFiltration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _category_filtration_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./category-filtration.js */ \"./src/app/main/tasks-filtration/category-filtration/category-filtration.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"CategoryFiltration\", function() { return _category_filtration_js__WEBPACK_IMPORTED_MODULE_0__[\"CategoryFiltration\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/category-filtration/index.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/date-filtration/date-filtration.css":
/*!***************************************************************************!*\
  !*** ./src/app/main/tasks-filtration/date-filtration/date-filtration.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./date-filtration.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/date-filtration/date-filtration.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/date-filtration/date-filtration.css?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/date-filtration/date-filtration.js":
/*!**************************************************************************!*\
  !*** ./src/app/main/tasks-filtration/date-filtration/date-filtration.js ***!
  \**************************************************************************/
/*! exports provided: DateFiltration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"DateFiltration\", function() { return DateFiltration; });\n/* harmony import */ var _date_filtration_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./date-filtration.css */ \"./src/app/main/tasks-filtration/date-filtration/date-filtration.css\");\n/* harmony import */ var _date_filtration_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_date_filtration_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction DateFiltration() {\r\n    const dateFiltration = document.createElement('label');\r\n    const dateFilter = document.createElement('input');\r\n\r\n    dateFiltration.classList.add('date-filtration');\r\n    dateFiltration.innerHTML = 'date:';\r\n\r\n    dateFilter.classList.add('form-control', 'date-filter');\r\n    dateFiltration.append(dateFilter);\r\n    \r\n\r\n    $(function() {\r\n        $(dateFilter).datepicker({\r\n        dateFormat: 'yy-m-d',\r\n        dayNamesMin : [ \"S\", \"M\", \"T\", \"W\", \"T\", \"F\", \"S\" ],\r\n        onSelect: filterByDate     \r\n        })\r\n\r\n    })\r\n\r\n\r\n    function filterByDate(date) {\r\n        const rows = document.querySelectorAll('.all-tasks tbody tr');\r\n\r\n        rows.forEach(row => {\r\n            if (row.childNodes[1].textContent !== date) {\r\n                row.style.display = 'none';\r\n            } else {\r\n                row.style.display = 'table-row';\r\n            }      \r\n        })\r\n    } \r\n\r\n    return dateFiltration\r\n}\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/date-filtration/date-filtration.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/date-filtration/index.js":
/*!****************************************************************!*\
  !*** ./src/app/main/tasks-filtration/date-filtration/index.js ***!
  \****************************************************************/
/*! exports provided: DateFiltration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _date_filtration_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./date-filtration.js */ \"./src/app/main/tasks-filtration/date-filtration/date-filtration.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"DateFiltration\", function() { return _date_filtration_js__WEBPACK_IMPORTED_MODULE_0__[\"DateFiltration\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/date-filtration/index.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/index.js":
/*!************************************************!*\
  !*** ./src/app/main/tasks-filtration/index.js ***!
  \************************************************/
/*! exports provided: TasksFiltration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _tasks_filtration_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tasks-filtration.js */ \"./src/app/main/tasks-filtration/tasks-filtration.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"TasksFiltration\", function() { return _tasks_filtration_js__WEBPACK_IMPORTED_MODULE_0__[\"TasksFiltration\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/index.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/priority-filtration/index.js":
/*!********************************************************************!*\
  !*** ./src/app/main/tasks-filtration/priority-filtration/index.js ***!
  \********************************************************************/
/*! exports provided: PriorityFiltration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _priority_filtration_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./priority-filtration.js */ \"./src/app/main/tasks-filtration/priority-filtration/priority-filtration.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"PriorityFiltration\", function() { return _priority_filtration_js__WEBPACK_IMPORTED_MODULE_0__[\"PriorityFiltration\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/priority-filtration/index.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/priority-filtration/priority-filtration.css":
/*!***********************************************************************************!*\
  !*** ./src/app/main/tasks-filtration/priority-filtration/priority-filtration.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./priority-filtration.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/priority-filtration/priority-filtration.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/priority-filtration/priority-filtration.css?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/priority-filtration/priority-filtration.js":
/*!**********************************************************************************!*\
  !*** ./src/app/main/tasks-filtration/priority-filtration/priority-filtration.js ***!
  \**********************************************************************************/
/*! exports provided: PriorityFiltration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"PriorityFiltration\", function() { return PriorityFiltration; });\n/* harmony import */ var _priority_filtration_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./priority-filtration.css */ \"./src/app/main/tasks-filtration/priority-filtration/priority-filtration.css\");\n/* harmony import */ var _priority_filtration_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_priority_filtration_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction PriorityFiltration() {\r\n    const priorityFiltration = document.createElement('label');\r\n    const priorityFilter = document.createElement('select');\r\n    const content = `\r\n        <option value=\"all\">all</option>\r\n        <option value=\"low\">low</option>\r\n        <option value=\"middle\">middle</option>\r\n        <option value=\"important\">important</option>\r\n    `;\r\n\r\n    priorityFiltration.classList.add('priority-filtration');\r\n    priorityFilter.classList.add('form-control', 'priority-filter');\r\n\r\n    priorityFiltration.innerHTML = 'priority:';\r\n    priorityFilter.innerHTML = content;\r\n\r\n    priorityFiltration.append(priorityFilter);\r\n\r\n\r\n    priorityFilter.addEventListener('change', filterByPriority);\r\n\r\n    function filterByPriority() {\r\n        const trTableRows = document.querySelectorAll('tbody tr');\r\n\r\n        trTableRows.forEach(tr => {\r\n            if(priorityFilter.value === 'all')  {\r\n                tr.style.display = 'table-row';\r\n            } else if (tr.childNodes[5].textContent !== priorityFilter.value) {\r\n                tr.style.display = 'none';\r\n            } else {\r\n                tr.style.display = 'table-row';\r\n            }     \r\n        })\r\n    }\r\n\r\n\r\n    return priorityFiltration;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/priority-filtration/priority-filtration.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/progress-filtration/index.js":
/*!********************************************************************!*\
  !*** ./src/app/main/tasks-filtration/progress-filtration/index.js ***!
  \********************************************************************/
/*! exports provided: ProgressFiltration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _progress_filtration_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./progress-filtration.js */ \"./src/app/main/tasks-filtration/progress-filtration/progress-filtration.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"ProgressFiltration\", function() { return _progress_filtration_js__WEBPACK_IMPORTED_MODULE_0__[\"ProgressFiltration\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/progress-filtration/index.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/progress-filtration/progress-filtration.css":
/*!***********************************************************************************!*\
  !*** ./src/app/main/tasks-filtration/progress-filtration/progress-filtration.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./progress-filtration.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/progress-filtration/progress-filtration.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/progress-filtration/progress-filtration.css?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/progress-filtration/progress-filtration.js":
/*!**********************************************************************************!*\
  !*** ./src/app/main/tasks-filtration/progress-filtration/progress-filtration.js ***!
  \**********************************************************************************/
/*! exports provided: ProgressFiltration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ProgressFiltration\", function() { return ProgressFiltration; });\n/* harmony import */ var _progress_filtration_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./progress-filtration.css */ \"./src/app/main/tasks-filtration/progress-filtration/progress-filtration.css\");\n/* harmony import */ var _progress_filtration_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_progress_filtration_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction ProgressFiltration() {\r\n    const progressFiltration = document.createElement('label');\r\n    const progressFilter = document.createElement('select');\r\n    const content = `\r\n        <option value=\"all\">all</option>\r\n        <option value=\"all-not-completed\">all not completed</option>\r\n        <option value=\"0%\">0%</option>\r\n        <option value=\"25%\">25%</option>\r\n        <option value=\"50%\">50%</option>\r\n        <option value=\"75%\">75%</option>\r\n        <option value=\"100%\">100%</option>\r\n    `;\r\n\r\n    progressFiltration.classList.add('progress-filtration');\r\n    progressFilter.classList.add('form-control', 'progress-filter');\r\n\r\n    progressFiltration.innerHTML = 'progress:';\r\n    progressFilter.innerHTML = content;\r\n\r\n    progressFiltration.append(progressFilter);\r\n\r\n    \r\n    progressFilter.addEventListener('change', filterByProgress);\r\n\r\n    function filterByProgress() {\r\n        const trTableRows = document.querySelectorAll('.tbody tr');\r\n\r\n        trTableRows.forEach(tr => {\r\n            if(progressFilter.value === 'all')  {\r\n                tr.style.display = 'table-row';\r\n            }  else if(progressFilter.value === 'all-not-completed' && tr.childNodes[4].textContent !== '100%') {\r\n                tr.style.display = 'table-row';\r\n            } else if (tr.childNodes[4].textContent !== progressFilter.value) {\r\n                tr.style.display = 'none';\r\n            } else {\r\n                tr.style.display = 'table-row';\r\n            }     \r\n        })\r\n    }\r\n\r\n    return progressFiltration;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/progress-filtration/progress-filtration.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/show-tasks/show-tasks.css":
/*!*****************************************************************!*\
  !*** ./src/app/main/tasks-filtration/show-tasks/show-tasks.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./show-tasks.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/show-tasks/show-tasks.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/show-tasks/show-tasks.css?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/show-tasks/show-tasks.js":
/*!****************************************************************!*\
  !*** ./src/app/main/tasks-filtration/show-tasks/show-tasks.js ***!
  \****************************************************************/
/*! exports provided: ShowTasks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ShowTasks\", function() { return ShowTasks; });\n/* harmony import */ var _show_tasks_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./show-tasks.css */ \"./src/app/main/tasks-filtration/show-tasks/show-tasks.css\");\n/* harmony import */ var _show_tasks_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_show_tasks_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction ShowTasks() {\r\n    const showTasks = document.createElement('button');\r\n    const content = `\r\n        <i class=\"fa fa-th-list\" aria-hidden=\"true\"></i>\r\n        all tasks`;\r\n    \r\n\r\n    showTasks.classList.add('btn', 'btn-success', 'show-tasks');\r\n    showTasks.setAttribute('type', 'button');\r\n    showTasks.innerHTML = content;\r\n\r\n    \r\n    showTasks.addEventListener('click', showAllTasks);\r\n    function showAllTasks() {\r\n        const dateFilter = document.querySelector('.date-filter');\r\n        const timeFilter = document.querySelector('.select-time');\r\n        const categoryFilter = document.querySelector('.category-filter');\r\n        const progressFilter = document.querySelector('.progress-filter');\r\n        const priorityFilter = document.querySelector('.priority-filter');\r\n        const trTableRows = document.querySelectorAll('.tbody tr');\r\n\r\n        trTableRows.forEach(tr => {\r\n            tr.style.display = 'table-row';\r\n            dateFilter.value = null;\r\n            timeFilter.value = null;\r\n            categoryFilter.value = null;\r\n            progressFilter.value = null;\r\n            priorityFilter.value = null;\r\n        })\r\n    }\r\n\r\n    return showTasks;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/show-tasks/show-tasks.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/tasks-filtration.css":
/*!************************************************************!*\
  !*** ./src/app/main/tasks-filtration/tasks-filtration.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js!./tasks-filtration.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/tasks-filtration.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/tasks-filtration.css?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/tasks-filtration.js":
/*!***********************************************************!*\
  !*** ./src/app/main/tasks-filtration/tasks-filtration.js ***!
  \***********************************************************/
/*! exports provided: TasksFiltration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"TasksFiltration\", function() { return TasksFiltration; });\n/* harmony import */ var _tasks_filtration_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tasks-filtration.css */ \"./src/app/main/tasks-filtration/tasks-filtration.css\");\n/* harmony import */ var _tasks_filtration_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_tasks_filtration_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _show_tasks_show_tasks__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./show-tasks/show-tasks */ \"./src/app/main/tasks-filtration/show-tasks/show-tasks.js\");\n/* harmony import */ var _time_filtration__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./time-filtration */ \"./src/app/main/tasks-filtration/time-filtration/index.js\");\n/* harmony import */ var _date_filtration__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./date-filtration */ \"./src/app/main/tasks-filtration/date-filtration/index.js\");\n/* harmony import */ var _category_filtration__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./category-filtration */ \"./src/app/main/tasks-filtration/category-filtration/index.js\");\n/* harmony import */ var _progress_filtration__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./progress-filtration */ \"./src/app/main/tasks-filtration/progress-filtration/index.js\");\n/* harmony import */ var _priority_filtration__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./priority-filtration */ \"./src/app/main/tasks-filtration/priority-filtration/index.js\");\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nfunction TasksFiltration() {\r\n    const tasksFiltration = document.createElement('div');\r\n\r\n    tasksFiltration.classList.add('tasks-filtration', 'd-flex', 'justify-content-around', 'align-items-center', 'flex-wrap', 'bg-secondary', 'text-white');\r\n\r\n    tasksFiltration.append(Object(_show_tasks_show_tasks__WEBPACK_IMPORTED_MODULE_1__[\"ShowTasks\"])(), Object(_time_filtration__WEBPACK_IMPORTED_MODULE_2__[\"TimeFiltration\"])(), Object(_date_filtration__WEBPACK_IMPORTED_MODULE_3__[\"DateFiltration\"])(), Object(_category_filtration__WEBPACK_IMPORTED_MODULE_4__[\"CategoryFiltration\"])(), Object(_progress_filtration__WEBPACK_IMPORTED_MODULE_5__[\"ProgressFiltration\"])(), Object(_priority_filtration__WEBPACK_IMPORTED_MODULE_6__[\"PriorityFiltration\"])());\r\n    \r\n    return tasksFiltration;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/tasks-filtration.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/time-filtration/index.js":
/*!****************************************************************!*\
  !*** ./src/app/main/tasks-filtration/time-filtration/index.js ***!
  \****************************************************************/
/*! exports provided: TimeFiltration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _time_filtration_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./time-filtration.js */ \"./src/app/main/tasks-filtration/time-filtration/time-filtration.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"TimeFiltration\", function() { return _time_filtration_js__WEBPACK_IMPORTED_MODULE_0__[\"TimeFiltration\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/time-filtration/index.js?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/time-filtration/time-filtration.css":
/*!***************************************************************************!*\
  !*** ./src/app/main/tasks-filtration/time-filtration/time-filtration.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./time-filtration.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/main/tasks-filtration/time-filtration/time-filtration.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/time-filtration/time-filtration.css?");

/***/ }),

/***/ "./src/app/main/tasks-filtration/time-filtration/time-filtration.js":
/*!**************************************************************************!*\
  !*** ./src/app/main/tasks-filtration/time-filtration/time-filtration.js ***!
  \**************************************************************************/
/*! exports provided: TimeFiltration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"TimeFiltration\", function() { return TimeFiltration; });\n/* harmony import */ var _time_filtration_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./time-filtration.css */ \"./src/app/main/tasks-filtration/time-filtration/time-filtration.css\");\n/* harmony import */ var _time_filtration_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_time_filtration_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction TimeFiltration() {\r\n    const timeFiltration = document.createElement('label');\r\n    const selectTime = document.createElement('select');\r\n    const content = `\r\n    <option value=\"all\">all</option>\r\n    <option value=\"today\">today</option>\r\n    <option value=\"week\">week</option>\r\n    <option value=\"month\">month</option>\r\n    <option value=\"year\">year</option>\r\n    `;\r\n    \r\n\r\n    timeFiltration.classList.add('time-filtration');\r\n    selectTime.classList.add('form-control', 'select-time');\r\n\r\n    timeFiltration.innerHTML = 'time:';\r\n    selectTime.innerHTML = content;\r\n\r\n    timeFiltration.append(selectTime);\r\n\r\n    \r\n    selectTime.addEventListener('change', filterByTime);\r\n\r\n    function filterByTime() {\r\n        const trTableRows = document.querySelectorAll('.tbody tr');\r\n\r\n        trTableRows.forEach(tr => {\r\n            if(selectTime.value === 'all')  {\r\n                tr.style.display = 'table-row';\r\n            } else if (selectTime.value === 'today' && tr.childNodes[1].textContent !== `${new Date().getFullYear()}-${new Date().getMonth()+1}-${new Date().getDate()}`) {\r\n                tr.style.display = 'none';\r\n            } else if (selectTime.value === 'week' && Date.parse(tr.childNodes[1].textContent) >= (Date.parse(new Date())+ 1000*60*60*24*7) ) {\r\n                tr.style.display = 'none';\r\n            } else if (selectTime.value === 'month' && Date.parse(tr.childNodes[1].textContent) >= (Date.parse(`${new Date().getFullYear()}-${new Date().getMonth() + 2}-${new Date().getDate()}`))) {\r\n                tr.style.display = 'none';\r\n            } else if (selectTime.value === 'year' && Date.parse(tr.childNodes[1].textContent) >= (Date.parse(`${new Date().getFullYear() + 1}-${new Date().getMonth()}-${new Date().getDate()}`))) {\r\n                tr.style.display = 'none';\r\n            } else {\r\n                tr.style.display = 'table-row';\r\n            }    \r\n        })\r\n    }\r\n\r\n    return timeFiltration;\r\n}\n\n//# sourceURL=webpack:///./src/app/main/tasks-filtration/time-filtration/time-filtration.js?");

/***/ }),

/***/ "./src/app/register-modal/index.js":
/*!*****************************************!*\
  !*** ./src/app/register-modal/index.js ***!
  \*****************************************/
/*! exports provided: RegisterModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _register_modal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./register-modal */ \"./src/app/register-modal/register-modal.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"RegisterModal\", function() { return _register_modal__WEBPACK_IMPORTED_MODULE_0__[\"RegisterModal\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/register-modal/index.js?");

/***/ }),

/***/ "./src/app/register-modal/modal-content/index.js":
/*!*******************************************************!*\
  !*** ./src/app/register-modal/modal-content/index.js ***!
  \*******************************************************/
/*! exports provided: ModalContent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modal_content__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-content */ \"./src/app/register-modal/modal-content/modal-content.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"ModalContent\", function() { return _modal_content__WEBPACK_IMPORTED_MODULE_0__[\"ModalContent\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/index.js?");

/***/ }),

/***/ "./src/app/register-modal/modal-content/modal-body/index.js":
/*!******************************************************************!*\
  !*** ./src/app/register-modal/modal-content/modal-body/index.js ***!
  \******************************************************************/
/*! exports provided: ModalBody */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modal_body__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-body */ \"./src/app/register-modal/modal-content/modal-body/modal-body.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"ModalBody\", function() { return _modal_body__WEBPACK_IMPORTED_MODULE_0__[\"ModalBody\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-body/index.js?");

/***/ }),

/***/ "./src/app/register-modal/modal-content/modal-body/login-label/index.js":
/*!******************************************************************************!*\
  !*** ./src/app/register-modal/modal-content/modal-body/login-label/index.js ***!
  \******************************************************************************/
/*! exports provided: LoginLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _login_label__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login-label */ \"./src/app/register-modal/modal-content/modal-body/login-label/login-label.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"LoginLabel\", function() { return _login_label__WEBPACK_IMPORTED_MODULE_0__[\"LoginLabel\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-body/login-label/index.js?");

/***/ }),

/***/ "./src/app/register-modal/modal-content/modal-body/login-label/login-label.css":
/*!*************************************************************************************!*\
  !*** ./src/app/register-modal/modal-content/modal-body/login-label/login-label.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./login-label.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/modal-content/modal-body/login-label/login-label.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-body/login-label/login-label.css?");

/***/ }),

/***/ "./src/app/register-modal/modal-content/modal-body/login-label/login-label.js":
/*!************************************************************************************!*\
  !*** ./src/app/register-modal/modal-content/modal-body/login-label/login-label.js ***!
  \************************************************************************************/
/*! exports provided: LoginLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"LoginLabel\", function() { return LoginLabel; });\n/* harmony import */ var _login_label_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login-label.css */ \"./src/app/register-modal/modal-content/modal-body/login-label/login-label.css\");\n/* harmony import */ var _login_label_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_login_label_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction LoginLabel() {\r\n    const loginLabel = document.createElement('label');\r\n    const title = 'login:';\r\n    const loginInput = document.createElement('input');\r\n    const errorLoginText = document.createElement('p');\r\n\r\n    loginLabel.classList.add('login-label', 'w-100');\r\n    loginInput.classList.add('form-control', 'register-login');\r\n    errorLoginText.classList.add('error-login-text', 'text-danger');\r\n\r\n    loginInput.setAttribute('type', 'text');\r\n    loginInput.setAttribute('required', 'required');\r\n\r\n    \r\n    loginLabel.append(title, loginInput, errorLoginText);\r\n\r\n    return loginLabel;\r\n}\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-body/login-label/login-label.js?");

/***/ }),

/***/ "./src/app/register-modal/modal-content/modal-body/modal-body.css":
/*!************************************************************************!*\
  !*** ./src/app/register-modal/modal-content/modal-body/modal-body.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./modal-body.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/modal-content/modal-body/modal-body.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-body/modal-body.css?");

/***/ }),

/***/ "./src/app/register-modal/modal-content/modal-body/modal-body.js":
/*!***********************************************************************!*\
  !*** ./src/app/register-modal/modal-content/modal-body/modal-body.js ***!
  \***********************************************************************/
/*! exports provided: ModalBody */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ModalBody\", function() { return ModalBody; });\n/* harmony import */ var _modal_body_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-body.css */ \"./src/app/register-modal/modal-content/modal-body/modal-body.css\");\n/* harmony import */ var _modal_body_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_body_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _login_label__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login-label */ \"./src/app/register-modal/modal-content/modal-body/login-label/index.js\");\n/* harmony import */ var _password_label__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./password-label */ \"./src/app/register-modal/modal-content/modal-body/password-label/index.js\");\n/* harmony import */ var _register_button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./register-button */ \"./src/app/register-modal/modal-content/modal-body/register-button/index.js\");\n\r\n\r\n\r\n\r\n\r\nfunction ModalBody() {\r\n    const modalBody = document.createElement('div');\r\n\r\n    modalBody.classList.add('form-group', 'modal-body', 'd-flex', 'flex-wrap');\r\n\r\n    modalBody.append(Object(_login_label__WEBPACK_IMPORTED_MODULE_1__[\"LoginLabel\"])(), Object(_password_label__WEBPACK_IMPORTED_MODULE_2__[\"PasswordLabel\"])(), Object(_register_button__WEBPACK_IMPORTED_MODULE_3__[\"RegisterButton\"])());\r\n\r\n    return modalBody;\r\n}\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-body/modal-body.js?");

/***/ }),

/***/ "./src/app/register-modal/modal-content/modal-body/password-label/index.js":
/*!*********************************************************************************!*\
  !*** ./src/app/register-modal/modal-content/modal-body/password-label/index.js ***!
  \*********************************************************************************/
/*! exports provided: PasswordLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _password_label__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./password-label */ \"./src/app/register-modal/modal-content/modal-body/password-label/password-label.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"PasswordLabel\", function() { return _password_label__WEBPACK_IMPORTED_MODULE_0__[\"PasswordLabel\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-body/password-label/index.js?");

/***/ }),

/***/ "./src/app/register-modal/modal-content/modal-body/password-label/password-label.css":
/*!*******************************************************************************************!*\
  !*** ./src/app/register-modal/modal-content/modal-body/password-label/password-label.css ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./password-label.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/modal-content/modal-body/password-label/password-label.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-body/password-label/password-label.css?");

/***/ }),

/***/ "./src/app/register-modal/modal-content/modal-body/password-label/password-label.js":
/*!******************************************************************************************!*\
  !*** ./src/app/register-modal/modal-content/modal-body/password-label/password-label.js ***!
  \******************************************************************************************/
/*! exports provided: PasswordLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"PasswordLabel\", function() { return PasswordLabel; });\n/* harmony import */ var _password_label_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./password-label.css */ \"./src/app/register-modal/modal-content/modal-body/password-label/password-label.css\");\n/* harmony import */ var _password_label_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_password_label_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction PasswordLabel() {\r\n    const passwordLabel = document.createElement('label');\r\n    const title = 'password:';\r\n    const passwordInput = document.createElement('input');\r\n\r\n    passwordLabel.classList.add('password-label', 'w-100');\r\n    passwordInput.classList.add('form-control', 'register-password');\r\n\r\n    passwordInput.setAttribute('type', 'password');\r\n    passwordInput.setAttribute('required', 'required');\r\n\r\n    passwordLabel.append(title, passwordInput);\r\n\r\n    return passwordLabel;\r\n}\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-body/password-label/password-label.js?");

/***/ }),

/***/ "./src/app/register-modal/modal-content/modal-body/register-button/index.js":
/*!**********************************************************************************!*\
  !*** ./src/app/register-modal/modal-content/modal-body/register-button/index.js ***!
  \**********************************************************************************/
/*! exports provided: RegisterButton */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _register_button__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./register-button */ \"./src/app/register-modal/modal-content/modal-body/register-button/register-button.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"RegisterButton\", function() { return _register_button__WEBPACK_IMPORTED_MODULE_0__[\"RegisterButton\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-body/register-button/index.js?");

/***/ }),

/***/ "./src/app/register-modal/modal-content/modal-body/register-button/register-button.css":
/*!*********************************************************************************************!*\
  !*** ./src/app/register-modal/modal-content/modal-body/register-button/register-button.css ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!./register-button.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/modal-content/modal-body/register-button/register-button.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-body/register-button/register-button.css?");

/***/ }),

/***/ "./src/app/register-modal/modal-content/modal-body/register-button/register-button.js":
/*!********************************************************************************************!*\
  !*** ./src/app/register-modal/modal-content/modal-body/register-button/register-button.js ***!
  \********************************************************************************************/
/*! exports provided: RegisterButton */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"RegisterButton\", function() { return RegisterButton; });\n/* harmony import */ var _register_button_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./register-button.css */ \"./src/app/register-modal/modal-content/modal-body/register-button/register-button.css\");\n/* harmony import */ var _register_button_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_register_button_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction RegisterButton() {\r\n    const registerButton = document.createElement('button');\r\n    const content = `\r\n        register\r\n    `;\r\n\r\n    registerButton.classList.add('btn', 'btn-primary', 'register-button', 'w-100');\r\n\r\n    registerButton.setAttribute('type', 'submit');\r\n\r\n    registerButton.innerHTML = content;\r\n\r\n    return registerButton;\r\n}\r\n\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-body/register-button/register-button.js?");

/***/ }),

/***/ "./src/app/register-modal/modal-content/modal-content.css":
/*!****************************************************************!*\
  !*** ./src/app/register-modal/modal-content/modal-content.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js!./modal-content.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/modal-content/modal-content.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-content.css?");

/***/ }),

/***/ "./src/app/register-modal/modal-content/modal-content.js":
/*!***************************************************************!*\
  !*** ./src/app/register-modal/modal-content/modal-content.js ***!
  \***************************************************************/
/*! exports provided: ModalContent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ModalContent\", function() { return ModalContent; });\n/* harmony import */ var _modal_content_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-content.css */ \"./src/app/register-modal/modal-content/modal-content.css\");\n/* harmony import */ var _modal_content_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_content_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _modal_header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modal-header */ \"./src/app/register-modal/modal-content/modal-header/index.js\");\n/* harmony import */ var _modal_body__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modal-body */ \"./src/app/register-modal/modal-content/modal-body/index.js\");\n/* harmony import */ var _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../server-requests/server-requests */ \"./src/app/server-requests/server-requests.js\");\n\r\n\r\n\r\n\r\n\r\nfunction ModalContent() {\r\n    const modalContent = document.createElement('form');\r\n\r\n    modalContent.classList.add('register-form', 'modal-content');\r\n\r\n    modalContent.append(Object(_modal_header__WEBPACK_IMPORTED_MODULE_1__[\"ModalHeader\"])(), Object(_modal_body__WEBPACK_IMPORTED_MODULE_2__[\"ModalBody\"])());\r\n\r\n    modalContent.addEventListener('submit', registerNewUser);\r\n\r\n    function registerNewUser(ev) {\r\n        ev.preventDefault();\r\n\r\n        const login = document.querySelector('.register-login').value;\r\n        const password = document.querySelector('.register-password').value;\r\n        const user = {\r\n            login,\r\n            password\r\n        };\r\n\r\n        const getUsers = new _server_requests_server_requests__WEBPACK_IMPORTED_MODULE_3__[\"ServerRequests\"]().getUsers();\r\n        \r\n\r\n        fetch('http://localhost:2000/users/', {\r\n            method: 'POST',\r\n            headers: {\r\n                'Content-Type': 'application/json'\r\n            },\r\n            body: JSON.stringify(user)\r\n        })\r\n        .then(res => res.json())\r\n        .then(user => {\r\n            getUsers\r\n            .then(users => {\r\n                const findUsers = users.find(us => {\r\n                    return us.login === user.login;\r\n                });\r\n\r\n                if(findUsers === undefined) {\r\n                    alert(`user ${user.login} is registered`);\r\n                    const registerModal = document.querySelector('.register-modal');\r\n                    registerModal.style.display = 'none';\r\n                } else {\r\n                    const errorLoginText = document.querySelector('.error-login-text');\r\n                    const loginInput = document.querySelector('.register-login');\r\n\r\n                    errorLoginText.innerHTML = 'This username is already taken. Enter a different username';\r\n                    \r\n                    loginInput.addEventListener('focus', delErrorText);\r\n\r\n                    function delErrorText() {\r\n                        errorLoginText.innerHTML = null;\r\n                    }\r\n                }\r\n            })\r\n        })\r\n    }\r\n\r\n    return modalContent;\r\n}\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-content.js?");

/***/ }),

/***/ "./src/app/register-modal/modal-content/modal-header/index.js":
/*!********************************************************************!*\
  !*** ./src/app/register-modal/modal-content/modal-header/index.js ***!
  \********************************************************************/
/*! exports provided: ModalHeader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modal_header__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-header */ \"./src/app/register-modal/modal-content/modal-header/modal-header.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"ModalHeader\", function() { return _modal_header__WEBPACK_IMPORTED_MODULE_0__[\"ModalHeader\"]; });\n\n\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-header/index.js?");

/***/ }),

/***/ "./src/app/register-modal/modal-content/modal-header/modal-header.css":
/*!****************************************************************************!*\
  !*** ./src/app/register-modal/modal-content/modal-header/modal-header.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js!./modal-header.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/modal-content/modal-header/modal-header.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-header/modal-header.css?");

/***/ }),

/***/ "./src/app/register-modal/modal-content/modal-header/modal-header.js":
/*!***************************************************************************!*\
  !*** ./src/app/register-modal/modal-content/modal-header/modal-header.js ***!
  \***************************************************************************/
/*! exports provided: ModalHeader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ModalHeader\", function() { return ModalHeader; });\n/* harmony import */ var _modal_header_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-header.css */ \"./src/app/register-modal/modal-content/modal-header/modal-header.css\");\n/* harmony import */ var _modal_header_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modal_header_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\n\r\nfunction ModalHeader() {\r\n    const modalHeader = document.createElement('div');\r\n    const title = document.createElement('h2');\r\n    const close = document.createElement('span');\r\n\r\n    modalHeader.classList.add('modal-header', 'bg-info');\r\n    close.classList.add('close');\r\n\r\n    close.innerHTML = `&times;`;\r\n    title.innerHTML = 'Register';\r\n\r\n    close.addEventListener('click', closeRegisterModal);\r\n\r\n    function closeRegisterModal() {\r\n        const registerModal = document.querySelector('.register-modal');\r\n        registerModal.style.display = 'none';\r\n    }\r\n\r\n    modalHeader.append(title, close);\r\n\r\n    return modalHeader;\r\n}\n\n//# sourceURL=webpack:///./src/app/register-modal/modal-content/modal-header/modal-header.js?");

/***/ }),

/***/ "./src/app/register-modal/register-modal.css":
/*!***************************************************!*\
  !*** ./src/app/register-modal/register-modal.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js!./register-modal.css */ \"./node_modules/css-loader/dist/cjs.js!./src/app/register-modal/register-modal.css\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/app/register-modal/register-modal.css?");

/***/ }),

/***/ "./src/app/register-modal/register-modal.js":
/*!**************************************************!*\
  !*** ./src/app/register-modal/register-modal.js ***!
  \**************************************************/
/*! exports provided: RegisterModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"RegisterModal\", function() { return RegisterModal; });\n/* harmony import */ var _register_modal_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./register-modal.css */ \"./src/app/register-modal/register-modal.css\");\n/* harmony import */ var _register_modal_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_register_modal_css__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _modal_content__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modal-content */ \"./src/app/register-modal/modal-content/index.js\");\n\r\n\r\n\r\nfunction RegisterModal() {\r\n    const registerModal = document.createElement('div');\r\n\r\n    registerModal.classList.add('register-modal');\r\n\r\n\r\n    registerModal.append(Object(_modal_content__WEBPACK_IMPORTED_MODULE_1__[\"ModalContent\"])());\r\n\r\n    return registerModal;\r\n}\n\n//# sourceURL=webpack:///./src/app/register-modal/register-modal.js?");

/***/ }),

/***/ "./src/app/server-requests/server-requests.js":
/*!****************************************************!*\
  !*** ./src/app/server-requests/server-requests.js ***!
  \****************************************************/
/*! exports provided: ServerRequests */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ServerRequests\", function() { return ServerRequests; });\nclass ServerRequests {\r\n    getTasks() {\r\n        return fetch(`http://localhost:2000/tasks`)\r\n            .then(res => res.json())\r\n            \r\n    }\r\n    \r\n    getCategories() {\r\n        return fetch('http://localhost:2000/categories')\r\n            .then(res => res.json())\r\n    }\r\n    \r\n    getTask(id) {\r\n        return fetch(`http://localhost:2000/tasks/${id}`)\r\n            .then(res => res.json())\r\n    }\r\n    \r\n    getCategories() {\r\n        return fetch('http://localhost:2000/categories')\r\n            .then(res => res.json())\r\n    }\r\n    \r\n    getCategory(id) {\r\n        return fetch(`http://localhost:2000/categories/${id}`)\r\n            .then(res => res.json())\r\n    }\r\n\r\n    getUsers() {\r\n        return fetch('http://localhost:2000/users')\r\n            .then(res => res.json());\r\n    }\r\n\r\n    getUser(id) {\r\n        return fetch(`http://localhost:2000/users/${id}`)\r\n            .then(res => res.json())\r\n    }\r\n\r\n    getUserTasks(id) {\r\n        return fetch(`http://localhost:2000/userTasks/${id}`)\r\n            .then(res => res.json())\r\n    }\r\n}\r\n\r\n\n\n//# sourceURL=webpack:///./src/app/server-requests/server-requests.js?");

/***/ }),

/***/ "./src/app/task helpers/changeDoneTitle.js":
/*!*************************************************!*\
  !*** ./src/app/task helpers/changeDoneTitle.js ***!
  \*************************************************/
/*! exports provided: changeDoneTitle */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"changeDoneTitle\", function() { return changeDoneTitle; });\n\r\nfunction changeDoneTitle(task) {\r\n    // const tdTitle = document.querySelector(`.tbody tr[data-id = \"${task.id}\"] td:first-child`);\r\n    const tdProgress = document.querySelector(`.tbody tr[data-id = \"${task.id}\"] td:nth-child(5)`);\r\n\r\n\r\n    const tr = tdProgress.parentElement;\r\n    if(tdProgress.textContent === '100%') {\r\n        // tdTitle.style.textDecoration = 'line-through';\r\n        \r\n        tr.style.textDecoration = 'line-through';\r\n        tr.classList.add('text-black-50');\r\n\r\n    } else {\r\n        // tdTitle.style.textDecoration = 'none';\r\n        tr.style.textDecoration = 'none';\r\n        tr.classList.remove('text-black-50');\r\n    }\r\n}\r\n    \n\n//# sourceURL=webpack:///./src/app/task_helpers/changeDoneTitle.js?");

/***/ }),

/***/ "./src/app/task helpers/changeStatusTitle.js":
/*!***************************************************!*\
  !*** ./src/app/task helpers/changeStatusTitle.js ***!
  \***************************************************/
/*! exports provided: changeStatusTitle */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"changeStatusTitle\", function() { return changeStatusTitle; });\nfunction changeStatusTitle(task) {\r\n    const tdStatus = document.querySelector(`.tbody tr[data-id = \"${task.id}\"] td:last-child`);\r\n\r\n    if(tdStatus.textContent === 'low') {\r\n        tdStatus.style.color = '#28a745';\r\n    } else if(tdStatus.textContent === 'middle') {\r\n        tdStatus.style.color = '#ffc107';\r\n    } else if(tdStatus.textContent === 'important'){\r\n        tdStatus.style.color = '#dc3545';\r\n    }\r\n}\n\n//# sourceURL=webpack:///./src/app/task_helpers/changeStatusTitle.js?");

/***/ }),

/***/ "./src/images/logo.png":
/*!*****************************!*\
  !*** ./src/images/logo.png ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (__webpack_require__.p + \"663e4121c480e0411b5c31acac836799.png\");\n\n//# sourceURL=webpack:///./src/images/logo.png?");

/***/ })

/******/ });