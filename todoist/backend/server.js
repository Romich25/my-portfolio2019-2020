const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const database = require('./db.json');
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

app.get('/', function(req, res) {
    res.send('Hello!');
})

app.get('/tasks', function(req, res) {
    +req.query.userId;
    console.log(req.query.userId);
    res.send(database.tasks);
})

app.get('/tasks/:id', function(req, res) {
    const id = req.params.id;
    const task = database.tasks.find((task) => task.id === +id);
    res.json(task);
}); 


app.get('/tasks', (req, res) => {
    console.log(req.query);
    if(req.query.status) {
        return res.json(database.tasks.filter(task => task.status === req.query.status))
    }
})

app.post('/tasks', function(req, res) {
    const task = {
        ...req.body,
        date: new Date().toLocaleDateString(),
        id: Date.now(),
        progress: 0
    }
    console.log(task);
    database.tasks.push(task);
    
    res.status(201).json(task.id);
    
   
});

app.put('/tasks/:id', function(req, res) {
    var task = database.tasks.find(function(task) {
        return task.id === +req.params.id;
    });
    task.title = req.body.title;
    task.status = req.body.status;
    task.deadlineDate = req.body.deadlineDate;
    task.category = req.body.category;
    task.time = req.body.time;
    task.priority = req.body.priority;
    task.progress = req.body.progress;
    task.comment = req.body.comment;
    res.status(200).json(+req.params.id);
});


app.delete('/tasks/:id', function(req, res) {
    database.tasks = database.tasks.filter(function(task) {
        return task.id !== +req.params.id;
    });
    res.status(200).json(+req.params.id);
});

app.listen(2000, function () {
    console.log('API started');
})




app.get('/categories', function(req, res) {
    res.send(database.categories);
})

app.post('/categories', function(req, res) {
    const category = {
        ...req.body,
        id: Date.now()
    }

    console.log(category);

    database.categories.push(category);  
    res.status(200).json(category);
    
});

app.get('/categories/:id', function(req, res) {
    const id = req.params.id;
    const category = database.categories.find((category) => category.id === +id);
    res.json(category);
}); 



app.get('/users', function(req, res) {
    res.send(database.users);
})

app.post('/users', function(req, res) {
    const user = {
        ...req.body,
        userId: Date.now()
    }

    console.log(user);

    database.users.push(user);  
    res.status(200).json(user);
});

app.get('/users/:id', function(req, res) {
    const id = req.params.id;
    const user = database.user.find((user) => user.UserId === +id);
    res.json(user);
}); 

app.get('/userTasks/:id', function(req, res) {
    const userTasks = database.tasks.filter(task => {
        return task.userId == +req.params.id
    });
    res.status(200).json(userTasks);
})

app.post('/users/is-logged-in', function(req, res) {
    const userLogin = req.body.login;
    const userPassword = req.body.password;

    const user = database.users.find(user => user.login === userLogin && user.password === userPassword);

    res.json(user ? {id: user.userId, login: user.login} : null);
})