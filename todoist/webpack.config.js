const path = require('path');

module.exports = {
    entry: './src/app/index.js',
    mode: 'development',
    output: {
        filename:'index.js',
        path: path.resolve(__dirname,'dist')
    },
    resolve: {
        alias: {
          images: path.resolve(__dirname, 'src/images'),
        },
      },
    module: {
        rules: [
            {
            test: /\.css$/,
            use: [
                'style-loader',
                'css-loader'
                ] 
            },
            {
            test: /\.(png|svg|jpg|gif)$/,
            use: [
              'file-loader',
            ],
          },
        ]
    }
}