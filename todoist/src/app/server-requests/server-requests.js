export class ServerRequests {
    getTasks() {
        return fetch(`http://localhost:2000/tasks`)
            .then(res => res.json())
            
    }
    
    getCategories() {
        return fetch('http://localhost:2000/categories')
            .then(res => res.json())
    }
    
    getTask(id) {
        return fetch(`http://localhost:2000/tasks/${id}`)
            .then(res => res.json())
    }
    
    getCategories() {
        return fetch('http://localhost:2000/categories')
            .then(res => res.json())
    }
    
    getCategory(id) {
        return fetch(`http://localhost:2000/categories/${id}`)
            .then(res => res.json())
    }

    getUsers() {
        return fetch('http://localhost:2000/users')
            .then(res => res.json());
    }

    getUser(id) {
        return fetch(`http://localhost:2000/users/${id}`)
            .then(res => res.json())
    }

    getUserTasks(id) {
        return fetch(`http://localhost:2000/userTasks/${id}`)
            .then(res => res.json())
    }
}

