export function changeStatusTitle(task) {
    const tdStatus = document.querySelector(`.tbody tr[data-id = "${task.id}"] td:last-child`);

    if(tdStatus.textContent === 'low') {
        tdStatus.style.color = '#28a745';
    } else if(tdStatus.textContent === 'middle') {
        tdStatus.style.color = '#ffc107';
    } else if(tdStatus.textContent === 'important'){
        tdStatus.style.color = '#dc3545';
    }
}