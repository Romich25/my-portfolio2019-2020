
export function changeDoneTitle(task) {
    // const tdTitle = document.querySelector(`.tbody tr[data-id = "${task.id}"] td:first-child`);
    const tdProgress = document.querySelector(`.tbody tr[data-id = "${task.id}"] td:nth-child(5)`);


    const tr = tdProgress.parentElement;
    if(tdProgress.textContent === '100%') {
        // tdTitle.style.textDecoration = 'line-through';
        
        tr.style.textDecoration = 'line-through';
        tr.classList.add('text-black-50');

    } else {
        // tdTitle.style.textDecoration = 'none';
        tr.style.textDecoration = 'none';
        tr.classList.remove('text-black-50');
    }
}
    