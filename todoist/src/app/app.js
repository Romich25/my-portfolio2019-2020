import './app.css';
import { Header } from './header';
import { Main } from './main';
import { Footer } from './footer';
import { EnterModal } from './enter-modal';
import { RegisterModal } from './register-modal';


export function App() {
    const app = document.createElement('div');

    app.classList.add('app');

    app.append(EnterModal(), RegisterModal(), Header(), Main(), Footer());

    return app;
}

