import './modal-add-task.css';
import { FormAddTask } from './form';
import { getTr} from '../../main/tasks-filtration/all-tasks/table/tbody/get-tr';
import { ServerRequests } from '../../server-requests/server-requests';
import { getAutocompleteTitles } from '../../header/container/search-label/search-input-group/search/getAutocompleteTitles';
import { renderTaskInfoToModalChange } from '../modal/render-tasks';

export function ModalAddTask() {
    const modalAddTask = document.createElement('div');

    modalAddTask.classList.add('modal-add-task');
    modalAddTask.append(FormAddTask());
    modalAddTask.addEventListener('submit', addNewTask);

    function addNewTask(ev) {
        ev.preventDefault();
        const title = ev.target.querySelector('#input-add').value;
        const deadlineDate = ev.target.querySelector('#input-date').value;
        const category = ev.target.querySelector('#category').value;
        const priorityValue = +ev.target.querySelector('#priority').value;
        const time = ev.target.querySelector('#input-time').value;
        const comment = ev.target.querySelector('.comment').value;
        const user = document.querySelector('.user-name-input').getAttribute('placeholder');
        // const userId = document.querySelector('.user-name-input').getAttribute('data-id');
        const userId = localStorage.getItem('id');

        let priority;
        if(priorityValue === 0) {
            priority = 'low'
        } else if(priorityValue === 1) {
            priority = 'middle'
        } else if(priorityValue === 2) {
            priority = 'important'
        }
        
        const task = {
            title,
            deadlineDate,
            category,
            priority,
            time,
            comment,
            user,
            userId
        }

        fetch('http://localhost:2000/tasks/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(task)
        })
        .then(res => res.json())
        .then(id => {
            new ServerRequests().getTask(id).then(task => {
                const tr = getTr(task);
                const tBody = document.querySelector('tbody');
                const td = tr.lastChild;

                if(td.textContent === 'low') {
                    td.style.color = '#28a745';
                } else if(td.textContent === 'middle') {
                    td.style.color = '#ffc107';
                } else if(td.textContent === 'important'){
                    td.style.color = '#dc3545';
                }

                tr.addEventListener('click', getInfoTaskFromTable);
                tBody.append(tr);
        
                function getInfoTaskFromTable(ev) {
                    const id = ev.target.parentElement.getAttribute('data-id');
                    const modal = document.querySelector('.modal');
                    const titleOfModal = document.querySelector('.modal-header h2');
                    const title = ev.target.parentElement.firstChild.textContent;
        
                    modal.style.display = "block";
                    titleOfModal.textContent = `${title}`;
                      
                    new ServerRequests().getTask(id)
                    .then(task => {
                        renderTaskInfoToModalChange(task);
                    })
                }
            })

            const getTasks = new ServerRequests().getUserTasks(userId);

            getAutocompleteTitles(getTasks);
        })
        .then(() => {
            const title = document.querySelector('#input-add');
            const deadlineDate = document.querySelector('#input-date');
            const time = document.querySelector('#input-time');
            const category = document.querySelector('#category');
            const comment = document.querySelector('.comment');

            title.value = null;
            deadlineDate.value = null;
            time.value = null;
            category.value = null;
            comment.value =null;
        })
        modalAddTask.style.display = 'none';
    }
    return modalAddTask;
}