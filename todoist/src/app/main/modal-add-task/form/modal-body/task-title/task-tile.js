import './task-title.css';

export function TaskTitle() {
    const taskTitle = document.createElement('label');
    const title = document.createElement('div');
    const titleInput = document.createElement('input');

    title.innerHTML = 'task title:';

    taskTitle.classList.add('task-title');
    titleInput.classList.add('form-control', 'input', 'input-add');

    titleInput.setAttribute('type', 'text');
    titleInput.setAttribute('placeholder', 'create task');
    titleInput.setAttribute('id', 'input-add');
    titleInput.setAttribute('required', 'required');

    taskTitle.append(title, titleInput);
    

    return taskTitle;
}