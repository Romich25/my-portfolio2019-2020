import './deadline-date.css';

export function DeadlineDate() {
    const deadlineDate = document.createElement('label');
    const title = document.createElement('div');
    const daedlineDateInput = document.createElement('input');

    title.innerHTML = 'deadline date:';

    deadlineDate.classList.add('deadline-date');
    daedlineDateInput.classList.add('form-control', 'input-date');

    daedlineDateInput.setAttribute('id', 'input-date');
    daedlineDateInput.setAttribute('type', 'text');
    daedlineDateInput.setAttribute('autocomplete', 'off');
    daedlineDateInput.setAttribute('required', 'required');

    deadlineDate.append(title, daedlineDateInput);

    $(function() {
        $(daedlineDateInput).datepicker({
        dateFormat: 'yy-m-d',
        dayNamesMin : [ "S", "M", "T", "W", "T", "F", "S" ],
        })
    })

    return deadlineDate;
}