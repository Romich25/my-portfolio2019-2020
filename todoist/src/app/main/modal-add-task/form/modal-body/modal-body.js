import './modal-body.css';
import { TaskTitle } from './task-title';
import { DeadlineDate } from './deadline-date';
import { Time } from './time';
import { Category } from './category';
import { Priority } from './priority';
import { Comment } from './comment';

export function AddTaskModalBody() {
    const addTaskModalBody = document.createElement('div');

    addTaskModalBody.classList.add('form-group', 'mb-0', 'labels', 'd-flex', 'justify-content-between', 'flex-wrap', 'modal-body');

    addTaskModalBody.append(TaskTitle(), DeadlineDate(), Time(), Category(), Priority(), Comment());

    return addTaskModalBody;
}