import './comment.css';

export function Comment() {
    const commentLabel = document.createElement('label');
    const commentText = document.createElement('textarea');

    commentLabel.classList.add('comment-label', 'w-100');
    commentText.classList.add('form-control', 'comment');

    commentText.setAttribute('rows', '3');
    commentText.setAttribute('placeholder', 'add comment');

    commentLabel.innerHTML = 'comment:';

    commentLabel.append(commentText);

    return commentLabel;
}