import './time.css';

export  function Time() {
    const time = document.createElement('label');
    const title = document.createElement('div');
    const timeInput = document.createElement('input');

    title.innerHTML = 'time:';

    time.classList.add('time');
    timeInput.classList.add('form-control', 'input-time');

    timeInput.setAttribute('type', 'time');
    timeInput.setAttribute('id', 'input-time');
    timeInput.setAttribute('autocomplete', 'off');

    time.append(title, timeInput);

    return time;
}