
export function categoryRender(getCategories, categorySelect) {
    function addCategories(categories) {
        categories.forEach(category => {    
            addOptionsInCategories(category);
        })
    }

    function addOptionsInCategories(category) {
        const option = document.createElement('option');
        option.textContent = category.category;
        option.setAttribute('data-id', category.id);
        categorySelect.append(option);
    }

    getCategories.then(categories => {
        addCategories(categories);
        return categories;
    })
}


    

    
    
    