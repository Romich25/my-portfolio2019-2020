import './category.css';
import { ServerRequests } from '../../../../../server-requests/server-requests';



export function Category() {
    const addCategory = document.createElement('label');
    const title = document.createElement('div');
    const inputGroup = document.createElement('div');
    const inputGroupPrepend = document.createElement('div');
    const categorySelect = document.createElement('select');

    addCategory.classList.add('add-category');
    inputGroup.classList.add('input-group', 'mb-3');
    inputGroupPrepend.classList.add('input-group-prepend', 'add-new');
    categorySelect.classList.add('form-control', 'category');

    categorySelect.setAttribute('name', 'category');
    categorySelect.setAttribute('id', 'category');
   
    title.innerHTML = 'category:';
    inputGroupPrepend.innerHTML = `
        <span class="input-group-text" id="basic-addon1">
            <i class="fa fa-plus" aria-hidden="true"></i>
        </span>
    `;

    inputGroup.append(inputGroupPrepend, categorySelect);
    addCategory.append(title, inputGroup);

    function addCategories(categories) {
        categories.forEach(category => {    
            addOptionsInCategories(category);
        })
    }

    function addOptionsInCategories(category) {
        const option = document.createElement('option');
        option.textContent = category.category;
        option.setAttribute('data-id', category.id);
        categorySelect.append(option);
    }

    const getCategories = new ServerRequests().getCategories();
    
    getCategories.then(categories => {
        addCategories(categories);
        return categories;
    })


    inputGroupPrepend.addEventListener('click', openCategoryAddModal);

    function openCategoryAddModal() {
        const addCategoryModal = document.querySelector('.add-category-modal');
        addCategoryModal.style.display = "block";
    }

    return addCategory;
}