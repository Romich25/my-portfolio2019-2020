import './modal-header.css';

export function AddTaskModalHeader() {
    const addTaskModalheader = document.createElement('div');
    const title = document.createElement('h2');
    const close = document.createElement('span');

    title.innerHTML = 'Create your task:';
    close.innerHTML = '&times;';

    addTaskModalheader.classList.add('modal-header', 'bg-info');
    close.classList.add('close');

    addTaskModalheader.append(title, close);
 
    close.onclick = function() {
        const modal = document.querySelector('.modal-add-task');
        modal.style.display = 'none';
    }


    window.addEventListener('click', (ev) => {
        const modal = document.querySelector('.modal-add-task');
        if(ev.target === modal) {
            modal.style.display = 'none';
        }
    })

    return addTaskModalheader;
}