import './modal-footer.css';

export function AddTaskModalFooter() {
    const addTaskModalFooter = document.createElement('div');
    const saveButton = document.createElement('button');

    addTaskModalFooter.classList.add('modal-footer', 'bg-info');
    saveButton.classList.add('save-button', 'btn', 'btn-primary');

    saveButton.setAttribute('type', 'submit');
    saveButton.setAttribute('id', 'save-button');
    saveButton.innerHTML = `
        save
        <i class="fa fa-floppy-o" aria-hidden="true"></i>
    `;

    addTaskModalFooter.append(saveButton);

    return addTaskModalFooter;
}