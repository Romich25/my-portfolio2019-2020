import './form.css';
import { AddTaskModalHeader } from './modal-header';
import { AddTaskModalBody } from './modal-body';
import { AddTaskModalFooter } from './modal-footer';

export function FormAddTask() {
    const formAddTask = document.createElement('form');

    formAddTask.classList.add('form-add-task', 'modal-content');

    formAddTask.setAttribute('id', 'form-task');

    formAddTask.append(AddTaskModalHeader(), AddTaskModalBody(), AddTaskModalFooter());
    
    return formAddTask
}