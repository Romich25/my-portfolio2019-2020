import './category-form.css';
import { ModalHeader } from './modal-header';
import { ModalBody } from './modalbody';
import { ServerRequests } from '../../../server-requests/server-requests';


export function CategoryForm() {
    const categoryForm = document.createElement('form');

    categoryForm.classList.add('category-form', 'modal-content', 'col-4');

    categoryForm.append(ModalHeader(), ModalBody());

    categoryForm.addEventListener('submit', addNewCategories);

    function addNewCategories(ev) {
        ev.preventDefault();

        const category = document.querySelector('.new-category').value;
        const newCategory = {
            category
        }
        
        fetch('http://localhost:2000/categories/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(newCategory)
        })
        .then(res => {
            return res.json();
        })
        .then(category => {
            const categoryEl = document.querySelector('.new-category');
            const newOption = document.createElement('option');
            const addNewCategoryModal = document.querySelector('.add-category-modal');

            newOption.textContent = `${category.category}`;
            newOption.setAttribute('data-id', category.id);

            categoryEl.appendChild(newOption);
           
            addNewCategoryModal.style.display = 'none';

            return category.id;
        }).then(id => {
            new ServerRequests().getCategory(id).then(category => {
                addOptionsInCategories(category);

                function addOptionsInCategories(category) {
                    const categorySelects = document.querySelectorAll('.category');

                    categorySelects.forEach(select => {
                        const option = document.createElement('option');
                        option.textContent = category.category;
                        option.setAttribute('data-id', category.id);
                        select.append(option);
                    })
                }
            })
        })
    }

    return categoryForm;
}