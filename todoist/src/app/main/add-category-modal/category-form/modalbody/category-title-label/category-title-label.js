import './category-title-label.css';

export function CategoryTitleLabel() {
    const categoryTitleLabel = document.createElement('label');
    const categoryTitle = document.createElement('div');
    const categoryInput = document.createElement('input');

    categoryTitleLabel.classList.add('category-title-label','col');
    categoryInput.classList.add('form-control', 'new-category');

    categoryInput.setAttribute('type', 'text');
    categoryInput.setAttribute('required', 'required');

    categoryTitle.innerHTML = 'category title:';

    categoryTitleLabel.append(categoryTitle, categoryInput);

    return categoryTitleLabel;
}