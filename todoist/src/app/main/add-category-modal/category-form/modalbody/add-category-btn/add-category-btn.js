import './add-category-btn.css';

export function AddCategoryBtn() {
    const addCategoryBtn = document.createElement('button');
    
    addCategoryBtn.classList.add('btn', 'btn-primary', 'add-category-btn');

    addCategoryBtn.setAttribute('type', 'submit');

    addCategoryBtn.innerHTML = `
        add
        <i class="fa fa-floppy-o" aria-hidden="true"></i>
    `;

    return addCategoryBtn;
}