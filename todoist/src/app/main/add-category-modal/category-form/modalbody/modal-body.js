import './modal-body.css';
import { CategoryTitleLabel } from './category-title-label';
import { AddCategoryBtn } from './add-category-btn';

export function ModalBody() {
    const modalBody = document.createElement('div');

    modalBody.classList.add('form-group', 'modal-body');

    modalBody.append(CategoryTitleLabel(), AddCategoryBtn());

    return modalBody;
}