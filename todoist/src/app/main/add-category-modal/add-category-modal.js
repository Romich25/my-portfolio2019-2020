import './add-category-modal.css';
import { CategoryForm } from './category-form';

export function AddCategoryModal() {
    const addCategoryModal = document.createElement('div');

    addCategoryModal.classList.add('add-category-modal');
    
    addCategoryModal.append(CategoryForm());

    return addCategoryModal;
}