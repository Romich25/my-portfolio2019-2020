import './progress-filtration.css';

export function ProgressFiltration() {
    const progressFiltration = document.createElement('label');
    const progressFilter = document.createElement('select');
    const content = `
        <option value="all">all</option>
        <option value="all-not-completed">all not completed</option>
        <option value="0%">0%</option>
        <option value="25%">25%</option>
        <option value="50%">50%</option>
        <option value="75%">75%</option>
        <option value="100%">100%</option>
    `;

    progressFiltration.classList.add('progress-filtration');
    progressFilter.classList.add('form-control', 'progress-filter');

    progressFiltration.innerHTML = 'progress:';
    progressFilter.innerHTML = content;

    progressFiltration.append(progressFilter);

    
    progressFilter.addEventListener('change', filterByProgress);

    function filterByProgress() {
        const trTableRows = document.querySelectorAll('.tbody tr');

        trTableRows.forEach(tr => {
            if(progressFilter.value === 'all')  {
                tr.style.display = 'table-row';
            }  else if(progressFilter.value === 'all-not-completed' && tr.childNodes[4].textContent !== '100%') {
                tr.style.display = 'table-row';
            } else if (tr.childNodes[4].textContent !== progressFilter.value) {
                tr.style.display = 'none';
            } else {
                tr.style.display = 'table-row';
            }     
        })
    }

    return progressFiltration;
}