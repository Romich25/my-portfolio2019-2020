import './priority-filtration.css';

export function PriorityFiltration() {
    const priorityFiltration = document.createElement('label');
    const priorityFilter = document.createElement('select');
    const content = `
        <option value="all">all</option>
        <option value="low">low</option>
        <option value="middle">middle</option>
        <option value="important">important</option>
    `;

    priorityFiltration.classList.add('priority-filtration');
    priorityFilter.classList.add('form-control', 'priority-filter');

    priorityFiltration.innerHTML = 'priority:';
    priorityFilter.innerHTML = content;

    priorityFiltration.append(priorityFilter);


    priorityFilter.addEventListener('change', filterByPriority);

    function filterByPriority() {
        const trTableRows = document.querySelectorAll('tbody tr');

        trTableRows.forEach(tr => {
            if(priorityFilter.value === 'all')  {
                tr.style.display = 'table-row';
            } else if (tr.childNodes[5].textContent !== priorityFilter.value) {
                tr.style.display = 'none';
            } else {
                tr.style.display = 'table-row';
            }     
        })
    }


    return priorityFiltration;
}