import './show-tasks.css';

export function ShowTasks() {
    const showTasks = document.createElement('button');
    const content = `
        <i class="fa fa-th-list" aria-hidden="true"></i>
        all tasks`;
    

    showTasks.classList.add('btn', 'btn-success', 'show-tasks');
    showTasks.setAttribute('type', 'button');
    showTasks.innerHTML = content;

    
    showTasks.addEventListener('click', showAllTasks);
    function showAllTasks() {
        const dateFilter = document.querySelector('.date-filter');
        const timeFilter = document.querySelector('.select-time');
        const categoryFilter = document.querySelector('.category-filter');
        const progressFilter = document.querySelector('.progress-filter');
        const priorityFilter = document.querySelector('.priority-filter');
        const trTableRows = document.querySelectorAll('.tbody tr');

        trTableRows.forEach(tr => {
            tr.style.display = 'table-row';
            dateFilter.value = null;
            timeFilter.value = null;
            categoryFilter.value = null;
            progressFilter.value = null;
            priorityFilter.value = null;
        })
    }

    return showTasks;
}