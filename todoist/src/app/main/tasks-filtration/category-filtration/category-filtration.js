import './category-filtration.css';
import { categoryRender } from '../../modal-add-task/form/modal-body/category/categoryRender';
import { ServerRequests } from '../../../server-requests/server-requests';

export function CategoryFiltration() {
    const categoryFiltration = document.createElement('label');
    const categorySelect = document.createElement('select');

    const content = `
        <option value="all">all</option>
    `;

    categoryFiltration.classList.add('category-iltration');
    categorySelect.classList.add('form-control', 'category', 'category-filter');

    categoryFiltration.innerHTML = 'category:';
    categorySelect.innerHTML = content;

    categoryFiltration.append(categorySelect);

    const categories = new ServerRequests().getCategories();

    categoryRender(categories, categorySelect);

  
    categorySelect.addEventListener('change', filterByCategory);

    function filterByCategory() {
        const trTableRows = document.querySelectorAll('.tbody tr');

        trTableRows.forEach(tr => {
            if(categorySelect.value == 'all')  {
                tr.style.display = 'table-row';
            } else if (tr.childNodes[3].textContent !== categorySelect.value) {
                tr.style.display = 'none';
            } else {
                tr.style.display = 'table-row';
            }      
        })
    }

    return categoryFiltration;
}