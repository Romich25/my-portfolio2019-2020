import './date-filtration.css';

export function DateFiltration() {
    const dateFiltration = document.createElement('label');
    const dateFilter = document.createElement('input');

    dateFiltration.classList.add('date-filtration');
    dateFiltration.innerHTML = 'date:';

    dateFilter.classList.add('form-control', 'date-filter');
    dateFiltration.append(dateFilter);
    

    $(function() {
        $(dateFilter).datepicker({
        dateFormat: 'yy-m-d',
        dayNamesMin : [ "S", "M", "T", "W", "T", "F", "S" ],
        onSelect: filterByDate     
        })

    })


    function filterByDate(date) {
        const rows = document.querySelectorAll('.all-tasks tbody tr');

        rows.forEach(row => {
            if (row.childNodes[1].textContent !== date) {
                row.style.display = 'none';
            } else {
                row.style.display = 'table-row';
            }      
        })
    } 

    return dateFiltration
}