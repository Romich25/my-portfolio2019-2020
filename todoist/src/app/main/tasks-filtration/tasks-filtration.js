import './tasks-filtration.css';
import { ShowTasks } from './show-tasks/show-tasks';
import { TimeFiltration } from './time-filtration';
import { DateFiltration } from './date-filtration';
import { CategoryFiltration } from './category-filtration';
import { ProgressFiltration } from './progress-filtration';
import { PriorityFiltration } from './priority-filtration';


export function TasksFiltration() {
    const tasksFiltration = document.createElement('div');

    tasksFiltration.classList.add('tasks-filtration', 'd-flex', 'justify-content-around', 'align-items-center', 'flex-wrap', 'bg-secondary', 'text-white');

    tasksFiltration.append(ShowTasks(), TimeFiltration(), DateFiltration(), CategoryFiltration(), ProgressFiltration(), PriorityFiltration());
    
    return tasksFiltration;
}