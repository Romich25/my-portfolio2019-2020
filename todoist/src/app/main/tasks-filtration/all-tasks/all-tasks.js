import './all-tasks.css';
import { Table } from './table';


export function AllTasks() {
    const allTasks = document.createElement('div');

    allTasks.classList.add('all-tasks');
    allTasks.append(Table());

    return allTasks;
}