import './thead.css';

export function Thead() {
    const tHead = document.createElement('thead');
    const content = `
    <tr>
        <th scope="col">task title</th>
        <th scope="col">deadline date</th>
        <th scope="col">time</th>
        <th scope="col">category</th>
        <th scope="col">progress</th>
        <th scope="col">priority</th>
    </tr>
    `;

    tHead.classList.add('thead', 'thead-dark');
    tHead.innerHTML = content;

    return tHead;
}