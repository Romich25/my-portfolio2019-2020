import './table.css';
import { Thead } from './thead';
import { Tbody } from './tbody';

export function Table() {
    const table = document.createElement('table');

    table.classList.add('table');

    table.append(Thead(), Tbody());
    

    return table;
}