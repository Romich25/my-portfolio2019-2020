import './tbody.css';


export function Tbody() {
    const tbody = document.createElement('tbody');

    tbody.classList.add('tbody', 'tasks');

    return tbody;
}

