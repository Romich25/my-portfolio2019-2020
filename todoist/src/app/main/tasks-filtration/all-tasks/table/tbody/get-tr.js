import { ServerRequests } from '../../../../../server-requests/server-requests';
import { renderTaskInfoToModalChange } from '../../../../modal/render-tasks';

export function getTr(task) {
    const tr = document.createElement('tr');
    const taskTitleTd = getTd(task.title);
    const taskDeadlineDateTd = getTd(task.deadlineDate);
    const taskTimeTd = getTd(task.time);
    const taskCategoryTd = getTd(task.category);
    const taskProgressTd = getTd(`${task.progress}%`);
    const taskPriorityTd = getTd(task.priority);

    taskPriorityTd.style.fontWeight = 'bold';

    taskTitleTd.setAttribute('class', 'title');

    tr.append(taskTitleTd, taskDeadlineDateTd, taskTimeTd, taskCategoryTd, taskProgressTd, taskPriorityTd);
    tr.setAttribute('data-id', task.id);
    tr.setAttribute('style', 'display: table-row');

    tr.addEventListener('click', getInfoTaskFromTable);


    function getInfoTaskFromTable(ev) {
        const id = ev.target.parentElement.getAttribute('data-id');
        
        new ServerRequests().getTask(id)
        .then(task => {
            renderTaskInfoToModalChange(task);

            const modal = document.querySelector('.modal');
            const titleOfModal = document.querySelector('.modal .modal-header h2');
            
            const title = ev.target.parentElement.firstChild.textContent;

            modal.style.display = "block";
            titleOfModal.innerHTML = `${title}`;
        })
    }

    
    function getTd(content) {
        const td = document.createElement('td');
    
        td.textContent = content;

        if(td.textContent === 'low') {
            td.style.color = '#28a745';
        } else if(td.textContent === 'middle') {
            td.style.color = '#ffc107';
        } else if(td.textContent === 'important'){
            td.style.color = '#dc3545';
        }
        return td;
    }

    return tr;
} 