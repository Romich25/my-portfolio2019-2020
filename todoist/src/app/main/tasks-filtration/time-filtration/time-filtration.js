import './time-filtration.css';

export function TimeFiltration() {
    const timeFiltration = document.createElement('label');
    const selectTime = document.createElement('select');
    const content = `
    <option value="all">all</option>
    <option value="today">today</option>
    <option value="week">week</option>
    <option value="month">month</option>
    <option value="year">year</option>
    `;
    

    timeFiltration.classList.add('time-filtration');
    selectTime.classList.add('form-control', 'select-time');

    timeFiltration.innerHTML = 'time:';
    selectTime.innerHTML = content;

    timeFiltration.append(selectTime);

    
    selectTime.addEventListener('change', filterByTime);

    function filterByTime() {
        const trTableRows = document.querySelectorAll('.tbody tr');

        trTableRows.forEach(tr => {
            if(selectTime.value === 'all')  {
                tr.style.display = 'table-row';
            } else if (selectTime.value === 'today' && tr.childNodes[1].textContent !== `${new Date().getFullYear()}-${new Date().getMonth()+1}-${new Date().getDate()}`) {
                tr.style.display = 'none';
            } else if (selectTime.value === 'week' && Date.parse(tr.childNodes[1].textContent) >= (Date.parse(new Date())+ 1000*60*60*24*7) ) {
                tr.style.display = 'none';
            } else if (selectTime.value === 'month' && Date.parse(tr.childNodes[1].textContent) >= (Date.parse(`${new Date().getFullYear()}-${new Date().getMonth() + 2}-${new Date().getDate()}`))) {
                tr.style.display = 'none';
            } else if (selectTime.value === 'year' && Date.parse(tr.childNodes[1].textContent) >= (Date.parse(`${new Date().getFullYear() + 1}-${new Date().getMonth()}-${new Date().getDate()}`))) {
                tr.style.display = 'none';
            } else {
                tr.style.display = 'table-row';
            }    
        })
    }

    return timeFiltration;
}