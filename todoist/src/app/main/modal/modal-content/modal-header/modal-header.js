import './modal-header.css';

export function ModalHeader() {
    const modalHeader = document.createElement('div');
    const title = document.createElement('h2');
    const close = document.createElement('span');

    modalHeader.classList.add('modal-header', 'bg-info');
    close.classList.add('close');

    close.innerHTML = '&times;';

    modalHeader.append(title, close);

    
    close.addEventListener('click', closeModal);

    function closeModal(ev) {
        const taskModal = document.querySelector('.modal');
        taskModal.style.display = 'none'
    }
   
    window.addEventListener('click', (ev) => {
        const taskModal = document.querySelector('.modal');
        if(ev.target == taskModal) {
            taskModal.style.display = 'none';
        }
    })




    return modalHeader;
}