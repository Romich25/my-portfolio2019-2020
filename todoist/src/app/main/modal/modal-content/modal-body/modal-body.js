import './modal-body.css';
import { ChangeForm } from './change-form';

export function ModalBody() {
    const modalBody = document.createElement('div');
    const p = document.createElement('p');

    modalBody.classList.add('modal-body');
    modalBody.append(p, ChangeForm());

    return modalBody;
}