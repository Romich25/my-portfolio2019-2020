import './deadline-label.css';

export function DeadlineLabel() {
    const deadlineLabel = document.createElement('label');
    const title = document.createElement('div');
    const deadlineInput = document.createElement('input');

    deadlineLabel.classList.add('deadline-label');
    deadlineInput.classList.add('form-control', 'input-date');

    title.innerHTML = 'deadline date:';

    deadlineInput.setAttribute('type', 'text');
    deadlineInput.setAttribute('id', 'info-date');
    deadlineInput.setAttribute('autocomplete', 'off');
    deadlineInput.setAttribute('required', 'required');

    deadlineLabel.append(title, deadlineInput);

    $(function() {
        $(deadlineInput).datepicker({
        dateFormat: 'yy-m-d',
        dayNamesMin : [ "S", "M", "T", "W", "T", "F", "S" ],
        })

    })

    return deadlineLabel;
}