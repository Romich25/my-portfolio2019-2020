import './form-group.css';
import { Title } from './title';
import { Labels } from './labels';

export function FormGroup() {
    const formGroup = document.createElement('div');

    formGroup.classList.add('form-group');

    formGroup.append(Title(), Labels());

    return formGroup;
}