import './comment-label.css';

export function CommentLabel() {
    const commentLabel = document.createElement('label');
    const commentText = document.createElement('textarea');

    commentLabel.classList.add('comment-label', 'w-100');
    commentText.classList.add('form-control', 'comment-info');

    commentText.setAttribute('rows', '3');

    commentLabel.innerHTML = 'comment:';

    commentLabel.append(commentText);

    return commentLabel;
}