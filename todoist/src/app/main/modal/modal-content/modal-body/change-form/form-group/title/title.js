import './title.css';

export function Title() {
    const title = document.createElement('h2');

    title.classList.add('h2', 'display-4', 'row', 'justify-content-center');

    title.innerHTML = 'Edit task:';

    return title;
}