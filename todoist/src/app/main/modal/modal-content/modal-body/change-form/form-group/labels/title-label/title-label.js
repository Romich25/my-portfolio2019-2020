import './title-label.css';

export function TitleLabel() {
    const titleLabel = document.createElement('label');
    const title = document.createElement('div');
    const titleInput = document.createElement('input');

    titleLabel.classList.add('title-label');
    titleInput.classList.add('form-control', 'info-title');

    titleInput.setAttribute('id', 'info-title');
    titleInput.setAttribute('type', 'text');
    titleInput.setAttribute('required', 'required');
    
    title.innerHTML = 'task title:';

    titleLabel.append(title, titleInput);

    return titleLabel
}