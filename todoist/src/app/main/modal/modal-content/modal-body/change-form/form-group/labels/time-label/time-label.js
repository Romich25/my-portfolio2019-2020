import './time-label.css';

export function TimeLabel() {
    const timeLabel = document.createElement('label');
    const title = document.createElement('div');
    const timeInput = document.createElement('input');

    timeLabel.classList.add('time-label');
    timeInput.classList.add('form-control');

    timeInput.setAttribute('type', 'time');
    timeInput.setAttribute('id', 'info-time');
    timeInput.setAttribute('autocomplete', 'off');

    title.innerHTML = 'time:';

    timeLabel.append(title, timeInput);

    return timeLabel
}