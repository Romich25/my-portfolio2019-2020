import './progress-label.css';

export function ProgressLabel() {
    const progressLabel = document.createElement('label');
    const progressTitle = document.createElement('div');
    const progressInput = document.createElement('input');

    progressLabel.classList.add('progress-label');
    progressTitle.classList.add('progress-title');
    progressInput.classList.add('custom-range', 'progress-input');

    progressInput.setAttribute('type', 'range');
    progressInput.setAttribute('min', '0');
    progressInput.setAttribute('max', '4');
    progressInput.setAttribute('value', '0');

    progressTitle.innerHTML = 'progress: 0%';

    progressLabel.append(progressTitle, progressInput);


    progressInput.addEventListener('input', () => {
        let progress;
        if(+progressInput.value === 0) {
            progress = 0;
        } else if(+progressInput.value === 1) {
            progress = 25;
        } else if(+progressInput.value === 2) {
            progress = 50;
        } else if(+progressInput.value === 3) {
            progress = 75;
        } else {
            progress = 100;
        }

        progressTitle.textContent = `progress: ${progress}%`;
    })

    return progressLabel;
}