import './priority-label.css';

export function PriorityLabel() {
    const priorityLabel = document.createElement('label');
    const priorityTitle = document.createElement('div');
    const priorityInput = document.createElement('input');

    priorityLabel.classList.add('priority-label');
    priorityTitle.classList.add('priority-title');
    priorityInput.classList.add('custom-range', 'priority-input');

    priorityInput.setAttribute('type', 'range');
    priorityInput.setAttribute('min', '0');
    priorityInput.setAttribute('max', '2');
    priorityInput.setAttribute('value', '0');
    priorityInput.setAttribute('id', 'info-priority');

    priorityTitle.innerHTML = 'priority:';

    priorityLabel.append(priorityTitle, priorityInput);
    

    function renderPriority() {
        priorityInput.addEventListener('input', () => {
            let priority;
            if(priorityInput.value == 0) {
                priority = 'low'
            } else if(priorityInput.value == 1) {
                priority = 'middle'
            } else if(priorityInput.value == 2) {
                priority = 'important'
            }

            priorityTitle.textContent = `priority: ${priority}`;
        })
    };

    renderPriority();

    return priorityLabel;
}