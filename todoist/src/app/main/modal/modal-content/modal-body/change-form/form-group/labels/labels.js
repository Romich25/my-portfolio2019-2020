import './labels.css';
import { TitleLabel } from './title-label';
import { DeadlineLabel } from './deadline-label';
import { TimeLabel } from './time-label';
import { CategoryLabel } from './category-label';
import { ProgressLabel } from './progress-label';
import { PriorityLabel } from './priority-label';
import { CommentLabel } from './comment-label';


export function Labels() {
    const labels = document.createElement('div');

    labels.classList.add('labels', 'd-flex', 'justify-content-between', 'flex-wrap');

    labels.append(TitleLabel(), DeadlineLabel(), TimeLabel(), CategoryLabel(), ProgressLabel(), PriorityLabel(), CommentLabel());

    return labels;
}