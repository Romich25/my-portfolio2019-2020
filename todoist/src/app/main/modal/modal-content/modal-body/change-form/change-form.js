import './change-form.css';
import { FormGroup } from './form-group';

export function ChangeForm() {
    const changeForm = document.createElement('form');

    changeForm.classList.add('change-form');

    changeForm.append(FormGroup());

    return changeForm;
}