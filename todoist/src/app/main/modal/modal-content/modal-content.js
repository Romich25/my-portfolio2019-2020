import './modal-content.css';
import { ModalHeader } from './modal-header/modal-header.js';
import { ModalBody } from './modal-body/modal-body';
import { ModalFooter } from './modal-footer';

export function ModalContent() {
    const modalContent = document.createElement('div');

    modalContent.classList.add('modal-content');

    modalContent.append(ModalHeader(), ModalBody(), ModalFooter());

    return modalContent;
}