import './change-btn.css';
import { ServerRequests } from '../../../../../server-requests/server-requests';
import { getAutocompleteTitles } from '../../../../../header/container/search-label/search-input-group/search/getAutocompleteTitles';
import { changeDoneTitle } from '../../../../../task helpers/changeDoneTitle';
import { changeStatusTitle } from '../../../../../task helpers/changeStatusTitle';

export function ChangeBtn() {
    const changeBtn = document.createElement('button');
    const content = `
        <i class="fa fa-floppy-o" aria-hidden="true"></i>
        save
    `;

    changeBtn.classList.add('change-button', 'btn', 'btn-primary');
    changeBtn.innerHTML = content;


    changeBtn.addEventListener('click', changeTask);

    function changeTask() {
        const title = document.querySelector('.info-title').value;
        const deadlineDate = document.querySelector('#info-date').value;
        const category = document.querySelector('#info-category').value;
        const priorityValue = +document.querySelector('.priority-input').value;
        const progressValue = +document.querySelector('.progress-input').value;
        const time = document.querySelector('#info-time').value;
        const comment = document.querySelector('.comment-info').value;
        // const userId = document.querySelector('.user-name-input').getAttribute('data-id');
        const userId = localStorage.getItem('id');

        let priority;
        if(priorityValue === 0) {
            priority = 'low'
        } else if(priorityValue === 1) {
            priority = 'middle'
        } else if(priorityValue === 2) {
            priority = 'important'
        }

        let progress;
        if(progressValue === 0) {
            progress = 0;
        } else if(progressValue === 1) {
            progress = 25;
        } else if(progressValue === 2) {
            progress = 50;
        } else if(progressValue === 3) {
            progress = 75;
        } else {
            progress = 100;
        }
        
        const task = {
            title,
            deadlineDate,
            time,
            category,
            progress,
            priority,
            comment
        }    

        const id = document.querySelector('.info-title').getAttribute('data-id');
    
        fetch('http://localhost:2000/tasks/'+id, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(task)
        })
        .then(res => res.json())
        .then(id => {
            const changeTrEl = document.querySelector(`tr[data-id="${id}"]`);
            const childOfTr = changeTrEl.childNodes;

            childOfTr[0].textContent = title;
            childOfTr[1].textContent = deadlineDate;
            childOfTr[2].textContent = time;
            childOfTr[3].textContent = category;
            childOfTr[4].textContent = `${progress}%`;
            childOfTr[5].textContent = priority;

            const modal = document.querySelector('.modal');
            modal.style.display = 'none';

            const getTasks = new ServerRequests().getUserTasks(userId);

            getAutocompleteTitles(getTasks);

            new ServerRequests().getTask(id).then(task => {
                changeDoneTitle(task);
                changeStatusTitle(task)
            })        
        })
    }
    return changeBtn;
}