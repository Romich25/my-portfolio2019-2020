import './delete-btn.css';
import { ServerRequests } from '../../../../../server-requests/server-requests';
import { getAutocompleteTitles } from '../../../../../header/container/search-label/search-input-group/search/getAutocompleteTitles';

export function DeleteBtn() {
    const deleteBtn = document.createElement('button');
    const content = `
        <i class="fa fa-trash-o" aria-hidden="true"></i>
        del
    `;

    deleteBtn.classList.add('delete-button', 'btn', 'btn-danger');
    deleteBtn.innerHTML = content;

    deleteBtn.addEventListener('click', deleteTask);

    function deleteTask() {
        const id = document.querySelector('.info-title').getAttribute('data-id');

        fetch('http://localhost:2000/tasks/'+id, {
            method: 'Delete',
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(res => {
            return res.json();
        }).then(id => {
            const delTrEl = document.querySelector(`tr[data-id="${id}"]`);
            const modal = document.querySelector('.modal');
            const userId = localStorage.getAttribute('id');
            const getTasks = new ServerRequests().getUserTasks(userId);

            delTrEl.remove();
            
            modal.style.display = 'none';

            getAutocompleteTitles(getTasks);
        })
    }

    return deleteBtn;
}