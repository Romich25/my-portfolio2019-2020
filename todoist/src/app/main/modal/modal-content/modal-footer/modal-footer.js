import './modal-footer.css';
import { ChangeBtn } from './change-btn';
import { DeleteBtn } from './delete-btn';

export function ModalFooter() {
const modalFooter = document.createElement('div');

modalFooter.classList.add('modal-footer', 'bg-info');

modalFooter.append(ChangeBtn(), DeleteBtn());

return modalFooter
}