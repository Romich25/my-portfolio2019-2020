import './modal.css';
import { ModalContent } from './modal-content';

export function Modal() {
    const modal = document.createElement('div');

    modal.classList.add('modal');
    modal.setAttribute('id', 'infoModal');
    modal.append(ModalContent());

    return modal;
}