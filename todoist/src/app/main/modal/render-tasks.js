export function renderTaskInfoToModalChange(task) {
    const titleInput = document.querySelector('.info-title');
    const deadlineInput = document.querySelector('#info-date');
    const categoryInput = document.querySelector('#info-category');
    const timeInput = document.querySelector('#info-time');
    const priority = document.querySelector('#info-priority');
    const modalTitle = document.querySelector('.modal .modal-header h2');
    const createDate = document.querySelector('.modal .modal-body p');
    const priorityTitle = document.querySelector('.priority-title');
    const progressInput = document.querySelector('.progress-input');
    const progressTitle = document.querySelector('.progress-title');
    const commentInfo = document.querySelector('.comment-info');

    titleInput.value = task.title;
    titleInput.setAttribute('data-id', task.id);
    deadlineInput.value = task.deadlineDate;
    categoryInput.value = task.category;
    timeInput.value = task.time;
    commentInfo.value = task.comment;
    
    let progressValue;
    if(task.progress === 0) {
        progressValue = 0;
    } else if(task.progress === 25) {
        progressValue = 1;
    } else if(task.progress === 50) {
        progressValue = 2;
    } else if(task.progress === 75) {
        progressValue = 3;
    } else if(task.progress === 100) {
        progressValue = 4;
    }

    progressTitle.textContent = `progress: ${task.progress}%`;
    progressInput.value = progressValue;

    let priorityValue;
    if(task.priority == 'low') {
        priorityValue = 0;
    } else if(task.priority == 'middle') {
        priorityValue = 1;
    } else {
        priorityValue = 2;
    }
    priority.value = priorityValue;
    modalTitle.textContent = task.title;
    createDate.textContent = `create date: ${task.date}`;
    priorityTitle.textContent = `priority: ${task.priority}`;

    return task;
}