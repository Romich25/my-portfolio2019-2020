import './add-task-btn.css';


export function AddTaskBtn() {
    const addTaskBtn = document.createElement('button');
    
    const content = `
    <i class="fa fa-pencil" aria-hidden="true"></i>
    Add new task`;
    

    addTaskBtn.classList.add('add-task-btn', 'btn', 'btn-lg', 'btn-block', 'btn-info', 'mb-5', 'mt-4');
    addTaskBtn.innerHTML = content;

    addTaskBtn.addEventListener('click', openAddNewTaskModal);

    function openAddNewTaskModal() {
        const addModal = document.querySelector('.modal-add-task');
        addModal.style.display = 'block';
    }

    return addTaskBtn;
}