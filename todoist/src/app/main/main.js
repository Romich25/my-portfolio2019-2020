import './main.css';
import { AddTaskBtn } from './add-task-btn';
import { TasksFiltration } from './tasks-filtration';
import { AllTasks } from './tasks-filtration/all-tasks';
import { Modal } from './modal';
import { AddCategoryModal } from './add-category-modal';
import { ModalAddTask } from './modal-add-task';

export function Main() {
    const main = document.createElement('main');
    const title = document.createElement('h2');

    title.classList.add('text-center', 'bg-secondary', 'text-white', 'mb-0');
    title.innerHTML = 'Tasks filter';

    main.classList.add('main', 'container');

    main.append(AddTaskBtn(), title, TasksFiltration(), AllTasks(), Modal(), ModalAddTask(), AddCategoryModal());

    return main;
}