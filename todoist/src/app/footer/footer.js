import './footer.css';

export function Footer() {
    const footer = document.createElement('footer');

    footer.classList.add('footer', 'bg-info', 'text-center', 'text-white');

    footer.innerHTML = `&copy; Roman Panfilov, 2020`;

    return footer;
}