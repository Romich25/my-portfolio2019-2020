import './register-modal.css';
import { ModalContent } from './modal-content';

export function RegisterModal() {
    const registerModal = document.createElement('div');

    registerModal.classList.add('register-modal');


    registerModal.append(ModalContent());

    return registerModal;
}