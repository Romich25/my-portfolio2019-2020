import './modal-content.css';
import { ModalHeader } from './modal-header';
import { ModalBody } from './modal-body';
import { ServerRequests } from '../../server-requests/server-requests';

export function ModalContent() {
    const modalContent = document.createElement('form');

    modalContent.classList.add('register-form', 'modal-content');

    modalContent.append(ModalHeader(), ModalBody());

    modalContent.addEventListener('submit', registerNewUser);

    function registerNewUser(ev) {
        ev.preventDefault();

        const login = document.querySelector('.register-login').value;
        const password = document.querySelector('.register-password').value;
        const user = {
            login,
            password
        };

        const getUsers = new ServerRequests().getUsers();
        

        fetch('http://localhost:2000/users/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
        })
        .then(res => res.json())
        .then(user => {
            getUsers
            .then(users => {
                const findUsers = users.find(us => {
                    return us.login === user.login;
                });

                if(findUsers === undefined) {
                    alert(`user ${user.login} is registered`);
                    const registerModal = document.querySelector('.register-modal');
                    registerModal.style.display = 'none';
                } else {
                    const errorLoginText = document.querySelector('.error-login-text');
                    const loginInput = document.querySelector('.register-login');

                    errorLoginText.innerHTML = 'This username is already taken. Enter a different username';
                    
                    loginInput.addEventListener('focus', delErrorText);

                    function delErrorText() {
                        errorLoginText.innerHTML = null;
                    }
                }
            })
        })
    }

    return modalContent;
}