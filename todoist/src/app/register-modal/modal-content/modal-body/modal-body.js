import './modal-body.css'
import { LoginLabel } from './login-label';
import { PasswordLabel } from './password-label';
import { RegisterButton} from './register-button';

export function ModalBody() {
    const modalBody = document.createElement('div');

    modalBody.classList.add('form-group', 'modal-body', 'd-flex', 'flex-wrap');

    modalBody.append(LoginLabel(), PasswordLabel(), RegisterButton());

    return modalBody;
}