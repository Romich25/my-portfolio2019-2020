import './login-label.css';

export function LoginLabel() {
    const loginLabel = document.createElement('label');
    const title = 'login:';
    const loginInput = document.createElement('input');
    const errorLoginText = document.createElement('p');

    loginLabel.classList.add('login-label', 'w-100');
    loginInput.classList.add('form-control', 'register-login');
    errorLoginText.classList.add('error-login-text', 'text-danger');

    loginInput.setAttribute('type', 'text');
    loginInput.setAttribute('required', 'required');

    
    loginLabel.append(title, loginInput, errorLoginText);

    return loginLabel;
}