import './register-button.css';

export function RegisterButton() {
    const registerButton = document.createElement('button');
    const content = `
        register
    `;

    registerButton.classList.add('btn', 'btn-primary', 'register-button', 'w-100');

    registerButton.setAttribute('type', 'submit');

    registerButton.innerHTML = content;

    return registerButton;
}
