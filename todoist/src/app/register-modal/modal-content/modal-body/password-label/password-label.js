import './password-label.css';

export function PasswordLabel() {
    const passwordLabel = document.createElement('label');
    const title = 'password:';
    const passwordInput = document.createElement('input');

    passwordLabel.classList.add('password-label', 'w-100');
    passwordInput.classList.add('form-control', 'register-password');

    passwordInput.setAttribute('type', 'password');
    passwordInput.setAttribute('required', 'required');

    passwordLabel.append(title, passwordInput);

    return passwordLabel;
}