import './modal-header.css'

export function ModalHeader() {
    const modalHeader = document.createElement('div');
    const title = document.createElement('h2');
    const close = document.createElement('span');

    modalHeader.classList.add('modal-header', 'bg-info');
    close.classList.add('close');

    close.innerHTML = `&times;`;
    title.innerHTML = 'Register';

    close.addEventListener('click', closeRegisterModal);

    function closeRegisterModal() {
        const registerModal = document.querySelector('.register-modal');
        registerModal.style.display = 'none';
    }

    modalHeader.append(title, close);

    return modalHeader;
}