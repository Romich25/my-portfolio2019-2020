import './enter-modal.css';
import { ModalContent } from './modal-content';
import { ServerRequests } from '../server-requests/server-requests';
import { getAutocompleteTitles } from '../../app/header/container/search-label/search-input-group/search/getAutocompleteTitles';
import { getTr } from '../main/tasks-filtration/all-tasks/table/tbody/get-tr';
import { changeDoneTitle } from '../task helpers/changeDoneTitle';


export function EnterModal() {
    const enterModal = document.createElement('div');
  
    enterModal.classList.add('enter-modal');

    enterModal.append(ModalContent());

    if(localStorage.getItem('id') !== null) {
        enterModal.style.display = 'none';
        const id = localStorage.getItem('id');
            
        new ServerRequests().getUserTasks(id)
        .then(tasks => {
            const tbody = document.querySelector('.tbody');
            tbody.style.display = 'table-rows';
            getAutocompleteTitles(new ServerRequests().getUserTasks(id));
            tasks.forEach(task => {
                const userNameInput = document.querySelector('.user-name-input');
                const login = localStorage.getItem('login');

                tbody.append(getTr(task))
                
                changeDoneTitle(task);
                
                userNameInput.setAttribute('placeholder', `${login}`);
            })
        })
    } else {
        enterModal.style.display = 'block';
    }

    return enterModal;
}