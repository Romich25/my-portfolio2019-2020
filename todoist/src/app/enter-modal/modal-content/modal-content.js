import './modal-content.css';
import { ModalHeader } from './modal-header/index.';
import { ModalBody } from './modal-body';
import { ServerRequests } from '../../server-requests/server-requests';
import { getTr } from './../../main/tasks-filtration/all-tasks/table/tbody/get-tr';
import { changeDoneTitle } from '../../task helpers/changeDoneTitle';
import { getAutocompleteTitles } from '../../header/container/search-label/search-input-group/search/getAutocompleteTitles';


export function ModalContent() {
    const modalContent= document.createElement('form');

    modalContent.classList.add('enter-form', 'modal-content');

    modalContent.append(ModalHeader(), ModalBody());

    modalContent.addEventListener('submit', enterInToDo);


    function enterInToDo(ev) {
        ev.preventDefault();
        const login = document.querySelector('.login').value;
        const password = document.querySelector('.password').value;
        const userData = {
            login,
            password
        }

        fetch('http://localhost:2000/users/is-logged-in', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(userData)
    })
    .then(res => res.json())
    .then(userData => {
        if(userData !== null) {
            localStorage.setItem('login', `${userData.login}`);
            localStorage.setItem('id', `${userData.id}`);
        } 
        return userData.id;
    })
    
    .then(id => {
        const enterModal = document.querySelector('.enter-modal');
        
        enterModal.style.display = 'none';

        new ServerRequests().getUserTasks(id)
        .then(tasks => {
            const tbody = document.querySelector('.tbody');

            getAutocompleteTitles(new ServerRequests().getUserTasks(id));

            tasks.forEach(task => {
                tbody.append(getTr(task))
                changeDoneTitle(task);
            })   
            const userNameInput = document.querySelector('.user-name-input');
                
            userNameInput.setAttribute('placeholder', `${login}`);
        })
    })
    .catch(() => {
        const errorText = document.querySelector('.error-text');
        const inputLogin = document.querySelector('.login');
        const inputPassword = document.querySelector('.password');

        

        errorText.innerHTML = `The username or password you entered is incorrect. 
        Enter the correct data or register`;

        inputLogin.addEventListener('keydown', delErrorText);
        inputPassword.addEventListener('keydown', delErrorText);

        function delErrorText() {
            errorText.innerHTML = null;
        }
    })    

        // new ServerRequests().getUsers()
        // .then(users => {
           
        // const user = users.find((user) => {
        //     const userLogin = user.login;
        //     const userPassword = user.password;
        //     const userid = user.userId;
        //     const userNameInput = document.querySelector('.user-name-input');

        //     userNameInput.setAttribute('placeholder', `${userLogin}`);
        //     userNameInput.setAttribute('data-id', `${userid}`);

        //     localStorage.setItem(`login`,`${userLogin}`);
        //     localStorage.setItem(`id`, `${userid}`);
        //     return userLogin === login && userPassword === password;
        // })
            
        //    return user.userId
        // })
        // .then(id => {
        //     const enterModal = document.querySelector('.enter-modal');
        
        //     enterModal.style.display = 'none';
           
        //     new ServerRequests().getUserTasks(id)
        //     .then(tasks => {
        //         const tbody = document.querySelector('.tbody');
                             
        //         getAutocompleteTitles(new ServerRequests().getUserTasks(id));
        //         tasks.forEach(task => {
        //             tbody.append(getTr(task))
                    
        //             changeDoneTitle(task);
        //         })
        //     })
        // })
        // .catch(() => {
        //     const errorText = document.querySelector('.error-text');
        //     const inputLogin = document.querySelector('.login');
        //     const inputPassword = document.querySelector('.password');

        //     errorText.innerHTML = `The username or password you entered is incorrect. 
        //     Enter the correct data or register`;

        //     inputLogin.addEventListener('keydown', delErrorText);
        //     inputPassword.addEventListener('keydown', delErrorText);

        //     function delErrorText() {
        //         errorText.innerHTML = null;
        //     }
        // })
    }
    return modalContent;
}