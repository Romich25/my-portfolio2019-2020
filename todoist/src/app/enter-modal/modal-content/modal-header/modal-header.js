import './modal-header.css';

export function ModalHeader() {
    const modalHeader = document.createElement('div');
    const content = `
        <h2>Sing in/Register</h2>
    `;

    modalHeader.classList.add('modal-header', 'bg-info');

    modalHeader.innerHTML = content;

    return modalHeader;
}

