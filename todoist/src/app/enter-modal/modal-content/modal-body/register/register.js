import './register.css';

export function Register() {
    const register = document.createElement('p');

    register.classList.add('register', 'text-primary');

    register.innerHTML = 'register now';

    register.addEventListener('click', openRgisterModal);

    function openRgisterModal() {
        const registerModal = document.querySelector('.register-modal');

        registerModal.style.display = 'block';
    }

    return register;
}