import './login-label.css';

export function LoginLabel() {
    const loginLabel = document.createElement('label');
    const title = 'login:';
    const loginInput = document.createElement('input');

    loginLabel.classList.add('login-label', 'w-100');
    loginInput.classList.add('form-control', 'login');

    loginInput.setAttribute('type', 'text');

    loginLabel.append(title, loginInput);

    return loginLabel;
}