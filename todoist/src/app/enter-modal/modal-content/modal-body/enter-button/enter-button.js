import './enter-button.css';

export function EnterButton() {
    const enterButton = document.createElement('button');
    const content = `
        <i class="fa fa-sign-in" aria-hidden="true"></i>
        enter
    `;

    enterButton.classList.add('btn', 'btn-primary', 'enter-button', 'w-100');

    enterButton.setAttribute('type', 'submit');

    enterButton.innerHTML = content;
       

    return enterButton;
}

