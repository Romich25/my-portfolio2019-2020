import './password-label.css';

export function PasswordLabel() {
    const passwordLabel = document.createElement('label');
    const title = 'password:';
    const passwordInput = document.createElement('input');

    passwordLabel.classList.add('password-label', 'w-100');
    passwordInput.classList.add('form-control', 'password');

    passwordInput.setAttribute('type', 'password');

    passwordLabel.append(title, passwordInput);

    return passwordLabel;
}