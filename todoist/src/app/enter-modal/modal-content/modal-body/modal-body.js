import './modal-body.css';
import { LoginLabel } from './login-label';
import { PasswordLabel } from './password-label';
import { EnterButton } from './enter-button';
import { ErrorText } from './error-text';
import { Register } from './register';


export function ModalBody() {
    const modalBody = document.createElement('div');

    modalBody.classList.add('form-group', 'modal-body', 'd-flex', 'flex-wrap');

    modalBody.append(LoginLabel(), PasswordLabel(), EnterButton(), ErrorText(), Register());

    return modalBody;
}

