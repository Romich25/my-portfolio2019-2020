import './error-text.css';

export function ErrorText() {
    const errorText = document.createElement('p');

    errorText.classList.add('error-text', 'text-danger');

    return errorText;
}