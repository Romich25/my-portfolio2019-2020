export function getTask(id) {
    return fetch(`http://localhost:2000/tasks/${id}`)
        .then(res => res.json())
}