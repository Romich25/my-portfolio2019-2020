import './header.css';
import { Container } from './container';




export function Header() {
    const header = document.createElement('header');

    header.classList.add('header', 'bg-info', 'pb-3', 'pt-4', 'h4', 'mb-3');

    header.append(Container());

    return header;
}

