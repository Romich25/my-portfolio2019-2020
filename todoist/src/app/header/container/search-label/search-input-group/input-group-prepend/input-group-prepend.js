import './input-group-prepend.css';

export function InputGroupPrepend() {
    const inputGroupPrepend = document.createElement('div');
    const content = `
        <div class="input-group-text">
            <i class="fa fa-search" aria-hidden="true"></i>
        </div>
    `;

    inputGroupPrepend.classList.add('input-group-prepend', 'button-search');

    inputGroupPrepend.innerHTML = content;

    inputGroupPrepend.addEventListener('click', openSearchTaskInTable);

    function openSearchTaskInTable() {
        const trTableRows = document.querySelectorAll('.tbody tr');
        const search = document.querySelector('.search');
        trTableRows.forEach(tr => {
            if(tr.childNodes[0].textContent !== search.value) {
            tr.style.display = 'none';
            } else {
                tr.style.display = 'table-row'
            }

            if(search.value === '') {
                tr.style.display = 'table-row'
            }
        })
    }

    return inputGroupPrepend;
}