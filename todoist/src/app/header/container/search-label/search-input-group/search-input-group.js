import './search-input-group.css';
import { Search } from './search/search';
import { InputGroupPrepend } from './input-group-prepend';

export function SearchInputGroup() {
    const inputGroup = document.createElement('div');

    inputGroup.classList.add('input-group', 'mb-2');

    inputGroup.append(Search(), InputGroupPrepend());

    return inputGroup;
}
