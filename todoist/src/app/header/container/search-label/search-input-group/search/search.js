import './search.css';
import { ServerRequests } from '../../../../../server-requests/server-requests';
import { getAutocompleteTitles } from './getAutocompleteTitles';


export function Search() {
    const search = document.createElement('input');

    search.classList.add('search', 'form-control');

    search.setAttribute('type', 'search');
    const userId = localStorage.getItem('id');

    const getTasks = new ServerRequests().getUserTasks(userId);

    
    getAutocompleteTitles(getTasks);

    search.addEventListener('keypress', openSearchTaskInTableEnter);
    
    function openSearchTaskInTableEnter(ev) {
        if(ev.keyCode === 13) {
           ev.preventDefault();
           openSearchTaskInTable();
       }
   }

   function openSearchTaskInTable() {
        const trTableRows = document.querySelectorAll('.tbody tr');
        trTableRows.forEach(tr => {
            if(search.value === '') {
                tr.style.display = 'table-row'
            }

            if(tr.childNodes[0].textContent.includes(`${search.value}`) == false) {
                tr.style.display = 'none';
            } else {
                tr.style.display = 'table-row'
            }
        })
    }
    return search;
}