export function getAutocompleteTitles(getTasks) {
    

    getTasks
    .then(tasks => tasks.map(task => task.title))      
    .then(source =>  $('.search').autocomplete({
        source,
        minLength: 3,
        select: function( event, ui ) {
            const rows = document.querySelectorAll('.all-tasks tbody tr');
            const search = document.querySelector('.search');
            rows.forEach(tr => {
                if(tr.childNodes[0].textContent !== ui.item.value) {
                    tr.style.display = 'none';
            } else {
                tr.style.display = 'table-row'
            }
    
            if(ui.item.value === '') {
                tr.style.display = 'table-row'
            }
              
            })

        }
    }))
}