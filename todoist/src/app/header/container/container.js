import './container.css';
import { Logo } from './logo';
import { User } from './user';
import { SearchLabel } from './search-label';


export function Container() {
    const container = document.createElement('div');

    container.classList.add('container', 'd-flex', 'justify-content-between', 'flex-wrap');

    container.append(Logo(), SearchLabel(), User());

    return container;
}