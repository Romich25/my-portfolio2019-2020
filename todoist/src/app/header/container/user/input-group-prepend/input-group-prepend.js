import './input-group-prepend.css';

export function InputGroupPrepend() {
    const inputGroupPrepend = document.createElement('div');
    const content = `
        <div class="input-group-text">
            <i class="fa fa-user-circle-o" aria-hidden="true"></i>
        </div>
    `;

    inputGroupPrepend.classList.add('input-group-prepend');
    inputGroupPrepend.innerHTML = content;

    return inputGroupPrepend;
}