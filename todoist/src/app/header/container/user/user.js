import './user.css';
import { InputGroupPrepend } from './input-group-prepend';
import { Input } from './input';
import { ExitButton } from './exet-button/exit-button';


export function User() {
    const user = document.createElement('label');
    const inputGroup = document.createElement('div');

    user.classList.add('user', 'd-flex');
    inputGroup.classList.add('input-group', 'mb-2', 'mr-sm-2');
    

    user.append(inputGroup);

    inputGroup.append(InputGroupPrepend(), Input(), ExitButton());

    return user;
}