import './exit-button.css';

export function ExitButton() {
    const exitButton = document.createElement('button');
    

    exitButton.classList.add('btn', 'btn-dark', 'ml-2');

    exitButton.setAttribute('type', 'button');

    exitButton.innerHTML = 'exit';

    exitButton.addEventListener('click', openEnterModal);

    function openEnterModal() {
        const enterModal = document.querySelector('.enter-modal');
        const tableRows = document.querySelectorAll('.tbody tr');
        const inputPassword = document.querySelector('.enter-modal .password')

        enterModal.style.display = 'block';

        tableRows.forEach(tr => {
            tr.remove();
        })
        localStorage.clear();
        inputPassword.value = null;
    }

    return exitButton;
}