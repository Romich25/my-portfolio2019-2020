import './input.css';

export function Input() {
    const input = document.createElement('input');

    input.classList.add('form-control', 'user-name-input');

    input.setAttribute('readonly', 'readonly');
    input.setAttribute('name', 'user');

    return input;
}